package com.xajeyu.stackgeoduck.basemvp;

/**
 * @PackageName: com.xajeyu.stackgeoduck.basemvp
 * @FileName: BaseView.java
 * @CreationTime: 2017/5/19  15:18
 * @author: XajeYu
 * @E-mail: xajeyu@gmail.com
 * @function:
 */
public interface BaseView<T> {
    void setPresenter(T presenter);
}
