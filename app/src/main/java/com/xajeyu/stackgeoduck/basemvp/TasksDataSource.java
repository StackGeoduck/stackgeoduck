package com.xajeyu.stackgeoduck.basemvp;

import android.support.annotation.NonNull;

/**
 * @PackageName: com.xajeyu.stackgeoduck.basemvp
 * @FileName: TasksDataSource.java
 * @CreationTime: 2017/5/19  15:21
 * @author: XajeYu
 * @E-mail: xajeyu@gmail.com
 * @function:
 */
public interface TasksDataSource {
    void getTasks(@NonNull LoadTasksCallback loadTasksCallback);
    void getTask(@NonNull String taskID,@NonNull GetTasksCallback callback);
    /**
     * 数据操作回调
     */
    interface LoadTasksCallback{
        void onTasksLoaded(Object data);
        void onDataNotAvailable(Object msg);
    }

    /**
     * 获取数据操作
     */
    interface GetTasksCallback{
        void onTasksLoaded(Object data);
        void onDataNotAvailable(Object msg);
    }



}
