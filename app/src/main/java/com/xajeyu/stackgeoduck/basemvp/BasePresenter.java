package com.xajeyu.stackgeoduck.basemvp;

/**
 * @PackageName: com.xajeyu.stackgeoduck.basemvp
 * @FileName: BasePresenter.java
 * @CreationTime: 2017/5/19  15:18
 * @author: XajeYu
 * @E-mail: xajeyu@gmail.com
 * @function: Presenter
 */
public interface BasePresenter {
    void start();
}
