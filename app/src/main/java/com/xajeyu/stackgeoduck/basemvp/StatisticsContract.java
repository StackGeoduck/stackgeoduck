package com.xajeyu.stackgeoduck.basemvp;

/**
 * @PackageName: com.xajeyu.stackgeoduck.basemvp
 * @FileName: StatisticsContract.java
 * @CreationTime: 2017/5/19  15:28
 * @author: XajeYu
 * @E-mail: xajeyu@gmail.com
 * @function: VIEW and PRESENTER
 */
public interface StatisticsContract {
    interface View extends BaseView<Presenter>{
        void setProgressIndicator(Object msg);
        void showOnSuccess(Object msg);
        void showLoadFail(Object msg);
    }
    interface Presenter extends BasePresenter{

    }
}
