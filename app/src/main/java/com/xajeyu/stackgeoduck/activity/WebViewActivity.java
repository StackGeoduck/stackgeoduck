package com.xajeyu.stackgeoduck.activity;

import android.content.Context;
import android.graphics.Color;
import android.icu.text.DecimalFormat;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.DownloadListener;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.xajeyu.stackgeoduck.R;
import com.xajeyu.stackgeoduck.utils.NetworkTypeUtils;

import commons.validator.routines.UrlValidator;

/**
 * Created by e on 2017/5/22.
 */

public class WebViewActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView tiele;
    private WebView webView;
    private ProgressBar progressBar;
    private ImageView returnImgView;
    private Context mContext;
    private View decorView;
    private TextView mNetwornType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        mContext = this;
        initView();
        setWebView();
    }

    public void initView() {
        returnImgView = (ImageView) findViewById(R.id.webview_return);
        tiele = (TextView) findViewById(R.id.webview_title);
        webView = (WebView) findViewById(R.id.scan_webView);
        progressBar = (ProgressBar) findViewById(R.id.webview_progressBar);
        returnImgView.setOnClickListener(this);
    }

    public void setWebView() {
        progressBar.setMax(100);
        String des = getIntent().getStringExtra("url"); //获取URL
        UrlValidator u = new UrlValidator();
        boolean valid = u.isValid(des);
        if (valid) {
            webView.loadUrl(des);   //HTML
        } else {
            webView.loadDataWithBaseURL(des, des, "text/htlm", "utf-8", null);//字符串
        }

        //支持JavaScript功能
        webView.getSettings().setJavaScriptEnabled(true);
        //设置可以支持缩放
        webView.getSettings().setSupportZoom(true);
        //设置出现缩放工具,必须要加,否则缩放不起作用
        webView.getSettings().setBuiltInZoomControls(true);
        //设置隐藏缩放工具
        webView.getSettings().setDisplayZoomControls(false);
        //扩大比例的缩放
        webView.getSettings().setUseWideViewPort(true);
        //自适应屏幕
        webView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        webView.getSettings().setLoadWithOverviewMode(true);
        //在本app运行此URL，不跳转到浏览器
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return super.shouldOverrideUrlLoading(view, url);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                tiele.setText(view.getTitle());
            }
        });
        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                System.out.println("进度" + newProgress);
                if (newProgress == 100) {
                    // 网页加载完成
                    progressBar.setVisibility(View.GONE);
                } else {
                    // 加载中
                    progressBar.setProgress(newProgress);
                }
                super.onProgressChanged(view, newProgress);
            }
        });
        webView.setDownloadListener(new DownloadListener() {
            @Override
            public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimetype, long contentLength) {
                //调用内置浏览器下载
//                Uri uri = Uri.parse(url);
//                Intent intent = new Intent(Intent.ACTION_VIEW,uri);
//                startActivity(intent);
                System.out.println("-->" + url + "--" + userAgent + "--" + contentDisposition + "--" + mimetype + "--" + contentLength);

                AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.remind_dialog);
                AlertDialog dialog = builder.create();
                //显示dialog
                dialog.show();

                Window window = dialog.getWindow();
                //设置最底下
                window.setGravity(Gravity.BOTTOM);
                //设置动画
                window.setWindowAnimations(R.style.showdialog);
                //设置显示的view
                window.setContentView(R.layout.webview_dialog);
                //获取view
                decorView = window.getDecorView();
                TextView size = (TextView) decorView.findViewById(R.id.webview_dialog_FileSize);
                mNetwornType = (TextView) decorView.findViewById(R.id.webview_dialog_networntype);
                TextView downloadTv = (TextView) decorView.findViewById(R.id.webview_dialog_download_tv);
                TextView editTv = (TextView) decorView.findViewById(R.id.webview_dialog_edit_tv);
                //监听
                downloadTv.setOnClickListener(WebViewActivity.this);
                editTv.setOnClickListener(WebViewActivity.this);
                //设置显示文件大小
                size.setText(FormetFileSize(contentLength));
                //获取窗口管理者
                WindowManager windowManager = getWindowManager();
                //获取Display
                Display display = windowManager.getDefaultDisplay();
                //获取WindowManager.LayoutParams
                WindowManager.LayoutParams lp = window.getAttributes();
                //获取宽度
                lp.width = (display.getWidth());
                //设置宽度
                dialog.getWindow().setAttributes(lp);
                //获取网络类型
                NetworkTypeUtils.NetwornType networkType = NetworkTypeUtils.getNetworkTypeUtils().isNetworkType(mContext, false);
                isNetwornType(networkType);
            }
        });

    }

    public void isNetwornType(NetworkTypeUtils.NetwornType networkType) {
        switch (networkType) {
            case NETWORN_NONE:

                break;
            case NETWORN_MOBILE:
                mNetwornType.setText("正在使用移动网络，将产生流量");
                mNetwornType.setTextColor(Color.RED);
                break;
            case NETWORN_WIFI:
                mNetwornType.setText("正在使用无线网络");
                break;
        }
    }

    //点击事件
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.webview_return://返回
                finish();
                break;
            case R.id.webview_dialog_download_tv: //下载
                System.out.println("点击下载");
                break;
            case R.id.webview_dialog_edit_tv:
                System.out.println("编辑文件路径");
                break;
        }
    }

    /**
     * 转换文件大小为B、KB、MB、GB
     *
     * @param fileS 文件大小
     * @return 以B、KB、MB、GB形式返回文件大小
     */
    public String FormetFileSize(long fileS) {
        DecimalFormat df = new DecimalFormat("#.00");
        String fileSizeString;
        if (fileS < 1024) {
            fileSizeString = df.format((double) fileS) + "B";
        } else if (fileS < 1048576) {
            fileSizeString = df.format((double) fileS / 1024) + "KB";
        } else if (fileS < 1073741824) {
            fileSizeString = df.format((double) fileS / 1048576) + "MB";
        } else {
            fileSizeString = df.format((double) fileS / 1073741824) + "GB";
        }
        return fileSizeString;
    }
}
