package com.xajeyu.stackgeoduck.activity;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.github.jdsjlzx.recyclerview.LRecyclerView;
import com.github.jdsjlzx.recyclerview.LRecyclerViewAdapter;
import com.google.gson.Gson;
import com.universalvideoview.UniversalMediaController;
import com.universalvideoview.UniversalVideoView;
import com.xajeyu.stackgeoduck.R;
import com.xajeyu.stackgeoduck.adapter.MvRecyclerViewAdapter;
import com.xajeyu.stackgeoduck.dataobj.home.MvMp4Bean;
import com.xajeyu.stackgeoduck.network.api.news.NewsApi;
import com.xajeyu.stackgeoduck.utils.NetworkTypeUtils;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by 13517 on 2017/5/15.
 */

public class HomeMusicMvActivity extends AppCompatActivity {
    View mBottomLayout;
    View mVideoLayout;
    UniversalVideoView mVideoView;
    UniversalMediaController mMediaController;
    int cachedHeight;
    private NetworkTypeUtils.NetwornType networkType;
    private String mvid;
    private LRecyclerView lRecyclerView;
    private MvMp4Bean mvMp4Bean;
    private TextView tvTitle;
    private TextView tvSinger;
    private TextView tvPlayCount;
    private TextView tvTime;
    private TextView tvDes;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music_mv);

        initUI();

        initData();

        HttpRequest();

    }

    /**
     * 网络请求
     */
    private void HttpRequest() {

        OkHttpClient okHttpClient = new OkHttpClient();
        Request request = new Request.Builder()
                .url(NewsApi.MvMp4 + mvid)
                .build();

        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                String string = response.body().string();
                //解析JSON数据
                Gson gson = new Gson();

                mvMp4Bean = gson.fromJson(string, MvMp4Bean.class);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //在主线程中设置MV的标题
                        mMediaController.setTitle(mvMp4Bean.data.name);
                        //设置MV的链接，_$240可以修改清晰度 （240 480 720 1080）
                        mVideoView.setVideoURI(Uri.parse(mvMp4Bean.data.brs._$480));

                        tvTitle.setText(mvMp4Bean.data.name);
                        tvSinger.setText("歌手："+mvMp4Bean.data.artistName);
                        tvPlayCount.setText("播放："+mvMp4Bean.data.playCount);
                        tvTime.setText("发行："+mvMp4Bean.data.publishTime);
                        if (!mvMp4Bean.data.desc.equals("")){
                            tvDes.setText("简介："+mvMp4Bean.data.desc);
                        }else {
                            tvDes.setText("简介："+"无");

                        }


                    }
                });

            }
        });
    }

    private void initData() {

        //视频播放器 设置媒体控制器
        mVideoView.setMediaController(mMediaController);

        //设置竖屏视频区域大小
        setVideoAreaSize();

        //判断网络类型
        networkType = NetworkTypeUtils.getNetworkTypeUtils().isNetworkType(getApplicationContext(), false);
        isNetworkType(networkType);

        //开始播放视频  一进来就开始
        mVideoView.start();

        //设置视频播放器的回调
        mVideoView.setVideoViewCallback(new UniversalVideoView.VideoViewCallback() {


            @Override
            public void onScaleChange(boolean isFullscreen) {
                if (isFullscreen) {
                    ViewGroup.LayoutParams layoutParams = mVideoLayout.getLayoutParams();
                    layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
                    layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT;
                    mVideoLayout.setLayoutParams(layoutParams);
                    //设置全屏时,无关的View消失,以便为视频控件和控制器控件留出最大化的位置
                    // mBottomLayout.setVisibility(View.GONE);
                } else {
                    ViewGroup.LayoutParams layoutParams = mVideoLayout.getLayoutParams();
                    layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
                    layoutParams.height = cachedHeight;
                    mVideoLayout.setLayoutParams(layoutParams);
                    //mBottomLayout.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPause(MediaPlayer mediaPlayer) {

            }

            @Override
            public void onStart(MediaPlayer mediaPlayer) {

            }

            @Override
            public void onBufferingStart(MediaPlayer mediaPlayer) {

            }

            @Override
            public void onBufferingEnd(MediaPlayer mediaPlayer) {

            }
        });


        LRecyclerViewAdapter lRecyclerViewAdapter = new LRecyclerViewAdapter(new MvRecyclerViewAdapter(mvMp4Bean));
        lRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        lRecyclerView.setPullRefreshEnabled(false);
        lRecyclerView.setAdapter(lRecyclerViewAdapter);
    }

//            ToggleButton s = new ToggleButton(getApplicationContext());


    private void initUI() {
        tvTitle = (TextView) findViewById(R.id.tv_mv_title);
        tvSinger = (TextView) findViewById(R.id.tv_mv_singer);
        tvPlayCount = (TextView) findViewById(R.id.tv_mv_paly_count);
        tvTime = (TextView) findViewById(R.id.tv_mv_time);
        tvDes = (TextView) findViewById(R.id.tv_mv_des);

        lRecyclerView = (LRecyclerView) findViewById(R.id.mv_recyclerview);
        mVideoView = (UniversalVideoView) findViewById(R.id.videoView);
        mMediaController = (UniversalMediaController) findViewById(R.id.media_controller);
        mVideoLayout = findViewById(R.id.video_layout);
        mvid = getIntent().getStringExtra("MVID");
    }


    /**
     * 设置竖屏视频区域大小
     */
    private void setVideoAreaSize() {
        mVideoLayout.post(new Runnable() {
            @Override
            public void run() {
                int width = mVideoLayout.getWidth();
                cachedHeight = (int) (width * 405f / 720f);
//                cachedHeight = (int) (width * 3f / 4f);
//                cachedHeight = (int) (width * 9f / 16f);
                ViewGroup.LayoutParams videoLayoutParams = mVideoLayout.getLayoutParams();
                videoLayoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
                videoLayoutParams.height = cachedHeight;
                mVideoView.requestFocus();
            }
        });
    }

    /**
     * 判断当前是移动网络还是Wi-fi
     *
     * @param networnType
     */
    public void isNetworkType(NetworkTypeUtils.NetwornType networnType) {
        switch (networnType) {
            case NETWORN_NONE:
                Toast.makeText(getApplicationContext(), "请检查当前网络是否可用", Toast.LENGTH_SHORT).show();
                break;
            case NETWORN_WIFI:
                mVideoView.start();
                break;
            case NETWORN_MOBILE:
                break;
        }
    }


}
