package com.xajeyu.stackgeoduck.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.xajeyu.stackgeoduck.R;
import com.xajeyu.stackgeoduck.utils.NGGuidePageTransformer;
import com.xajeyu.stackgeoduck.utils.SpUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 13517 on 2017/5/25.
 */

public class GuideActivity extends Activity implements ViewPager.OnPageChangeListener{
    List<Integer> list = new ArrayList<>();
    private ViewPager viewpager;
    private NGGuidePageTransformer ngGuidePageTransformer;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guide);
        list.add(R.drawable.guide_1);
        list.add(R.drawable.guide_2);
        list.add(R.drawable.guide_3);

        ngGuidePageTransformer = new NGGuidePageTransformer();
        ngGuidePageTransformer.setCurrentItem(this,0,list);
        viewpager = (ViewPager) findViewById(R.id.viewpager);
        viewpager.setPageTransformer(true, ngGuidePageTransformer);
        viewpager.setAdapter(new GuideAdapter(list,getApplicationContext()));
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
    public class GuideAdapter extends PagerAdapter {

        private List<Integer> list;
        private Context context;
        public GuideAdapter(List<Integer> list, Context context){
            this.list = list;
            this.context = context;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View view = View.inflate(context, R.layout.item_imagview, null);
            ImageView imageView = (ImageView) view.findViewById(R.id.imageview);
            Button button = (Button) view.findViewById(R.id.bt_start);
            if (position == list.size() - 1 ){
                button.setVisibility(View.VISIBLE);
            }

            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SpUtil.putBoolean(context,"is_start",false);
                    startActivity(new Intent(context, MainActivity.class));
                    finish();
                }
            });
            imageView.setBackgroundResource(list.get(position));
            container.addView(view);
            return view;
        }
    }
}
