package com.xajeyu.stackgeoduck.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.jdsjlzx.recyclerview.LRecyclerView;
import com.github.jdsjlzx.recyclerview.LRecyclerViewAdapter;
import com.xajeyu.sdk.http.listener.RequestCallBack;
import com.xajeyu.sdk.imageloader.ImageLoaderManager;
import com.xajeyu.sdk.music.MusicPlaylist;
import com.xajeyu.stackgeoduck.R;
import com.xajeyu.stackgeoduck.activity.base.BaseActivity;
import com.xajeyu.stackgeoduck.adapter.MusicItemAdapter;
import com.xajeyu.stackgeoduck.network.http.AppNetwork;
import com.xajeyu.stackgeoduck.utils.DividerItemDecoration;

import java.util.ArrayList;

import static com.xajeyu.sdk.http.HttpConstant.Params.p;

/**
 * Created by 13517 on 2017/5/11.
 */

public class MusicActivity extends BaseActivity {
    /**
     * UI
     */
    private LRecyclerView recyclerView;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private LRecyclerViewAdapter lRecyclerViewAdapter;
    /**
     * DATA
     */
    private MusicPlaylist mMusicPlaylist;
    private ImageView imageView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music);
        initUI();
        initData();
    }

    /**
     * 初始化数据
     */
    private void initData() {
        int id = getIntent().getIntExtra("id", 0);
        AppNetwork.getInstance().RequestPlaylist(id, new RequestCallBack() {
            @Override
            public void onSuccess(Object response) {
                mMusicPlaylist = (MusicPlaylist) response;

                //填充数据适配器
                lRecyclerViewAdapter = new LRecyclerViewAdapter(new MusicItemAdapter(getApplicationContext(),mMusicPlaylist));
                //添加头布局
                View view = View.inflate(getApplicationContext(), R.layout.item_music_header, null);
                TextView titleNumber = (TextView) view.findViewById(R.id.item_music_number);
                // TODO  哈哈哈哈
                titleNumber.setText("共"+mMusicPlaylist.result.tracks.size()+"首");
                // 设置歌单背景图
                ImageLoaderManager.getInstance(getApplicationContext()).display(imageView,mMusicPlaylist.result.coverImgUrl);

               // collapsingToolbarLayout.setCollapsedTitleTextAppearance();
                // 设置歌单标题
                collapsingToolbarLayout.setTitle(mMusicPlaylist.result.name);
                lRecyclerViewAdapter.addHeaderView(view);


                //设置布局管理器为线性布局
                recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                //给recyclerView添加下划线
                //    recyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(),DividerItemDecoration.VERTICAL_LIST));
                //禁用下拉刷新功能
                recyclerView.setPullRefreshEnabled(false);
                //设置数据适配器
                recyclerView.setAdapter(lRecyclerViewAdapter);
            }

            @Override
            public void onFail(Object response) {

            }

            @Override
            public void onDownloadProgress(long progress) {

            }
        });
    }

    /**
     * 初始化UI
     */
    private void initUI() {
        recyclerView = (LRecyclerView) findViewById(R.id.rv_music_item);
        imageView = (ImageView) findViewById(R.id.iv_top_img);
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing);

    }
}
