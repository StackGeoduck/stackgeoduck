package com.xajeyu.stackgeoduck.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.xajeyu.stackgeoduck.R;

/**
 * Created by 13517 on 2017/5/4.
 */

public class NewsActivity extends Activity {

    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        webView = (WebView) findViewById(R.id.webview);

        String des = getIntent().getStringExtra("des"); //获取fragment中传过来的URL

        webView.getSettings().setJavaScriptEnabled(true);   //支持JavaScript功能

        webView.loadUrl(des);       //开始运行

        //在本app运行此URL，不跳转到浏览器
        webView.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return super.shouldOverrideUrlLoading(view, url);
            }
        });
    }
}
