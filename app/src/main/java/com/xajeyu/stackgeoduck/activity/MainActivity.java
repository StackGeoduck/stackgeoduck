package com.xajeyu.stackgeoduck.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.xajeyu.sdk.http.listener.RequestCallBack;
import com.xajeyu.sdk.imageloader.ImageLoaderManager;
import com.xajeyu.stackgeoduck.R;
import com.xajeyu.stackgeoduck.activity.base.BaseActivity;
import com.xajeyu.stackgeoduck.adapter.HomePagerAdapter;
import com.xajeyu.stackgeoduck.cinema.cinemafragment.mvp.CinemaFragment;
import com.xajeyu.stackgeoduck.libs.zxing.CaptureActivity;
import com.xajeyu.stackgeoduck.login.mvp.model.UserInfo;
import com.xajeyu.stackgeoduck.network.http.AppNetwork;
import com.xajeyu.stackgeoduck.view.fragment.ConnotationJokesFragment;
import com.xajeyu.stackgeoduck.view.fragment.HomeFragment;
import com.xajeyu.stackgeoduck.view.fragment.LiveFragment;
import com.xajeyu.stackgeoduck.view.fragment.NewsFragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import de.hdodenhof.circleimageview.CircleImageView;


public class MainActivity extends BaseActivity implements View.OnClickListener {
    // Fragment集合 存放ViewPager滑动的Fragment
    private List<Fragment> mFragments;
    // String集合 存放各个分页头
    private List<String> mTitles;
    // ViewPager用来显示Fragment
    private ViewPager mHomeViewPager;
    //TabLayout 分页
    private TabLayout mTabLayout;

    // 登录状态码
    private static final int LOGINSTATU = 1;
    // 用户头像
    private CircleImageView userIcon;
    // 用户名字
    private TextView userName;
    // 用户邮箱
    private TextView userEmail;
    private DrawerLayout mDrawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ViewInit();

    }

    /**
     * 初始化ToolBar
     */
    public void ToolbarInit() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        ImageView mAvatarImageView = (ImageView) findViewById(R.id.first_top_toolbar_avatar);
        ImageView mScanitImageView = (ImageView) findViewById(R.id.first_top_toolbar_scanit);
        mAvatarImageView.setOnClickListener(this);
        mScanitImageView.setOnClickListener(this);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            exitBy2Click();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private static Boolean isExit = false;

    /**
     * 双击返回按键退出程序
     */
    public void exitBy2Click() {
        Timer tExit = null;
        if (!isExit) {

            if (mDrawerLayout.isDrawerOpen(Gravity.START)){
                mDrawerLayout.closeDrawer(Gravity.START);
            } else {
                isExit = true;
                Toast.makeText(getApplicationContext(), "再按一次退出程序", Toast.LENGTH_SHORT).show();
                tExit = new Timer();
                tExit.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        isExit = false;
                    }
                }, 2000);
            }
        } else {
            finish();
            System.exit(0);
        }
    }

    /**
     * View初始化
     */
    private void ViewInit() {
        ToolbarInit();
        NavigationViewInit();
        DataInit();
        // 获取ViewPager控件
        mHomeViewPager = (ViewPager) findViewById(R.id.first_viewpager_data);
        // 获取TabLayout控件
        mTabLayout = (TabLayout) findViewById(R.id.first_top_bar_tablayout);
        // 将TabLayout设置文字信息
        mTabLayout.addTab(mTabLayout.newTab().setText(mTitles.get(0)));
        mTabLayout.addTab(mTabLayout.newTab().setText(mTitles.get(1)));
        mTabLayout.addTab(mTabLayout.newTab().setText(mTitles.get(2)));
        mTabLayout.addTab(mTabLayout.newTab().setText(mTitles.get(3)));
        mTabLayout.addTab(mTabLayout.newTab().setText(mTitles.get(4)));
        // 设置ViewPager缓存Fragment
        mHomeViewPager.setOffscreenPageLimit(5);
        // 获取一个HomePagerAdapter适配器
        HomePagerAdapter pagerAdapter = new HomePagerAdapter(getSupportFragmentManager(), mFragments, mTitles);
        // 为ViewPager设置适配器
        mHomeViewPager.setAdapter(pagerAdapter);
        // 设置TabLayout绑定的ViewPager
        mTabLayout.setupWithViewPager(mHomeViewPager);
        mTabLayout.setTabsFromPagerAdapter(pagerAdapter);
    }

    /**
     * 数据初始化
     */
    private void DataInit() {
        // 初始化Fragment集合
        mFragments = new ArrayList<>();
        mFragments.add(new HomeFragment());  // 首页Fragment
        mFragments.add(new ConnotationJokesFragment()); //  推荐Fragment
        mFragments.add(new LiveFragment()); // 追番Fragment
        mFragments.add(new NewsFragment()); // 分区Fragment
        mFragments.add(new CinemaFragment()); // 发现Fragment
        // 初始化ViewPager分页头信息集合
        mTitles = new ArrayList<>();
        mTitles.add((String) getResources().getText(R.string.home_text));
        mTitles.add((String) getResources().getText(R.string.connotation_jokes_text));
        mTitles.add((String) getResources().getText(R.string.his_text));
        mTitles.add((String) getResources().getText(R.string.partitioning_text));
        mTitles.add((String) getResources().getText(R.string.found_text));
    }


    /**
     * 初始化抽屉布局数据
     */
    private void NavigationViewInit() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view);
        navigationView.setItemIconTintList(null);
        // 获取侧滑栏头部
        View headerView = navigationView.getHeaderView(0);
        userIcon = (CircleImageView) headerView.findViewById(R.id.item_menu_user_icon);
        userName = (TextView) headerView.findViewById(R.id.item_menu_user_name);
        userEmail = (TextView) headerView.findViewById(R.id.item_menu_user_email);
        userIcon.setOnClickListener(this);
        userName.setOnClickListener(this);
        userEmail.setOnClickListener(this);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    // 点击设置与帮助
                    case R.id.menu_left_setting: {
                        startActivity(new Intent(getApplicationContext(), SettingActivity.class));
                        break;
                    }
                }
                return true;
            }
        });
    }

    /**
     * 按钮点击事件
     *
     * @param v
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.item_menu_user_icon:
                startActivityForResult(new Intent(getApplicationContext(), LoginActivity.class), LOGINSTATU);
                break;
            case R.id.first_top_toolbar_avatar://点击头像弹出侧边栏
                mDrawerLayout.openDrawer(GravityCompat.START);
                break;
            case R.id.first_top_toolbar_scanit://扫一扫
                //初始化扫描
                Intent intent = new Intent(this, CaptureActivity.class);
                startActivityForResult(intent, 0);
                break;
        }
    }

    /**
     * 接收Activity回传的值
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // 当登录成功时设置用户头像、以及一些用户信息
        if (resultCode == LOGINSTATU) {
            AppNetwork.getInstance().RequestUserInfo(data.getStringExtra("user_account"), new RequestCallBack() {
                @Override
                public void onSuccess(Object response) {
                    UserInfo userInfo = (UserInfo) response;
                    ImageLoaderManager.getInstance(getApplicationContext()).display(userIcon, userInfo.user_icon);
                    userEmail.setText(userInfo.user_mail);
                    userName.setText(userInfo.user_name);
                }

                @Override
                public void onFail(Object response) {

                }

                @Override
                public void onDownloadProgress(long progress) {

                }
            });
        }
    }
}
