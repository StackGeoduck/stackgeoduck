package com.xajeyu.stackgeoduck.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.xajeyu.stackgeoduck.R;
import com.xajeyu.stackgeoduck.activity.base.BaseActivity;

/**
 * @PackageName: com.xajeyu.stackgeoduck.activity
 * @FileName: SettingActivity.java
 * @CreationTime: 2017/5/15  16:18
 * @author: XajeYu
 * @E-mail: xajeyu@gmail.com
 * @function: 设置Activity
 */
public class SettingActivity extends BaseActivity{

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
    }
}
