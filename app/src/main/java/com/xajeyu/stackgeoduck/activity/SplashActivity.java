package com.xajeyu.stackgeoduck.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import com.xajeyu.stackgeoduck.R;
import com.xajeyu.stackgeoduck.utils.SpUtil;

/**
 * Created by 13517 on 2017/5/10.
 */

public class SplashActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //取消状态栏
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_splash);
        new Thread(){
            @Override
            public void run() {
                SystemClock.sleep(2000);

                if (!SpUtil.getBoolean(getApplicationContext(),"is_start",true)){
                    startActivity(new Intent(getApplicationContext(),MainActivity.class));
                    finish();
                }else {
                    startActivity(new Intent(getApplicationContext(),GuideActivity.class));
                    finish();
                }

            }
        }.start();
    }
}
