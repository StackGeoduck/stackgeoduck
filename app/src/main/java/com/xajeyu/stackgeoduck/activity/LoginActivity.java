package com.xajeyu.stackgeoduck.activity;

import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.xajeyu.sdk.utils.Tool;
import com.xajeyu.stackgeoduck.R;
import com.xajeyu.stackgeoduck.login.mvp.presenter.LoginPresenter;
import com.xajeyu.stackgeoduck.login.mvp.view.LoginView;
import com.xajeyu.stackgeoduck.view.customview.CustomVideoView;

import java.util.List;

import static com.xajeyu.stackgeoduck.R.id.ll_register_two;

/**
 * @PackageName: com.xajeyu.stackgeoduck.activity
 * @FileName: LoginActivity.java
 * @CreationTime: 2017/5/12  10:54
 * @author: XajeYu
 * @E-mail: xajeyu@gmail.com
 * @类说明: 登陆、注册功能模块
 */

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, Validator.ValidationListener, LoginView {

    private static final String TAG = "LoginActivity";
    /**
     * UI
     */
    private CustomVideoView videoview;
    private TextView tvRegister;
    private LinearLayout llLogin;
    private LinearLayout llRegisterTwo;
    private Button login_bt;
    private TextView tvRegisterTwo;
    private Button registerBtTwo;
    @Email
    @NotEmpty
    private EditText mUserAccountEdt; // 账号输入框
    @Password
    @NotEmpty
    private EditText mUserPwdEdt;  // 密码输入框
    @ConfirmPassword
    @NotEmpty
    private EditText mUserPwdEdts; // 密码输入框重复
    private ImageView imageView;
    /**
     * Data
     */
    private static int LoginAndRegister;
    private static final int LOGINSTATU = 1;
    private static final int REGISTERSTATU = 2;
    private Validator validator;

    private LoginPresenter loginPresenter;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        loginPresenter = new LoginPresenter(this);
        initValidator();
        initView();


    }

    /**
     * 表单验证器初始化
     */
    private void initValidator() {
        validator = new Validator(this);
        validator.setValidationListener(this);
    }

    /**
     * 初始化view
     */
    private void initView() {
        imageView = (ImageView) findViewById(R.id.image_sss);
        // 设置控件层级最上级
        imageView.bringToFront();
        //加载视频资源控件
        videoview = (CustomVideoView) findViewById(R.id.videoview);

        LinearLayout manager = (LinearLayout) findViewById(R.id.login_qwer);
        manager.bringToFront();

        login_bt = (Button) findViewById(R.id.login_bt);
        mUserPwdEdts = (EditText) findViewById(R.id.login_et_password_two);
        tvRegister = (TextView) findViewById(R.id.tv_register);
        mUserAccountEdt = (EditText) findViewById(R.id.login_et_user);
        mUserPwdEdt = (EditText) findViewById(R.id.login_et_password);
        tvRegisterTwo = (TextView) findViewById(R.id.tv_register_two);

        llLogin = (LinearLayout) findViewById(R.id.ll_login);
        llRegisterTwo = (LinearLayout) findViewById(ll_register_two);

        registerBtTwo = (Button) findViewById(R.id.register_bt_two);

        login_bt.getBackground().setAlpha(200);
        registerBtTwo.getBackground().setAlpha(200);

        // 设置登录按钮监听
        login_bt.setOnClickListener(this);
        tvRegister.setOnClickListener(this);
        tvRegisterTwo.setOnClickListener(this);
        registerBtTwo.setOnClickListener(this);


        //设置播放加载路径
        videoview.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.login));
        videoview.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setOnInfoListener(new MediaPlayer.OnInfoListener() {
                    @Override
                    public boolean onInfo(MediaPlayer mp, int what, int extra) {
                        if (what == MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START) {
                            // video 视屏播放的时候把背景设置为透明
                            videoview.setBackgroundColor(Color.TRANSPARENT);

                            return true;
                        }
                        return false;
                    }
                });
            }
        });
        //播放
        videoview.start();

        //循环播放
        videoview.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                videoview.start();
            }
        });

        // 线程休眠已修复加载视频BUG
        new Thread() {
            @Override
            public void run() {
                SystemClock.sleep(700);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        imageView.setVisibility(View.GONE);
                    }
                });

            }
        }.start();


    }

    //返回重启加载
    @Override
    protected void onRestart() {
        initView();
        super.onRestart();
    }

    @Override
    protected void onStop() {
        videoview.stopPlayback();
        super.onStop();
    }

    /**
     * 按钮监听
     *
     * @param v
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            // 返回登录
            case R.id.tv_register_two: {
                showLoginForm(true);
                break;
            }
            // 注册按钮
            case R.id.tv_register: {
                // 显示注册表单
                showRegisterForm(true);
                break;
            }
            // 登录按钮
            case R.id.login_bt: {
                // 表单验证并且登录
                // 设置登录状态
                LoginAndRegister = LOGINSTATU;
                validator.validate();
                break;
            }
            // 注册按钮
            case R.id.register_bt_two: {
                Toast.makeText(getApplicationContext(), "注册成功,现在可以登录了!", Toast.LENGTH_LONG).show();
                // 设置注册状态
                LoginAndRegister = REGISTERSTATU;
                validator.validate();
                break;
            }
        }

    }

    /**
     * 显示注册界面
     *
     * @param isShowRegisterForm 开关
     */
    private void showRegisterForm(boolean isShowRegisterForm) {
        if (isShowRegisterForm) {
            mUserPwdEdts.setVisibility(View.VISIBLE);
            llRegisterTwo.setVisibility(View.VISIBLE);
            llLogin.setVisibility(View.GONE);
            tvRegister.setVisibility(View.GONE);
            tvRegisterTwo.setVisibility(View.VISIBLE);
            mUserAccountEdt.setText("");
            mUserPwdEdt.setText("");
        }
    }

    /**
     * 显示登录按钮
     *
     * @param isShowLoginForm 开关
     */
    private void showLoginForm(boolean isShowLoginForm) {
        if (isShowLoginForm) {
            llRegisterTwo.setVisibility(View.GONE);
            mUserPwdEdts.setVisibility(View.GONE);
            llLogin.setVisibility(View.VISIBLE);
            tvRegister.setVisibility(View.VISIBLE);
            tvRegisterTwo.setVisibility(View.GONE);
            mUserAccountEdt.setText("");
            mUserPwdEdt.setText("");
            mUserPwdEdts.setText("");
        }

    }

    /**
     * 表单验证成功
     */
    @Override
    public void onValidationSucceeded() {
        // 登录状态
        if (LoginAndRegister == 1) {
            loginPresenter.Login();
        } else if (LoginAndRegister == 2) {
            // 注册状态
            loginPresenter.Register();
        }
    }

    /**
     * 表单验证失败
     *
     * @param errors 错误的View集合
     */
    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            System.out.println("-----asd----->" + error);
        }
    }

    /**
     * 登录成功
     */
    @Override
    public void onLoginSuccess() {
        // 做View显示
        Toast.makeText(getApplicationContext(), "登录成功！", Toast.LENGTH_LONG).show();
        // 记录登录状态
        if (Tool.getInstance(getApplicationContext()).setCurrentlyLogged(getUserAccount())) {
            Bundle bundle = new Bundle();
            bundle.putString("user_account", getUserAccount());
            // 设置当前用户数据到MainActivity
            setResult(LOGINSTATU, getIntent().putExtras(bundle));
            // 取消当前页面
            finish();
        }
    }

    /**
     * 登录失败
     *
     * @param msg
     */
    @Override
    public void onLoginFailure(Object msg) {
        Toast.makeText(getApplicationContext(), "登录失败：" + msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onRegisterSuccess() {
        Toast.makeText(getApplicationContext(), "注册成功,现在可以登录了!", Toast.LENGTH_LONG).show();
        // 显示登录表单
        showLoginForm(true);
        loginPresenter.Login();
    }

    @Override
    public void onRegisterFailure(Object msg) {
        Toast.makeText(getApplicationContext(), "注册失败，请检查或联系管理员!", Toast.LENGTH_LONG).show();
    }

    @Override
    public String getUserAccount() {
        return mUserAccountEdt.getText().toString();
    }

    @Override
    public String getUserPassword() {
        return mUserPwdEdt.getText().toString();
    }

}
