package com.xajeyu.stackgeoduck.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.github.jdsjlzx.recyclerview.LRecyclerView;
import com.github.jdsjlzx.recyclerview.LRecyclerViewAdapter;
import com.google.gson.Gson;
import com.xajeyu.sdk.http.listener.RequestCallBack;
import com.xajeyu.sdk.imageloader.ImageLoaderManager;
import com.xajeyu.stackgeoduck.R;
import com.xajeyu.stackgeoduck.cinema.adapter.CinemaDetailsAdapter;
import com.xajeyu.stackgeoduck.cinema.adapter.CinemaDetailsMediaAdapter;
import com.xajeyu.stackgeoduck.cinema.bean.CinemaDetailsInfo;
import com.xajeyu.stackgeoduck.cinema.cinemaImg.mvp.ImgActivity;
import com.xajeyu.stackgeoduck.network.http.AppNetwork;
import com.xajeyu.stackgeoduck.utils.DividerItemDecoration;
import com.xajeyu.stackgeoduck.view.customview.SetCanNotScrollLayoutManager;

import jp.wasabeef.glide.transformations.BlurTransformation;


/**
 * Created by e on 2017/5/5.
 */

public class CinemaActivity extends AppCompatActivity {
    /**
     * 短评
     */
    private LRecyclerView mCommentaryLRecyclerView;
    /**
     * 网络请求的数据
     */
    public CinemaDetailsInfo cinemaDetailsInfo;
    /**
     * head高度
     */
    private int height;
    /**
     * 中文电影名
     */
    private TextView mChineseName;
    /**
     * 英文电影名
     */
    private TextView mEnglishName;
    /**
     * 顶部渐渐透明部分
     */
    private Toolbar toolbar;
    /**
     * 电影图标
     */
    private ImageView img;
    /**
     * 观众评分
     */
    private TextView mAudienceRating;
    /**
     * 得分
     */
    private TextView mScore;
    /**
     * 评分人数量
     */
    private TextView mQuantity;
    /**
     * 电影类型
     */
    private TextView mType;
    /**
     * 地区和电影时长
     */
    private TextView mAreaWithDuration;
    /**
     * 上映时间
     */
    private TextView mReleaseDate;
    /**
     * 高斯模糊背景
     */
    private ImageView bgImg;
    /**
     * 电影简介收缩图片
     */
    private ImageView shrinkImg;
    /**
     * 电影简介收缩文字
     */
    private TextView shrinkText;
    /**
     * 判断当前是收起来的状态还是展开
     */
    private boolean is;
    /**
     * 包裹电影简介的布局
     */
    private LinearLayout shrinkLayout;
    /**
     * 媒体库(图片)
     */
    private RecyclerView mMediavLRecyclerView;
    private LRecyclerViewAdapter mLRecyclerViewAdapter;
    private TextView mToolbarName;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cinema);
        initData();
        initView();

    }

    private void initData() {
        Intent intent = getIntent();
        String url = intent.getStringExtra("url");
        setResult(20, intent.putExtra("vd", "123"));
        AppNetwork.getInstance().RequestCinemaDetails(url, new RequestCallBack() {
            @Override
            public void onSuccess(Object response) {
                cinemaDetailsInfo = (CinemaDetailsInfo) response;
                setCommentaryAdapter(cinemaDetailsInfo);
                setMediaAdapter(cinemaDetailsInfo);
                SetHeadData(cinemaDetailsInfo);
            }

            @Override
            public void onFail(Object response) {

            }

            @Override
            public void onDownloadProgress(long progress) {

            }
        });
    }

    private void initView() {
        initHeadView();
        mMediavLRecyclerView = (RecyclerView) findViewById(R.id.cinema_details_media_lrv);
        mCommentaryLRecyclerView = (LRecyclerView) findViewById(R.id.cinema_details_commentary_lrv);
        bgImg = (ImageView) findViewById(R.id.bg_Img);
        toolbar = (Toolbar) findViewById(R.id.cinema_top_toolbar);
        height = toolbar.getLayoutParams().height;
        NestedScrollView scrollView = (NestedScrollView) findViewById(R.id.cinema_details_scrollview);
        //设置滑动Toolbar慢慢显示
        scrollView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY <= 0) {   //设置标题的背景颜色
                    toolbar.setBackgroundColor(Color.TRANSPARENT);
                    mToolbarName.setTextColor(Color.TRANSPARENT);
                } else if (scrollY > 0 && scrollY <= height) { //滑动距离小于banner图的高度时，设置背景和字体颜色颜色透明度渐变
                    float scale = (float) scrollY / height;
                    System.out.println(scrollY + "-----" + height);
                    float alpha = (255 * scale);
                    toolbar.setBackgroundColor(Color.argb((int) alpha, 0, 255, 255));
                    mToolbarName.setTextColor(Color.argb((int) alpha, 255, 255, 255));
                } else {    //滑动到banner下面设置普通颜色
                    //toolbar.setBackgroundColor(Color.parseColor("#DD403B"));
                    mToolbarName.setTextColor(Color.GRAY);
                }
            }
        });
    }

    /**
     * 初始化头布局View
     */
    private void initHeadView() {
        mToolbarName = (TextView) findViewById(R.id.cinema_details_toolbar_name);
        img = (ImageView) findViewById(R.id.cinema_details_head_img);
        mChineseName = (TextView) findViewById(R.id.cinema_details_head_chinese_movie_name);
        mEnglishName = (TextView) findViewById(R.id.cinema_details_head_english_movie_name);
        mAudienceRating = (TextView) findViewById(R.id.cinema_details_head_audience_rating);
        mScore = (TextView) findViewById(R.id.cinema_details_head_score);
        mQuantity = (TextView) findViewById(R.id.cinema_details_head_quantity);
        mType = (TextView) findViewById(R.id.cinema_details_head_type);
        mAreaWithDuration = (TextView) findViewById(R.id.cinema_details_head_area_with_duration);
        mReleaseDate = (TextView) findViewById(R.id.cinema_details_head_release_date);
    }

    /**
     * 初始化头布局数据
     *
     * @param cinemaDetailsInfo
     */
    public void SetHeadData(CinemaDetailsInfo cinemaDetailsInfo) {
        ImageLoaderManager.getInstance(getApplicationContext()).display(img, cinemaDetailsInfo.data.MovieDetailModel.img);
        //设置高斯模糊
        Glide.with(CinemaActivity.this)
                .load(cinemaDetailsInfo.data.MovieDetailModel.img)
                .bitmapTransform(new BlurTransformation(this, 50))
                .into(bgImg);
        mChineseName.setText(cinemaDetailsInfo.data.MovieDetailModel.nm);
        mScore.setText(cinemaDetailsInfo.data.MovieDetailModel.sc + "");
        mQuantity.setText("(" + (int) cinemaDetailsInfo.data.MovieDetailModel.snum + "人评)");
        mType.setText(cinemaDetailsInfo.data.MovieDetailModel.cat);
        mAreaWithDuration.setText(cinemaDetailsInfo.data.MovieDetailModel.src + " / " + (int) cinemaDetailsInfo.data.MovieDetailModel.dur + "分钟");
        mReleaseDate.setText(cinemaDetailsInfo.data.MovieDetailModel.rt);
        mToolbarName.setText(cinemaDetailsInfo.data.MovieDetailModel.nm);
        initShrink();
    }

    /**
     * 电影简介收缩布局初始化
     */
    public void initShrink() {
        is = true;
        shrinkLayout = (LinearLayout) findViewById(R.id.cinema_details_shrink_llayout);
        shrinkImg = (ImageView) findViewById(R.id.cinema_details_introduction_img);
        shrinkText = (TextView) findViewById(R.id.cinema_details_introduction_text);
        //将dra中的<p>和</p>替换成空字符串
        String dra = cinemaDetailsInfo.data.MovieDetailModel.dra;
        dra = dra.replace("<p>", "");
        dra = dra.replace("</p>", "");
        shrinkText.setText(dra);
        shrinkText.setMaxLines(3);
        shrinkLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (is) {
                    setRotateAinimation(shrinkImg, 0f, 180f);
                    shrinkText.setMaxLines(Integer.MAX_VALUE);
                } else {
                    setRotateAinimation(shrinkImg, 180f, 0f);
                    shrinkText.setMaxLines(3);
                }
                is = !is;
            }
        });
    }

    public void setMediaAdapter(final CinemaDetailsInfo cinemaDetailsInfo) {
        CinemaDetailsMediaAdapter mCinemaDetailsMediaAdapter = new CinemaDetailsMediaAdapter(getApplicationContext(), cinemaDetailsInfo);
        //设置一行,横向
        mMediavLRecyclerView.setLayoutManager(new StaggeredGridLayoutManager(1,
                StaggeredGridLayoutManager.HORIZONTAL));
        mMediavLRecyclerView.setAdapter(mCinemaDetailsMediaAdapter);
        mCinemaDetailsMediaAdapter.setmOnItemClickListener(new CinemaDetailsMediaAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int postion) {
                Intent intent = new Intent(getApplicationContext(), ImgActivity.class);
                //把对象转成json字符串
                intent.putExtra("url", new Gson().toJson(cinemaDetailsInfo));
                Bundle bundle = new Bundle();
                bundle.putInt("postion", postion);
                intent.putExtra("bundle", bundle);
                startActivity(intent);
            }
        });
    }

    public void setCommentaryAdapter(CinemaDetailsInfo cinemaDetailsInfo) {
        CinemaDetailsAdapter cinemaDetailsAdapter = new CinemaDetailsAdapter(getApplicationContext(), cinemaDetailsInfo);
        mLRecyclerViewAdapter = new LRecyclerViewAdapter(cinemaDetailsAdapter);
        SetCanNotScrollLayoutManager setCanNotScrollLayoutManager = new SetCanNotScrollLayoutManager(getApplicationContext());
        //禁止LRecyclerView滚动
        setCanNotScrollLayoutManager.setScrollEnabled(false);
        //设置布局管理器
        mCommentaryLRecyclerView.setLayoutManager(setCanNotScrollLayoutManager);
        //添加分割线
        mCommentaryLRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL_LIST));
        //设置适配器
        mCommentaryLRecyclerView.setAdapter(mLRecyclerViewAdapter);
        //设置上拉加载和下拉刷新是否可用
        mCommentaryLRecyclerView.setPullRefreshEnabled(false);
        mCommentaryLRecyclerView.setLoadMoreEnabled(false);
        //添加脚页
//        mLRecyclerViewAdapter.addFooterView(View.inflate(getApplicationContext(),R.layout.item_news_foot,null));
    }

    /**
     * 旋转动画
     *
     * @param view  要旋转的View
     * @param start 动画开始位置
     * @param end   动画结束位置
     */
    public void setRotateAinimation(View view, float start, float end) {
        Animation animation = new RotateAnimation(start, end, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        animation.setFillAfter(true);
        animation.setDuration(100);
        view.startAnimation(animation);
    }
}
