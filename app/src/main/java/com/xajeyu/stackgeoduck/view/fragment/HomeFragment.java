package com.xajeyu.stackgeoduck.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.xajeyu.sdk.http.listener.RequestCallBack;
import com.xajeyu.sdk.music.MusicDataObj;
import com.xajeyu.stackgeoduck.R;
import com.xajeyu.stackgeoduck.adapter.HomeAppIndexAdapter;
import com.xajeyu.stackgeoduck.model.dataobj.live.LiveAppIndexInfo;
import com.xajeyu.stackgeoduck.network.http.AppNetwork;
import com.xajeyu.stackgeoduck.view.fragment.Base.BaseFragment;
import com.youth.banner.Banner;

/**
 * @PackageName: com.xajeyu.bilibili.view.fragment
 * @FileName: HomeFragment.java
 * @CreationTime: 2017/4/13  14:26
 * @author: XajeYu
 * @E-mail: xajeyu@gmail.com
 * @function: 首页Fragment
 */
public class HomeFragment extends BaseFragment implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {
    /**
     * UI部分
     */
    private View view;
    private MusicDataObj mMusicObj; // 网易云数据
    private HomeAppIndexAdapter homeAppIndexAdapter;
    private Banner mBanner;
    public static Context mContext;
    private LinearLayout mFocusLinearLayout;
    private LinearLayout mCenterLinearLayout;
    private LinearLayout mVideoLinearLayout;
    private LinearLayout mSearchLinearLayout;
    private LinearLayout mClassificationLinearLayout;
    public static String MVURL = "http://music.163.com/#/mv?id=";
    //下拉刷新控件
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerView;
    private LiveAppIndexInfo live = new LiveAppIndexInfo();

    @Override
    public void initData() {
        System.out.println("首页懒加载");
    }

    /**
     * 为HomeFragment添加视图
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);
        mContext = getContext();
        GetData();
        ViewInit();
        return view;
    }

    private void ViewInit() {
        // 获取控件信息
        mRecyclerView = (RecyclerView) view.findViewById(R.id.fragment_home_recycle);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mRecyclerView.setHasFixedSize(true);
        // 列数为两列


        // 设置监听
        // BannerBuilder();
        SwipeRefreshLayoutInit();
    }

    /**
     * 数据初始化
     */
    private void GetData() {
        AppNetwork.getInstance().RequestMusicApi(new RequestCallBack() {
            @Override
            public void onSuccess(Object response) {
                homeAppIndexAdapter = new HomeAppIndexAdapter(getContext(), response);

                // 在RecyclerView 添加头View
                setHeaderView(mRecyclerView);

                //私人FM，云音乐热歌等等的监听。
                homeAppIndexAdapter.setOnItemClickLitener(new HomeAppIndexAdapter.OnItemClickLitener() {
                    @Override
                    public void onItemClick(View view, int position) {

                    }
                });
                mRecyclerView.setAdapter(homeAppIndexAdapter);

            }

            @Override
            public void onFail(Object response) {

            }

            @Override
            public void onDownloadProgress(long progress) {

            }
        });

    }


    /**
     * 监听器
     *
     * @param v
     */
    @Override
    public void onClick(View v) {

    }

    private void setHeaderView(RecyclerView view) {
        View inflate = LayoutInflater.from(getContext()).inflate(R.layout.item_home_fragment_header, view, false);
        homeAppIndexAdapter.setHeaderView(inflate);
    }

    /**
     * 下拉刷新控件初始化
     */
    private void SwipeRefreshLayoutInit() {
        //获取控件
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
        //设置监听
        mSwipeRefreshLayout.setOnRefreshListener(this);
        //刷新时的颜色变化
        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright, android.R.color.holo_green_light,
                android.R.color.holo_orange_light, android.R.color.holo_red_light);


    }

    /**
     * 控件SwipeRefreshLayout刷新方法
     */
    @Override
    public void onRefresh() {
        // 判断是否刷新状态
        if (mSwipeRefreshLayout.isRefreshing()) {
            // 获取最新数据后刷新View并取消刷新状态
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }
}
