package com.xajeyu.stackgeoduck.view.fragment.Base;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * @PackageName: com.xajeyu.bilibili.view.fragment.Base
 * @FileName: BaseFragment.java
 * @CreationTime: 2017/4/13  14:27
 * @author: XajeYu
 * @E-mail: xajeyu@gmail.com
 * @function:
 */
public abstract class BaseFragment extends Fragment {
    public static Context mContext;
    /**
     * Fragment是否可见
     */
    private boolean isVisible;
    /**
     * onCreateView方法已经调用完毕
     */
    private boolean isPrepared;
    /**
     * 确保ViewPager来回切换时BaseFragment的initData方法不会被重复调用
     */
    private boolean isFirst = true;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mContext = getContext();
        return super.onCreateView(inflater, container, savedInstanceState);
    }



    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        isPrepared = true;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (getUserVisibleHint()) {
            isVisible = true;
            lazyLoad();
        } else {
            isVisible = false;
        }
    }

    public void lazyLoad() {
        if (!isPrepared || !isVisible || !isFirst) {
            return;
        }
        initData();
        isFirst = false;
    }

    public abstract void initData();
}
