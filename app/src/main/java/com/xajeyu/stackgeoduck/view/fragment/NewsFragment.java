package com.xajeyu.stackgeoduck.view.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.jdsjlzx.interfaces.OnItemClickListener;
import com.github.jdsjlzx.interfaces.OnLoadMoreListener;
import com.github.jdsjlzx.recyclerview.LRecyclerView;
import com.github.jdsjlzx.recyclerview.LRecyclerViewAdapter;
import com.google.gson.Gson;
import com.xajeyu.sdk.news.NewsBean;
import com.xajeyu.stackgeoduck.R;
import com.xajeyu.stackgeoduck.activity.NewsActivity;
import com.xajeyu.stackgeoduck.adapter.NewsAdapter;
import com.xajeyu.stackgeoduck.adapter.NewsHeaderAdapter;
import com.xajeyu.stackgeoduck.network.api.news.NewsApi;
import com.xajeyu.stackgeoduck.news.mvp.presenter.NewsPresenter;
import com.xajeyu.stackgeoduck.news.mvp.view.INewsView;
import com.xajeyu.stackgeoduck.utils.DividerItemDecoration;
import com.xajeyu.stackgeoduck.view.fragment.Base.BaseFragment;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * @PackageName: com.xajeyu.bilibili.view.fragment
 * @FileName: NewsFragment.java
 * @CreationTime: 2017/4/13  14:30
 * @author: XajeYu
 * @E-mail: xajeyu@gmail.com
 * @function: 新闻Framgnet
 */
public class NewsFragment extends BaseFragment implements INewsView{

    List<NewsBean.StoriesBean> MaxList = new ArrayList<>();
    public static Context mContext;
    private LRecyclerView lRecyclerView;
    public NewsBean newsBean;
    private SwipeRefreshLayout swipeRefreshLayout;
    private LRecyclerViewAdapter lRecyclerViewAdapter;
    private ArrayList<NewsBean.StoriesBean> list;
    private NewsPresenter newsPresenter;
    private String nowTime;
    int s = 1;

    public String getNowTime(){
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        String t=format.format(new Date());
        return t;
    }
    public String getYesteDayTime(int s){
        Date dNow = new Date();   //当前时间
        Date dBefore = new Date();

        Calendar calendar = Calendar.getInstance(); //得到日历
        calendar.setTime(dNow);//把当前时间赋给日历
        calendar.add(Calendar.DAY_OF_MONTH, -s);  //设置为前一天
        dBefore = calendar.getTime();   //得到前一天的时间


        SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMdd"); //设置时间格式
        String defaultStartDate = sdf.format(dBefore);    //格式化前一天
        String defaultEndDate = sdf.format(dNow); //格式化当前时间
        return defaultStartDate;

    }
    @Override
    public void initData(){
        initgetData();
    }

    public void initgetData() {
        nowTime = getNowTime();

        //定义一个全局可使用的上下文对象
        mContext = getContext();
        newsPresenter = new NewsPresenter(this);

        //请求顶部viewpager的数据
        HttpTop(NewsApi.NEWSURL);

     //   newsPresenter.setDataToView();

        //请求新闻网络
        HttpRequest(NewsApi.NEWSURL);

        //设置recyclerview
        setlRecyclerView();

        //设置上拉加载的监听
        setOnLoadMoreListener();



        //下拉刷新后的逻辑处理
        setSwipeRefreshLayout();
    }

    public void setlRecyclerView(){
        //禁用框架自带的下拉刷新功能
        lRecyclerView.setPullRefreshEnabled(false);


        //设置lRecyclerView的数据管理器为线性布局
        lRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        //添加下划线
        lRecyclerView.addItemDecoration(new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL_LIST));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_paritioning, container, false);
        lRecyclerView = (LRecyclerView) view.findViewById(R.id.lrv);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.sr_news);

        //设置数据适配器
        lRecyclerViewAdapter = new LRecyclerViewAdapter(new NewsAdapter(MaxList));

        return view;
    }
        /* lRecyclerViewAdapter.setOnItemClickListener(new OnItemClickListener() {
        @Override
        public void onItemClick(View view, int position) {
            String des = NewsApi.URL + old.stories.get(position).id;
            Intent intent = new Intent(mContext, NewsActivity.class);
            intent.putExtra("des", des);
            startActivity(intent);
        }
    });*/

    /**
     * 设置上拉加载的监听
     */


    public void setOnLoadMoreListener() {
        lRecyclerView.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {

                String yesteDayTime = getYesteDayTime(s);
                s++;
                nowTime = yesteDayTime;
                HttpRequest(NewsApi.TIMEURL+nowTime);

            }
        });
    }

    /**
     * 请求网络新闻数据
     */
    public void HttpRequest(String url) {

        OkHttpClient okHttpClient = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .build();

        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                String string = response.body().string();
                 list = GosnJiexi(string);

                lRecyclerViewAdapter.setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {

                        String des = NewsApi.URL + MaxList.get(position).id;
                        Intent intent = new Intent(mContext, NewsActivity.class);
                        intent.putExtra("des", des);
                        startActivity(intent);
                    }
                });

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        lRecyclerView.refreshComplete(10);
                        lRecyclerViewAdapter.notifyDataSetChanged();
                        lRecyclerView.setAdapter(lRecyclerViewAdapter);

                    }
                });
            }

        });


    }


    public void HttpTop(String url){
        OkHttpClient okHttpClient = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .build();
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String string = response.body().string();
                Gson g = new Gson();
                NewsBean topStoriesBean = g.fromJson(string, NewsBean.class);

                View HeaderView = View.inflate(mContext, R.layout.item_news_header, null);

                //给头布局viewpager设置数据适配器

                ViewPager viewPager = (ViewPager) HeaderView.findViewById(R.id.vp_header);


                lRecyclerViewAdapter.addHeaderView(HeaderView);

                viewPager.setAdapter(new NewsHeaderAdapter(topStoriesBean));

            }
        });
    }


    private ArrayList<NewsBean.StoriesBean> GosnJiexi(String json) {
        Gson gson = new Gson();
        newsBean = gson.fromJson(json, NewsBean.class);
        ArrayList<NewsBean.StoriesBean> list = new ArrayList<>();
        for (int i = 0; i< newsBean.stories.size(); i++){
            list.add(newsBean.stories.get(i));
        }
        MaxList.addAll(list);
        return list;
    }

    /**
     * 下拉刷新后的逻辑处理
     */
    public void setSwipeRefreshLayout() {
        //设置下拉刷新的颜色
        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright, android.R.color.holo_green_light,
                android.R.color.holo_orange_light, android.R.color.holo_red_light);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        HttpRequest(NewsApi.NEWSURL);
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }, 2000);
            }
        });
    }

    @Override
    public void onSuccess(Object msg) {

    }

    @Override
    public void onFail(Object msg) {

    }

    @Override
    public void getItemPosition(String msg) {
        Intent intent = new Intent(getViewContext(), NewsActivity.class);
        intent.putExtra("des", msg);
        startActivity(intent);
    }

    @Override
    public Context getViewContext() {
        return getContext();
    }

    @Override
    public LRecyclerView getLRecyclerView() {
        return lRecyclerView;
    }
}
