package com.xajeyu.stackgeoduck.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.xajeyu.sdk.http.listener.RequestCallBack;
import com.xajeyu.stackgeoduck.R;
import com.xajeyu.stackgeoduck.adapter.ConnotationJokesAdapter;
import com.xajeyu.stackgeoduck.network.http.AppNetwork;
import com.xajeyu.stackgeoduck.utils.DividerItemDecoration;
import com.xajeyu.stackgeoduck.view.fragment.Base.BaseFragment;

/**
 * @PackageName: com.xajeyu.bilibili.view.fragment
 * @FileName: ConnotationJokesFragment.java
 * @CreationTime: 2017/4/13  14:26
 * @author: XajeYu
 * @E-mail: xajeyu@gmail.com
 * @function: 内涵段子Framgnet
 */
public class ConnotationJokesFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {
    private  View view;
    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_connotation_jokes,container,false);
      //  ViewInit();
        return view;
    }
    @Override
    public void initData() {
        System.out.println("段子懒加载");
        ViewInit();
    }
    /**
     * 初始化View
     */
    private void ViewInit(){
        // 初始化Adapter
        mRecyclerView = (RecyclerView) view.findViewById(R.id.fragment_connotation_jokes_recycler);

        GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 1, GridLayoutManager.VERTICAL, false);//设置为一个3列的纵向网格布局

        mRecyclerView.setLayoutManager(layoutManager);

        mRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(),DividerItemDecoration.VERTICAL_LIST));
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.fragment_connotation_jokes_swipe_refresh_layout);
        getData();

        SwipeRefreshLayoutInit();
    }

    /**
     * 下拉刷新控件初始化
     */
    private void SwipeRefreshLayoutInit(){
        //设置监听
        mSwipeRefreshLayout.setOnRefreshListener(this);
        //刷新时的颜色变化
        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright, android.R.color.holo_green_light,
                android.R.color.holo_orange_light, android.R.color.holo_red_light);


    }
    /**
     * 获取数据
     */
    private void getData(){
        /**
         * 网络请求
         */
        AppNetwork.getInstance().RequestNeiHanApi(new RequestCallBack() {
            @Override
            public void onSuccess(Object response) {
                // 设置数据适配器
                mRecyclerView.setAdapter(new ConnotationJokesAdapter(getContext(),response));
                // 取消刷新
                mSwipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFail(Object response) {

            }

            @Override
            public void onDownloadProgress(long progress) {

            }
        });

    }
    /**
     * 下拉控件刷新时
     */
    @Override
    public void onRefresh() {
        if (mSwipeRefreshLayout.isRefreshing()){
            getData();
        }
    }
}
