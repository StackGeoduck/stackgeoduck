package com.xajeyu.stackgeoduck.view.customview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.xajeyu.stackgeoduck.R;

import static android.view.Gravity.CENTER;

/**
 * @PackageName: com.xajeyu.stackgeoduck.view.customview
 * @FileName: SettingItemCustomView.java
 * @CreationTime: 2017/5/21  10:47
 * @author: XajeYu
 * @E-mail: xajeyu@gmail.com
 * @function: 设置界面横条View
 */
public class SettingItemCustomView extends RelativeLayout{
    /**
     * Data
     */
    private int mBackground;
    private String mLeftText;
    private String mRightText;
    private float mTextSize;
    private int mTextColor;
    private Drawable mRightImg;

    private TextView mLeftTextView;
    private TextView mRightTextView;
    private ImageView mRightImageView;
    private View mView;
    private boolean mIsAddHorizontalLine; // 判断是否添加横线

    private LayoutParams mLayoutParams;


    public SettingItemCustomView(Context context) {
        super(context);
    }

    public SettingItemCustomView(final Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        // 获取值
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.SettingItemCustomView);
        // 获取值
        mBackground = typedArray.getColor(R.styleable.SettingItemCustomView_SettingBackground,0);
        mLeftText = typedArray.getString(R.styleable.SettingItemCustomView_leftText);
        mRightText = typedArray.getString(R.styleable.SettingItemCustomView_rightText);
        mTextSize = typedArray.getDimension(R.styleable.SettingItemCustomView_textSize,15);
        mTextColor = typedArray.getColor(R.styleable.SettingItemCustomView_textColor,0);
        mRightImg = typedArray.getDrawable(R.styleable.SettingItemCustomView_rightImage);
        mIsAddHorizontalLine = typedArray.getBoolean(R.styleable.SettingItemCustomView_isAddHorizontalLine,true);
        // 创建TextView
        mLayoutParams = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mLeftTextView = new TextView(context);
        mLeftTextView.setText(mLeftText);
        mLeftTextView.setTextSize(mTextSize);
        mLeftTextView.setGravity(CENTER);
        mLeftTextView.setLayoutParams(mLayoutParams);
        // 创建右侧箭头ImageView
        mLayoutParams = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mLayoutParams.addRule(ALIGN_PARENT_RIGHT,TRUE);
        mRightImageView = new ImageView(context);
        mRightImageView.setPadding(10,0,30,0);
        mRightImageView.setImageDrawable(mRightImg);
        mRightImageView.setForegroundGravity(CENTER_VERTICAL);
        mRightImageView.setLayoutParams(mLayoutParams);

        // 横线
        mView = new View(context);
        mLayoutParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,1);
        mLayoutParams.addRule(ALIGN_PARENT_BOTTOM,TRUE);
        mView.setBackgroundColor(getResources().getColor(R.color.uvv_gray));
        mView.setLayoutParams(mLayoutParams);

        // 横向
        setPadding(50,0,0,0);
        setGravity(CENTER);
        addView(mLeftTextView);
        addView(mRightImageView);
        if (mIsAddHorizontalLine){
            addView(mView);
        }
    }

    public SettingItemCustomView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public SettingItemCustomView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }
}
