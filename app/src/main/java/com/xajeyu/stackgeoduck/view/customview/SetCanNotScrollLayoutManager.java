package com.xajeyu.stackgeoduck.view.customview;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.util.AttributeSet;

/**
 * 自定义LinearLayoutManager
 * Created by e on 2017/5/6.
 */

public class SetCanNotScrollLayoutManager extends LinearLayoutManager {

    private boolean isScrollEnabled = true;

    public SetCanNotScrollLayoutManager(Context context) {
        super(context);
    }

    public SetCanNotScrollLayoutManager(Context context, int orientation, boolean reverseLayout) {
        super(context, orientation, reverseLayout);
    }

    public SetCanNotScrollLayoutManager(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    /**
     * 是否支持滑动
     *
     * @param flag
     */
    public void setScrollEnabled(boolean flag) {
        this.isScrollEnabled = flag;

    }
    @Override
    public boolean canScrollVertically() {
        //isScrollEnabled：recyclerview是否支持滑动
        //super.canScrollVertically()：是否为竖直方向滚动
        return isScrollEnabled && super.canScrollVertically();
    }
}
