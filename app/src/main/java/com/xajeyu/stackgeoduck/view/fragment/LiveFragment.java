package com.xajeyu.stackgeoduck.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.xajeyu.stackgeoduck.R;
import com.xajeyu.stackgeoduck.view.fragment.Base.BaseFragment;

/**
 * @PackageName: com.xajeyu.bilibili.view.fragment.Base
 * @FileName: LiveFragment.java
 * @CreationTime: 2017/4/13  14:30
 * @author: XajeYu
 * @E-mail: xajeyu@gmail.com
 * @function: 直播Fragment
 */
public class LiveFragment extends BaseFragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_his,container,false);
        return view;
    }
    @Override
    public void initData() {

    }
}
