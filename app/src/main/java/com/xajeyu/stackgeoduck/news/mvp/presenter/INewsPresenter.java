package com.xajeyu.stackgeoduck.news.mvp.presenter;

import java.util.List;

/**
 * @PackageName: com.xajeyu.stackgeoduck.news.mvp.presenter
 * @FileName: INewsPresenter.java
 * @CreationTime: 2017/5/18  20:36
 * @author: XajeYu
 * @E-mail: xajeyu@gmail.com
 * @function: 业务逻辑层接口
 */
public interface INewsPresenter {
    void onSuccess(Object msg);
    void onFail(Object msg);
    void getItemPosition(String msg);
}
