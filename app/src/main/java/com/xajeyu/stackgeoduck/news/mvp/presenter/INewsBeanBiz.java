package com.xajeyu.stackgeoduck.news.mvp.presenter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import com.github.jdsjlzx.recyclerview.LRecyclerView;

import java.io.IOException;
import java.util.List;

/**
 * @PackageName: com.xajeyu.stackgeoduck.news.mvp.presenter
 * @FileName: INewsBeanBiz.java
 * @CreationTime: 2017/5/18  20:35
 * @author: XajeYu
 * @E-mail: xajeyu@gmail.com
 * @function: 操作数据模型层
 */
public interface INewsBeanBiz {
    void setDataToView(Context context,LRecyclerView lRecyclerView,INewsPresenter iNewsPresenter);
}
