package com.xajeyu.stackgeoduck.news.mvp.presenter;

import com.xajeyu.sdk.news.NewsBean;
import com.xajeyu.stackgeoduck.news.mvp.model.NewsBeanBiz;
import com.xajeyu.stackgeoduck.news.mvp.view.INewsView;

import java.util.List;

/**
 * @PackageName: com.xajeyu.stackgeoduck.news.mvp.presenter
 * @FileName: NewsPresenter.java
 * @CreationTime: 2017/5/18  21:00
 * @author: XajeYu
 * @E-mail: xajeyu@gmail.com
 * @function:
 */
public class NewsPresenter implements INewsPresenter {
    private INewsView iNewsView;
    private INewsBeanBiz iNewsBeanBiz;
    private List<NewsBean.StoriesBean> mList;

    public NewsPresenter(INewsView iNewsView) {
        this.iNewsView = iNewsView;
        iNewsBeanBiz = new NewsBeanBiz();
    }
    public void setDataToView(){
        iNewsBeanBiz.setDataToView(iNewsView.getViewContext(),iNewsView.getLRecyclerView(),this);
    }
    @Override
    public void onSuccess(Object msg) {

    }

    @Override
    public void onFail(Object msg) {

    }

    @Override
    public void getItemPosition(String msg) {
        iNewsView.getItemPosition(msg);
    }
}
