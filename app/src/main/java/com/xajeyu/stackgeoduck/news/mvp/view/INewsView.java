package com.xajeyu.stackgeoduck.news.mvp.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import com.github.jdsjlzx.recyclerview.LRecyclerView;

import java.util.List;

/**
 * @PackageName: com.xajeyu.stackgeoduck.news.mvp.view
 * @FileName: INewsView.java
 * @CreationTime: 2017/5/18  21:01
 * @author: XajeYu
 * @E-mail: xajeyu@gmail.com
 * @function:
 */
public interface INewsView {
    void onSuccess(Object msg);
    void onFail(Object msg);
    void getItemPosition(String msg);
    Context getViewContext();
    LRecyclerView getLRecyclerView();
}
