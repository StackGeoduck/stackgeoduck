package com.xajeyu.stackgeoduck.news.presenter;

import com.xajeyu.stackgeoduck.news.model.INewsModel;
import com.xajeyu.stackgeoduck.news.model.NewsModeImpl;
import com.xajeyu.stackgeoduck.news.view.NewsView;

import java.util.List;

/**
 * Created by 13517 on 2017/5/19.
 */

public class NewsPresenter{

    private NewsView newsView;
    private INewsModel iNewsModel;

    public NewsPresenter(NewsView newsView){
        super();
        this.newsView = newsView;
        iNewsModel = new NewsModeImpl();

    }

    public void fetch(){

        iNewsModel.LoadNewsData(new INewsModel.LoadOnNewsDataListener() {
            @Override
            public void onComplete(List<?> data) {
                newsView.showData(data);
            }
        });
    }

    public void setTopData(){
        iNewsModel.LoadNewsTopData(new INewsModel.LoadOnTopNewsDataListener() {
            @Override
            public void onComplete(List<?> data) {
                newsView.showTopData(data);
            }
        });
    }

}
