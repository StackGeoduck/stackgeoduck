package com.xajeyu.stackgeoduck.news.model;

import com.google.gson.Gson;
import com.xajeyu.sdk.http.listener.RequestCallBack;
import com.xajeyu.sdk.news.NewsBean;
import com.xajeyu.stackgeoduck.network.api.news.NewsApi;
import com.xajeyu.stackgeoduck.network.http.AppNetwork;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by 13517 on 2017/5/19.
 */

public class NewsModeImpl implements INewsModel{

    @Override
    public void LoadNewsData(final LoadOnNewsDataListener listener) {


        AppNetwork.getInstance().RequestNewsData(new RequestCallBack() {
            List<NewsBean.StoriesBean> list = new ArrayList<>();
            @Override
            public void onSuccess(Object response) {
                NewsBean newsBean = (NewsBean) response;
                for (NewsBean.StoriesBean story : newsBean.stories) {
                    list.add(story);
                }
                listener.onComplete(list);

            }

            @Override
            public void onFail(Object response) {

            }

            @Override
            public void onDownloadProgress(long progress) {

            }
        });
    }


    @Override
    public void LoadNewsTopData(LoadOnTopNewsDataListener listener) {
        HttpTop(NewsApi.NEWSURL,listener);
    }



    public void HttpTop(String url, final LoadOnTopNewsDataListener listener){
        OkHttpClient okHttpClient = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .build();
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String string = response.body().string();
                Gson g = new Gson();

                NewsBean topStoriesBean = g.fromJson(string, NewsBean.class);
                List<NewsBean.TopStoriesBean> topList = new ArrayList<>();
                for (NewsBean.TopStoriesBean top_story : topStoriesBean.top_stories) {
                    topList.add(top_story);
                }
                listener.onComplete(topList);
            }
        });
    }
}
