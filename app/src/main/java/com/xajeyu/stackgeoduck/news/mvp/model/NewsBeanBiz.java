package com.xajeyu.stackgeoduck.news.mvp.model;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.github.jdsjlzx.interfaces.OnItemClickListener;
import com.github.jdsjlzx.recyclerview.LRecyclerView;
import com.github.jdsjlzx.recyclerview.LRecyclerViewAdapter;
import com.google.gson.Gson;
import com.xajeyu.sdk.http.listener.RequestCallBack;
import com.xajeyu.sdk.news.NewsBean;
import com.xajeyu.stackgeoduck.activity.NewsActivity;
import com.xajeyu.stackgeoduck.adapter.NewsAdapter;
import com.xajeyu.stackgeoduck.network.api.news.NewsApi;
import com.xajeyu.stackgeoduck.network.http.AppNetwork;
import com.xajeyu.stackgeoduck.news.mvp.presenter.INewsBeanBiz;
import com.xajeyu.stackgeoduck.news.mvp.presenter.INewsPresenter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * @PackageName: com.xajeyu.stackgeoduck.news.mvp.model
 * @FileName: NewsBeanBiz.java
 * @CreationTime: 2017/5/18  20:38
 * @author: XajeYu
 * @E-mail: xajeyu@gmail.com
 * @function:
 */
public class NewsBeanBiz implements INewsBeanBiz {
    List<NewsBean.StoriesBean> sList = new ArrayList<>();
    /**
     * 设置数据层
     *
     * @param context
     * @param lRecyclerView
     * @param iNewsPresenter
     */
    @Override
    public void setDataToView(final Context context, final LRecyclerView lRecyclerView, final INewsPresenter iNewsPresenter) {
        AppNetwork.getInstance().RequestNewsData(new RequestCallBack() {
            @Override
            public void onSuccess(Object response) {
                NewsBean newsBean = (NewsBean) response;
                for (NewsBean.StoriesBean story : newsBean.stories) {
                    sList.add(story);
                }
                LRecyclerViewAdapter lRecyclerViewAdapter = new LRecyclerViewAdapter(new NewsAdapter(sList));
                lRecyclerViewAdapter.setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        String des = NewsApi.URL + sList.get(position).id;
                        iNewsPresenter.getItemPosition(des);
                    }
                });
                lRecyclerView.refreshComplete(10);
                lRecyclerViewAdapter.notifyDataSetChanged();
                lRecyclerView.setAdapter(lRecyclerViewAdapter);
            }

            @Override
            public void onFail(Object response) {
                iNewsPresenter.onFail(response.toString());
            }

            @Override
            public void onDownloadProgress(long progress) {

            }
        });


    }
}
