package com.xajeyu.stackgeoduck.news.view;

import java.util.List;

/**
 * Created by 13517 on 2017/5/19.
 */

public interface NewsView {
    void showData(List<?> data);
    void showTopData(List<?> data);
}
