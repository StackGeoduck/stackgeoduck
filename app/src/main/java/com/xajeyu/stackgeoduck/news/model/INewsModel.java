package com.xajeyu.stackgeoduck.news.model;

import java.util.List;

/**
 * Created by 13517 on 2017/5/19.
 */

public interface INewsModel {
    /**
     * 加载数据
     */

    void LoadNewsData(LoadOnNewsDataListener listener);

    interface LoadOnNewsDataListener{
        void onComplete(List<?> data);
    }


    void LoadNewsTopData(LoadOnTopNewsDataListener listener);

    interface LoadOnTopNewsDataListener{
        void onComplete(List<?> data);
    }

}
