package com.xajeyu.stackgeoduck.dataobj.home;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by 13517 on 2017/5/16.
 */

public class MvMp4Bean {

    /**
     * loadingPic :
     * bufferPic :
     * loadingPicFS :
     * bufferPicFS :
     * subed : false
     * data : {"id":5526933,"name":"迁徙","artistId":6454,"artistName":"张信哲","briefDesc":"张信哲浪漫漂流到巴塞罗那","desc":"2017年张信哲备受期待的全新单曲「迁徙」，特别与华语乐坛的新生代音乐人合作，激荡出截然不同的火花。「迁徙」音乐跨度大，演唱技巧需求强大，是由创作小天王周兴哲又一佳作；由新生代优质制作人王斯禹打造出属于张信哲的气质摇滚品味。\n","cover":"http://p4.music.126.net/LVO_mVb_oNY2TRPqwHd1jA==/18711488883392571.jpg","coverId":18711488883392571,"playCount":19560,"subCount":252,"shareCount":125,"commentCount":156,"duration":367000,"nType":0,"publishTime":"2017-05-15","brs":{"480":"http://v4.music.126.net/20170517012148/d93a6438e3f974dc38c87d3d33b207cd/web/cloudmusic/JCMyMTM0IyBgJCQgJDAyMg==/mv/5526933/91b6e8e10bf21eaa134a45937d9d2ed3.mp4","240":"http://v4.music.126.net/20170517012148/bb376df8399dd8b5e7f0b7e45cc384ed/web/cloudmusic/JCMyMTM0IyBgJCQgJDAyMg==/mv/5526933/68264dc9959f20a3c57e51623a781e92.mp4","720":"http://v4.music.126.net/20170517012148/655c6499ad40469b6b10f2f290b45168/web/cloudmusic/JCMyMTM0IyBgJCQgJDAyMg==/mv/5526933/7c2ef10be9931aee62563b10a81f71d5.mp4","1080":"http://v4.music.126.net/20170517012148/7fa273f52a9b2a947db3994ae21a6f92/web/cloudmusic/JCMyMTM0IyBgJCQgJDAyMg==/mv/5526933/28fd7cde5b98285f70e9efcec1b31683.mp4"},"artists":[{"id":6454,"name":"张信哲"}],"isReward":false,"commentThreadId":"R_MV_5_5526933"}
     * code : 200
     */

    public String loadingPic;
    public String bufferPic;
    public String loadingPicFS;
    public String bufferPicFS;
    public boolean subed;
    public DataBean data;
    public int code;

    public static class DataBean {
        /**
         * id : 5526933
         * name : 迁徙
         * artistId : 6454
         * artistName : 张信哲
         * briefDesc : 张信哲浪漫漂流到巴塞罗那
         * desc : 2017年张信哲备受期待的全新单曲「迁徙」，特别与华语乐坛的新生代音乐人合作，激荡出截然不同的火花。「迁徙」音乐跨度大，演唱技巧需求强大，是由创作小天王周兴哲又一佳作；由新生代优质制作人王斯禹打造出属于张信哲的气质摇滚品味。

         * cover : http://p4.music.126.net/LVO_mVb_oNY2TRPqwHd1jA==/18711488883392571.jpg
         * coverId : 18711488883392571
         * playCount : 19560
         * subCount : 252
         * shareCount : 125
         * commentCount : 156
         * duration : 367000
         * nType : 0
         * publishTime : 2017-05-15
         * brs : {"480":"http://v4.music.126.net/20170517012148/d93a6438e3f974dc38c87d3d33b207cd/web/cloudmusic/JCMyMTM0IyBgJCQgJDAyMg==/mv/5526933/91b6e8e10bf21eaa134a45937d9d2ed3.mp4","240":"http://v4.music.126.net/20170517012148/bb376df8399dd8b5e7f0b7e45cc384ed/web/cloudmusic/JCMyMTM0IyBgJCQgJDAyMg==/mv/5526933/68264dc9959f20a3c57e51623a781e92.mp4","720":"http://v4.music.126.net/20170517012148/655c6499ad40469b6b10f2f290b45168/web/cloudmusic/JCMyMTM0IyBgJCQgJDAyMg==/mv/5526933/7c2ef10be9931aee62563b10a81f71d5.mp4","1080":"http://v4.music.126.net/20170517012148/7fa273f52a9b2a947db3994ae21a6f92/web/cloudmusic/JCMyMTM0IyBgJCQgJDAyMg==/mv/5526933/28fd7cde5b98285f70e9efcec1b31683.mp4"}
         * artists : [{"id":6454,"name":"张信哲"}]
         * isReward : false
         * commentThreadId : R_MV_5_5526933
         */

        public int id;
        public String name;
        public int artistId;
        public String artistName;
        public String briefDesc;
        public String desc;
        public String cover;
        public long coverId;
        public int playCount;
        public int subCount;
        public int shareCount;
        public int commentCount;
        public int duration;
        public int nType;
        public String publishTime;
        public BrsBean brs;
        public boolean isReward;
        public String commentThreadId;
        public List<ArtistsBean> artists;

        public static class BrsBean {
            /**
             * 480 : http://v4.music.126.net/20170517012148/d93a6438e3f974dc38c87d3d33b207cd/web/cloudmusic/JCMyMTM0IyBgJCQgJDAyMg==/mv/5526933/91b6e8e10bf21eaa134a45937d9d2ed3.mp4
             * 240 : http://v4.music.126.net/20170517012148/bb376df8399dd8b5e7f0b7e45cc384ed/web/cloudmusic/JCMyMTM0IyBgJCQgJDAyMg==/mv/5526933/68264dc9959f20a3c57e51623a781e92.mp4
             * 720 : http://v4.music.126.net/20170517012148/655c6499ad40469b6b10f2f290b45168/web/cloudmusic/JCMyMTM0IyBgJCQgJDAyMg==/mv/5526933/7c2ef10be9931aee62563b10a81f71d5.mp4
             * 1080 : http://v4.music.126.net/20170517012148/7fa273f52a9b2a947db3994ae21a6f92/web/cloudmusic/JCMyMTM0IyBgJCQgJDAyMg==/mv/5526933/28fd7cde5b98285f70e9efcec1b31683.mp4
             */

            @SerializedName("480")
            public String _$480;
            @SerializedName("240")
            public String _$240;
            @SerializedName("720")
            public String _$720;
            @SerializedName("1080")
            public String _$1080;
        }

        public static class ArtistsBean {
            /**
             * id : 6454
             * name : 张信哲
             */

            public int id;
            public String name;
        }
    }
}
