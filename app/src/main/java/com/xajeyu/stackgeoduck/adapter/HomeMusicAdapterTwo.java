package com.xajeyu.stackgeoduck.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.jdsjlzx.recyclerview.LRecyclerViewAdapter;
import com.xajeyu.sdk.imageloader.ImageLoaderManager;
import com.xajeyu.stackgeoduck.R;
import com.xajeyu.stackgeoduck.model.dataobj.home.HomeMvBean;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by 13517 on 2017/5/15.
 */

public class HomeMusicAdapterTwo extends RecyclerView.Adapter {

    private  HomeMvBean homeMvBean;
    private Context mContext;


    public HomeMusicAdapterTwo(Context mContext,Object homeMvBean) {
        this.mContext = mContext;
        this.homeMvBean = (HomeMvBean) homeMvBean;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = View.inflate(mContext, R.layout.item_music_home_2, null);

        return new HomeViewHolderTwo(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        HomeViewHolderTwo holderTwo = (HomeViewHolderTwo) holder;
        holderTwo.tvMusicNum.setText(homeMvBean.result.get(position).playCount+"");
        holderTwo.tvMusicText.setText(homeMvBean.result.get(position).name);
        holderTwo.tvMusicSinger.setText(homeMvBean.result.get(position).artistName);
        ImageLoaderManager.getInstance(mContext).display(holderTwo.qwer,homeMvBean.result.get(position).picUrl);

    }

    @Override
    public int getItemCount() {
        return 4;
    }

    static class HomeViewHolderTwo extends LRecyclerViewAdapter.ViewHolder{
        @BindView(R.id.qwer)
        ImageView qwer;
        private final TextView tvMusicNum;
        private final TextView tvMusicText;
        private final TextView tvMusicSinger;

        public HomeViewHolderTwo(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            tvMusicNum = (TextView) itemView.findViewById(R.id.tv_music_num);
            tvMusicText = (TextView) itemView.findViewById(R.id.tv_music_text);
            tvMusicSinger = (TextView) itemView.findViewById(R.id.tv_music_singer);
        }
    }
}
