package com.xajeyu.stackgeoduck.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.jdsjlzx.recyclerview.LRecyclerViewAdapter;
import com.xajeyu.stackgeoduck.R;

/**
 * Created by 13517 on 2017/5/16.
 */


public class HomeMusicAdapterThree extends RecyclerView.Adapter{
    private Context mContext;
     HomeMusicAdapterThree(Context context){
        this.mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = View.inflate(mContext, R.layout.item_music_home_3, null);
        return new HomeViewHolderThree(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        HomeViewHolderThree homeViewHolderThree = (HomeViewHolderThree) holder;

    }

    @Override
    public int getItemCount() {
        return 5;
    }


    class HomeViewHolderThree extends LRecyclerViewAdapter.ViewHolder{

        public HomeViewHolderThree(View itemView) {
            super(itemView);
        }
    }
}
