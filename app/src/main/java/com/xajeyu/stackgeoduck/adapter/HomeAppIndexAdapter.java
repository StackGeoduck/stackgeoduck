package com.xajeyu.stackgeoduck.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.jdsjlzx.interfaces.OnItemClickListener;
import com.github.jdsjlzx.recyclerview.LRecyclerView;
import com.github.jdsjlzx.recyclerview.LRecyclerViewAdapter;
import com.xajeyu.sdk.http.listener.RequestCallBack;
import com.xajeyu.sdk.music.MusicDataObj;
import com.xajeyu.stackgeoduck.R;
import com.xajeyu.stackgeoduck.activity.HomeMusicMvActivity;
import com.xajeyu.stackgeoduck.activity.MusicActivity;
import com.xajeyu.stackgeoduck.model.dataobj.home.HomeMvBean;
import com.xajeyu.stackgeoduck.network.http.AppNetwork;
import com.xajeyu.stackgeoduck.utils.ImageLoader;
import com.youth.banner.Banner;
import com.youth.banner.BannerConfig;
import com.youth.banner.Transformer;
import com.youth.banner.listener.OnBannerListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * @PackageName: com.xajeyu.bilibili.adapter
 * @FileName: HomeAppIndexAdapter.java
 * @CreationTime: 2017/4/19  10:11
 * @author: XajeYu
 * @E-mail: xajeyu@gmail.com
 * @function: 首页直播Adapter
 */
public class HomeAppIndexAdapter extends RecyclerView.Adapter implements View.OnClickListener {

    private OnItemClickLitener mOnItemClickLitener;

    private LRecyclerViewAdapter lRecyclerViewAdapter2;
    private LRecyclerView lRecylcerView3;

    public interface OnItemClickLitener {
        void onItemClick(View view, int position);
    }

    public void setOnItemClickLitener(OnItemClickLitener mOnItemClickLitener) {
        this.mOnItemClickLitener = mOnItemClickLitener;
    }


    /**
     * UI部分
     */
    private View mHeaderView;
    private View mFooterView;
    private Context context;

    /**
     * 数据部分
     */
    private MusicDataObj mMusicDataObj;
    // Banner图片集合
    private List<Integer> bannerList;
    // 条目类型
    private static final int TYPE_HEADER = 0;  //说明是带有Header的
    private static final int TYPE_FOOTER = 1;  //说明是带有Footer的
    private static final int TYPE_NORMAL = 2;  //说明是不带有header和footer的


    // 表明HomeFragment一共有多少个条目 不算Header和Footer和Header底部的select
    private static final int MAX_ENTRY = 1;


    // 导航栏下分区Item Icon
    private int[] entranceIconRes = new int[]{

    };

    // 分区Item title
    private String[] entranceTitls = new String[]{
            "私人FM", "每日歌曲推荐", "云音乐热歌榜"
    };

    /**
     * 构造函数
     *
     * @param context
     */
    public HomeAppIndexAdapter(Context context, Object musicDataObj) {
        this.context = context;
        mMusicDataObj = (MusicDataObj) musicDataObj;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Header
        if (mHeaderView != null && viewType == TYPE_HEADER) {
            return new LiveItemBanner(mHeaderView);
        }
        if (TYPE_NORMAL == viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_home_recyclerview, null);
            LRecyclerView lRecyclerView = (LRecyclerView) view.findViewById(R.id.rv_music_home);
            final LRecyclerView lRecyclerViewTwo = (LRecyclerView) view.findViewById(R.id.rv_music_home_2);

            //lRecyclerView数据适配
            LRecyclerViewAdapter lRecyclerViewAdapter = new LRecyclerViewAdapter(new HomeMusicAdapter(context, mMusicDataObj));
            View headerview = View.inflate(context, R.layout.lrecyclerview_header, null);
            lRecyclerViewAdapter.addHeaderView(headerview);
            lRecyclerView.setHasFixedSize(true);
            StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
            lRecyclerView.setLayoutManager(staggeredGridLayoutManager);
            lRecyclerView.setAdapter(lRecyclerViewAdapter);
            lRecyclerViewAdapter.setOnItemClickListener(new OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
//                mMusicDataObj.songs.data.get(position).mMusic
                    Intent intent = new Intent(context, MusicActivity.class);
                    intent.putExtra("id", mMusicDataObj.hotspot.data.get(position).id);
                    intent.putExtra("position", position);
                    context.startActivity(intent);
                }
            });


            StaggeredGridLayoutManager staggeredGridLayoutManager2 = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
            lRecyclerViewTwo.setLayoutManager(staggeredGridLayoutManager2);
            final View headerview2 = View.inflate(context, R.layout.lrecyclerview_header_2, null);



            AppNetwork.getInstance().RequsetMusicMv(new RequestCallBack() {
                @Override
                public void onSuccess(final Object response) {
                    // lRecyclerViewTwo数据适配
                    lRecyclerViewAdapter2 = new LRecyclerViewAdapter(new HomeMusicAdapterTwo(context,response));
                    lRecyclerViewAdapter2.addHeaderView(headerview2);
                    lRecyclerViewTwo.setAdapter(lRecyclerViewAdapter2);

                    lRecyclerViewAdapter2.setOnItemClickListener(new OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {
                           HomeMvBean homeMvBean = (HomeMvBean) response;
                            Intent intent = new Intent(context, HomeMusicMvActivity.class);
                            intent.putExtra("MVID",homeMvBean.result.get(position).id+"");
                            context.startActivity(intent);
                        }
                    });
                }

                @Override
                public void onFail(Object response) {
                    System.out.println("失败："+response);
                }

                @Override
                public void onDownloadProgress(long progress) {

                }
            });


            //lRecylcerView3的数据适配
            lRecylcerView3 = (LRecyclerView) view.findViewById(R.id.rv_music_home_3);

            LRecyclerViewAdapter lRecyclerViewAdapter3 = new LRecyclerViewAdapter(new HomeMusicAdapterThree(context));

            lRecylcerView3.setLayoutManager(new LinearLayoutManager(context));

            View header3 = View.inflate(context, R.layout.lrecyclerview_header_3, null);

            lRecyclerViewAdapter3.addHeaderView(header3);

            //设置分割线
         //   lRecylcerView3.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL_LIST));

            lRecylcerView3.setAdapter(lRecyclerViewAdapter3);

            return new MusicViewHolder(view);
        }
        return null;
    }

    //HeaderView和FooterView的get和set函数
    public View getHeaderView() {
        return mHeaderView;
    }

    public void setHeaderView(View headerView) {
        mHeaderView = headerView;
        notifyItemInserted(0);
    }

    public View getFooterView() {
        return mFooterView;
    }

    public void setFooterView(View footerView) {
        mFooterView = footerView;
        notifyItemInserted(getItemCount() - 1);
    }


    /**
     * 为Holder绑定数
     *
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        // 如果设置了回调，则设置点击事件
        if (mOnItemClickLitener != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = holder.getLayoutPosition();
                    mOnItemClickLitener.onItemClick(holder.itemView, pos);
                }
            });

            if (getItemViewType(position) == TYPE_NORMAL) {
                //    LiveItemViewHolder liveItemViewHolder = (LiveItemViewHolder) holder;
//            liveItemViewHolder.CardVideoImage.setImageDrawable(mLiveAppIndexInfo.getLiveVideoImage());
//            liveItemViewHolder.CardTextType.setText(mLiveAppIndexInfo.getLiveTypeText());
//            liveItemViewHolder.CardTextTitle.setText(mLiveAppIndexInfo.getLiveTitleText());
//            liveItemViewHolder.CardTextIssuer.setText(mLiveAppIndexInfo.getLiveIssuer());
//            liveItemViewHolder.CardTextViewNumber.setText(mLiveAppIndexInfo.getLiveViewNumber());
//            liveItemViewHolder.cardView.setOnClickListener(this);
            }
            if (getItemViewType(position) == TYPE_HEADER) {
                LiveItemBanner liveItemBanner = (LiveItemBanner) holder;
                BannerBuilder(liveItemBanner.banner);

                liveItemBanner.privateFmText.setText(entranceTitls[0]);
                liveItemBanner.hotSongText.setText(entranceTitls[1]);
                liveItemBanner.recommendedDailyText.setText(entranceTitls[2]);
            }
        }
    }

    /**
     * 配置Banner
     */
    private void BannerBuilder(Banner mBanner) {
        // 创建轮播图 图片集合
        List<String> image = new ArrayList<>();
        for (int i = 0; i < mMusicDataObj.banner.banners.size(); i++) {
            image.add(mMusicDataObj.banner.banners.get(i).imageUrl);
        }
        //设置banner样式
        mBanner.setBannerStyle(BannerConfig.CIRCLE_INDICATOR);
        //设置图片加载器
        mBanner.setImageLoader(new ImageLoader());
        //设置图片集合
        mBanner.setImages(image);
        //设置banner动画效果
        mBanner.setBannerAnimation(Transformer.Default);
        //设置轮播时间
        mBanner.setDelayTime(5000);
        //设置指示器位置（当banner模式中有指示器时）
        mBanner.setIndicatorGravity(BannerConfig.CENTER);
        //banner设置方法全部调用完毕时最后调用
        mBanner.start();
        // 图片点击事件
        mBanner.setOnBannerListener(new OnBannerListener() {
            @Override
            public void OnBannerClick(int position) {
                System.out.println(mMusicDataObj.banner.banners.get(position).url);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mHeaderView == null && mFooterView == null) {
            return MAX_ENTRY;
        } else if (mHeaderView != null) {
            return MAX_ENTRY + 1;
        } else {
            return MAX_ENTRY;
        }
    }

    /**
     * 点击Item事件
     *
     * @param v
     */
    @Override
    public void onClick(View v) {
        System.out.println("sss");
    }

    /**
     * 直播Holder
     */

    static class MusicViewHolder extends RecyclerView.ViewHolder {

        public MusicViewHolder(View itemView) {
            super(itemView);

        }
    }


    /**
     * 顶部导航栏Holder
     */
    static class LiveItemBanner extends RecyclerView.ViewHolder {
        // 轮播
        @BindView(R.id.fragment_home_top_bar)
        Banner banner;

        //私人FM
        @BindView(R.id.item_fragment_home_select_image_private_fm)
        ImageView privateFmImage;
        @BindView(R.id.item_fragment_home_select_text_private_fm)
        TextView privateFmText;
        // 歌单
        @BindView(R.id.item_fragment_home_select_image_hot_song)
        ImageView hotSongImage;
        @BindView(R.id.item_fragment_home_select_text_hot_song)
        TextView hotSongText;
        // 推荐
        @BindView(R.id.item_fragment_home_select_image_recommended_daily)
        ImageView recommendedDailyImage;
        @BindView(R.id.item_fragment_home_select_text_recommended_daily)
        TextView recommendedDailyText;

        public LiveItemBanner(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    /**
     * 导航条下的select
     */
    static class MusicItemSelect extends RecyclerView.ViewHolder {
        //私人FM
        @BindView(R.id.item_fragment_home_select_image_private_fm)
        ImageView privateFmImage;
        @BindView(R.id.item_fragment_home_select_text_private_fm)
        TextView privateFmText;
        // 歌单
        @BindView(R.id.item_fragment_home_select_image_hot_song)
        ImageView hotSongImage;
        @BindView(R.id.item_fragment_home_select_text_hot_song)
        TextView hotSongText;
        // 推荐
        @BindView(R.id.item_fragment_home_select_image_recommended_daily)
        ImageView recommendedDailyImage;
        @BindView(R.id.item_fragment_home_select_text_recommended_daily)
        TextView recommendedDailyText;

        public MusicItemSelect(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


    @Override
    public int getItemViewType(int position) {
        if (mHeaderView == null && mFooterView == null) {
            return TYPE_NORMAL;
        }
        if (position == 0 && mHeaderView != null) {
            // 第一个item应该加载Header
            return TYPE_HEADER;
        }
        if (position == getItemCount() - 1 && mFooterView != null) {
            //加载footer
            return TYPE_FOOTER;
        }
        return TYPE_NORMAL;
    }
}
