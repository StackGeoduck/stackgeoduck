package com.xajeyu.stackgeoduck.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.jdsjlzx.recyclerview.LRecyclerViewAdapter;
import com.xajeyu.sdk.imageloader.ImageLoaderManager;
import com.xajeyu.sdk.music.MusicDataObj;
import com.xajeyu.sdk.music.MusicPlaylist;
import com.xajeyu.stackgeoduck.R;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by 13517 on 2017/5/9.
 */

public class HomeMusicAdapter extends RecyclerView.Adapter {

    private Context mContext;
    private MusicDataObj musicDataObj;


    public HomeMusicAdapter(Context mContext, MusicDataObj musicDataObj) {
        this.mContext = mContext;
         this.musicDataObj = ClearRadio(musicDataObj);

    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = View.inflate(mContext, R.layout.item_music_home, null);

        return new HomeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        HomeViewHolder homeViewHolder = (HomeViewHolder) holder;
        ImageLoaderManager.getInstance(mContext).display(homeViewHolder.qwer, musicDataObj.hotspot.data.get(position).picUrl);
        homeViewHolder.tvMusicNum.setText(musicDataObj.hotspot.data.get(position).trackCount + "万");
        homeViewHolder.tvMusicText.setText(musicDataObj.hotspot.data.get(position).name);
    }

    @Override
    public int getItemCount() {
        return 6;
    }

    static class HomeViewHolder extends LRecyclerViewAdapter.ViewHolder {
        @BindView(R.id.qwer)
        ImageView qwer;
        private final TextView tvMusicNum;
        private final TextView tvMusicText;

        public HomeViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            tvMusicNum = (TextView) itemView.findViewById(R.id.tv_music_num);
            tvMusicText = (TextView) itemView.findViewById(R.id.tv_music_text);
        }
    }

    private MusicDataObj ClearRadio(MusicDataObj musicDataObj) {
        MusicDataObj musicDataObj1 = null;

        for (int i = 0; i < musicDataObj.hotspot.data.size(); i++) {
            if (musicDataObj.hotspot.data.get(i).type == 0) {
                musicDataObj.hotspot.data.remove(i);
            }
            musicDataObj1 = musicDataObj;

        }
        return musicDataObj1;
    }

   /* public MusicDataObj ClearRadio2(MusicDataObj musicDataObj){

        MusicDataObj musicDataObj2;
        MusicDataObj musicDataObj1 = ClearRadio(musicDataObj);
        for (int j = 0; j < musicDataObj1.hotspot.data.size(); j++) {
            if (!musicDataObj1.hotspot.data.get(j).highQuality) {
                musicDataObj1.hotspot.data.remove(j);
            }
        }
        musicDataObj2 = musicDataObj1;
        return musicDataObj2;

    }*/



}
