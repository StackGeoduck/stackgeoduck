package com.xajeyu.stackgeoduck.adapter;

import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.xajeyu.sdk.imageloader.ImageLoaderManager;
import com.xajeyu.sdk.news.NewsBean;
import com.xajeyu.stackgeoduck.R;
import com.xajeyu.stackgeoduck.activity.NewsActivity;
import com.xajeyu.stackgeoduck.network.api.news.NewsApi;

import static android.media.CamcorderProfile.get;
import static com.xajeyu.stackgeoduck.view.fragment.NewsFragment.mContext;

/**
 * Created by 13517 on 2017/5/4.
 */

public class NewsHeaderAdapter extends PagerAdapter {

    private NewsBean topStoriesBeanList;
    public NewsHeaderAdapter(NewsBean topStoriesBeanList){

        this.topStoriesBeanList = topStoriesBeanList;
    }

    @Override
    public int getCount() {
        return topStoriesBeanList.top_stories.size();

    }

    @Override
    public boolean isViewFromObject(View view, Object object) {

        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        container.removeView((View) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View view = View.inflate(mContext, R.layout.item_viewpager, null);
        TextView tvViewPager = (TextView) view.findViewById(R.id.tv_viewpager);
        ImageView ivViewpager = (ImageView) view.findViewById(R.id.iv_viewpager);
        tvViewPager.setText(topStoriesBeanList.top_stories.get(position).title);
        ImageLoaderManager.getInstance(mContext).display(ivViewpager,topStoriesBeanList.top_stories.get(position).image);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String des = NewsApi.URL + topStoriesBeanList.top_stories.get(position).id;
                Intent intent = new Intent(mContext, NewsActivity.class);
                intent.putExtra("des", des);
                mContext.startActivity(intent);

            }
        });
        container.addView(view);
        return view;
    }
}
