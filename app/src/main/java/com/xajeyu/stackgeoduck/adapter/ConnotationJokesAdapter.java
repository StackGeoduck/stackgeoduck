package com.xajeyu.stackgeoduck.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.xajeyu.stackgeoduck.R;
import com.xajeyu.stackgeoduck.model.dataobj.neihan.NeiHanData;
import com.xajeyu.sdk.imageloader.ImageLoaderManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * @PackageName: com.xajeyu.bilibili.adapter
 * @FileName: ConnotationJokesAdapter.java
 * @CreationTime: 2017/4/21  10:29
 * @author: XajeYu
 * @E-mail: xajeyu@gmail.com
 * @function: 内涵段子数据适配器
 */
public class ConnotationJokesAdapter extends RecyclerView.Adapter {
    private NeiHanData neiHanData;
    private Context mContext;

    /**
     * 构造方法
     *
     * @param response
     */
    public ConnotationJokesAdapter(Context context, Object response) {
        this.mContext = context;
        this.neiHanData = json((NeiHanData) response);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card_jokes_text, null);
        return new JokesItemCard(view);
    }

    /**
     * 数据绑定
     *
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        JokesItemCard jokesItemCard = (JokesItemCard) holder;
        // 加载用户头像
        ImageLoaderManager.getInstance(mContext).display(jokesItemCard.circleImageView, neiHanData.data.data.get(position).group.user.avatar_url);
        // 设置用户名
        jokesItemCard.userName.setText(neiHanData.data.data.get(position).group.user.name);
        // 设置内容
        jokesItemCard.title.setText(neiHanData.data.data.get(position).group.text);
        // 设置分类
        jokesItemCard.type.setText(neiHanData.data.data.get(position).group.category_name);
        // 设置点赞数
        jokesItemCard.zanNumber.setText(String.valueOf(neiHanData.data.data.get(position).group.digg_count));
        // 设置踩数
        jokesItemCard.caiNumber.setText(String.valueOf(neiHanData.data.data.get(position).group.bury_count));
        // 设置评论
        jokesItemCard.commentsNumber.setText(String.valueOf(neiHanData.data.data.get(position).group.comment_count));
    }

    @Override
    public int getItemCount() {
        return neiHanData.data.data.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    /**
     * 文字段子
     */
    static class JokesItemCard extends RecyclerView.ViewHolder {
        // 用户头像
        @BindView(R.id.item_card_jokes_text_image_user_icon)
        CircleImageView circleImageView;
        // 用户名字
        @BindView(R.id.item_card_jokes_text_text_view_user_name)
        TextView userName;
        // 内容
        @BindView(R.id.item_card_jokes_text_text_view_content)
        TextView title;
        // 分区
        @BindView(R.id.item_card_jokes_text_button_content_type_)
        Button type;
        // 赞
        @BindView(R.id.item_card_jokes_text_zan_number)
        TextView zanNumber;
        // 踩
        @BindView(R.id.item_card_jokes_text_cai_number)
        TextView caiNumber;
        // 评论
        @BindView(R.id.item_card_jokes_text_comments_number)
        TextView commentsNumber;
        // 转发数量
        @BindView(R.id.item_card_jokes_text_share_number)
        TextView shardNumber;
        // Card
        CardView cardView;

        public JokesItemCard(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    /**
     * 返回一个处理过的数据
     *
     * @param neiHanData
     * @return
     */
    private NeiHanData json(NeiHanData neiHanData) {
        NeiHanData sNeiHanData = new NeiHanData();
        for (int i = 0; i < neiHanData.data.data.size(); i++) {
            // 判断请求到的neiHanData数据中是否有某个数据为空。如果为空移除
            if (neiHanData.data.data.get(i).group == null) {
                neiHanData.data.data.remove(i);
            }
            sNeiHanData = neiHanData;
        }
        return sNeiHanData;
    }
}
