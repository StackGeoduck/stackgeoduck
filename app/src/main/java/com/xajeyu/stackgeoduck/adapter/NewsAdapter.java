package com.xajeyu.stackgeoduck.adapter;

/**
 * Created by 13517 on 2017/5/4.
 */

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.jdsjlzx.recyclerview.LRecyclerView;
import com.xajeyu.sdk.imageloader.ImageLoaderManager;
import com.xajeyu.sdk.news.NewsBean;
import com.xajeyu.stackgeoduck.R;


import java.util.ArrayList;
import java.util.List;

import static com.xajeyu.stackgeoduck.view.fragment.NewsFragment.mContext;

/**
 * lRecyclerView的数据适配器
 */
public class NewsAdapter extends RecyclerView.Adapter {

    private ArrayList<NewsBean.StoriesBean> newsBean;

    public NewsAdapter(List<NewsBean.StoriesBean> newsList){

        this.newsBean = (ArrayList<NewsBean.StoriesBean>) newsList;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        NewsViewHodler newsViewHodler = new NewsViewHodler(View.inflate(mContext, R.layout.item_news, null));
        return newsViewHodler;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        NewsViewHodler newsViewHodler = (NewsViewHodler) holder;
        newsViewHodler.tvNews.setText(newsBean.get(position).title);
        ImageLoaderManager.getInstance(mContext).display(newsViewHodler.ivNews,newsBean.get(position).images.get(0));

    }

    @Override
   public int getItemCount() {
        return newsBean.size();
    }

    /**
     * 新闻的ViewHodler
     */
    class NewsViewHodler extends LRecyclerView.ViewHolder{

        private final TextView tvNews;
        private final ImageView ivNews;

        public NewsViewHodler(View itemView) {
            super(itemView);
            tvNews = (TextView) itemView.findViewById(R.id.tv_news);
            ivNews = (ImageView) itemView.findViewById(R.id.iv_news);
        }
    }
}
