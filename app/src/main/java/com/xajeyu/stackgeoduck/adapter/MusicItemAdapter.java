package com.xajeyu.stackgeoduck.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.jdsjlzx.recyclerview.LRecyclerViewAdapter;
import com.xajeyu.sdk.http.listener.RequestCallBack;
import com.xajeyu.sdk.imageloader.ImageLoaderManager;
import com.xajeyu.sdk.music.MusicPlaylist;
import com.xajeyu.sdk.music.MusicSongObj;
import com.xajeyu.stackgeoduck.R;
import com.xajeyu.stackgeoduck.network.http.AppNetwork;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by 13517 on 2017/5/12.
 */


public class MusicItemAdapter extends RecyclerView.Adapter {

    private Context mContext;
    private MusicPlaylist mMusicPlaylist;
    private MusicSongObj mMusicSongObj;

    public MusicItemAdapter(Context mContext,MusicPlaylist musicPlaylist) {
        this.mContext = mContext;
        this.mMusicPlaylist = musicPlaylist;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = View.inflate(mContext, R.layout.item_music, null);
        return new MusicItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final MusicItemViewHolder musicItemViewHolder = (MusicItemViewHolder) holder;
        int id = mMusicPlaylist.result.tracks.get(position).id;
        AppNetwork.getInstance().RequestSong(id, new RequestCallBack() {
            @Override
            public void onSuccess(Object response) {
                mMusicSongObj = (MusicSongObj) response;
                // 设置歌名
                musicItemViewHolder.TvMusicName.setText(mMusicSongObj.songs.get(0).name);
                // 设置
                musicItemViewHolder.TvMusicSinger.setText(mMusicSongObj.songs.get(0).artists.get(0).name);
                // 设置歌曲封面
                ImageLoaderManager.getInstance(mContext).display(musicItemViewHolder.IvMusicImg,mMusicSongObj.songs.get(0).album.blurPicUrl);
            }
            @Override
            public void onFail(Object response) {

            }

            @Override
            public void onDownloadProgress(long progress) {

            }
        });

    }

    @Override
    public int getItemCount() {
        return mMusicPlaylist.result.tracks.size();
    }


     class MusicItemViewHolder extends LRecyclerViewAdapter.ViewHolder {
        @BindView(R.id.iv_music)
        ImageView IvMusicImg;
        @BindView(R.id.tv_music_name)
        TextView TvMusicName;
        @BindView(R.id.tv_music_singer)
        TextView TvMusicSinger;

        MusicItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
