package com.xajeyu.stackgeoduck.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

/**
 * @PackageName: com.xajeyu.bilibili.adapter
 * @FileName: HomePagerAdapter.java
 * @CreationTime: 2017/4/13  14:21
 * @author: XajeYu
 * @E-mail: xajeyu@gmail.com
 * @function: ViewPager数据适配器
 */
public class HomePagerAdapter extends FragmentStatePagerAdapter {
    private List<Fragment> fragments;
    private List<String> titles;

    public HomePagerAdapter(FragmentManager fragmentManager, List<Fragment> fragments, List<String> titles) {
        super(fragmentManager);
        this.fragments = fragments;
        this.titles = titles;
    }

    /**
     * 获取当前Fragment
     * @param position
     * @return
     */
    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    /**
     * 返回Fragment集合大小
     * @return
     */
    @Override
    public int getCount() {
        return fragments.size();
    }

    /**
     * 获取标题
     * @param position
     * @return
     */
    @Override
    public CharSequence getPageTitle(int position) {
        return titles.get(position);
    }
}
