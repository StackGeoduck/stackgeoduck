package com.xajeyu.stackgeoduck.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.xajeyu.stackgeoduck.dataobj.home.MvMp4Bean;

/**
 * Created by 13517 on 2017/5/22.
 */

public class MvRecyclerViewAdapter extends RecyclerView.Adapter {
    private MvMp4Bean mvMp4Bean;

    public MvRecyclerViewAdapter(MvMp4Bean mvMp4Bean) {
        this.mvMp4Bean = mvMp4Bean;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }
}
