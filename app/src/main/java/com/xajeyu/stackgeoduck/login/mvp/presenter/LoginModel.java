package com.xajeyu.stackgeoduck.login.mvp.presenter;

/**
 * @PackageName: com.xajeyu.stackgeoduck.model
 * @FileName: LoginModel.java
 * @CreationTime: 2017/5/16  14:33
 * @author: XajeYu
 * @E-mail: xajeyu@gmail.com
 * @function: 登录模型
 */
public interface LoginModel {
    // 登录
    void Login(String name,String password,OnLoginListener loginListener);
    // 注册
    void Register(String name,String password,OnLoginListener loginListener);
}
