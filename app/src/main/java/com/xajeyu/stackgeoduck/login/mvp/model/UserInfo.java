package com.xajeyu.stackgeoduck.login.mvp.model;

/**
 * @PackageName: com.xajeyu.stackgeoduck.dataobj.user
 * @FileName: UserInfo.java
 * @CreationTime: 2017/5/15  10:55
 * @author: XajeYu
 * @E-mail: xajeyu@gmail.com
 * @function:
 */
public class UserInfo {

    /**
     * user_account : 814387271@qq.com
     * user_name : ???
     * user_icon : http://tva1.sinaimg.cn/crop.0.0.1242.1242.180/005wokZXjw8fbw24bpyrfj30yi0yigol.jpg
     * user_mail : 814387271@qq.com
     * user_is_login : 1
     */

    public String user_account;
    public String user_name;
    public String user_icon;
    public String user_mail;
    public String user_is_login;
}
