package com.xajeyu.stackgeoduck.login.mvp.presenter;

/**
 * @PackageName: com.xajeyu.stackgeoduck.model
 * @FileName: OnLoginListener.java
 * @CreationTime: 2017/5/16  14:34
 * @author: XajeYu
 * @E-mail: xajeyu@gmail.com
 * @function:
 */
public interface OnLoginListener {
    // 账号错误
    void onUserAccountError();
    // 密码错误
     void onUserPasswordError();
    // 登录成功
     void onLoginSuccess();
    // 登录失败
     void onLoginFailure(Object msg);
    // 注册成功
    void onRegisterSuccess();
    // 注册失败
    void onRegisterFailure(Object msg);
}
