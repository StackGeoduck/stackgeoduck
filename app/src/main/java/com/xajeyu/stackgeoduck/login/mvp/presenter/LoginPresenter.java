package com.xajeyu.stackgeoduck.login.mvp.presenter;

import com.xajeyu.stackgeoduck.login.mvp.imple.LoginModelImple;
import com.xajeyu.stackgeoduck.login.mvp.view.LoginView;

/**
 * @PackageName: com.xajeyu.stackgeoduck.login.mvp.presenter
 * @FileName: LoginPresenter.java
 * @CreationTime: 2017/5/16  14:46
 * @author: XajeYu
 * @E-mail: xajeyu@gmail.com
 * @function:
 */
public class LoginPresenter implements OnLoginListener {
    private LoginModel loginModel;
    private LoginView loginView;

    /**
     * 构造函数
     * @param loginView
     */
    public LoginPresenter(LoginView loginView) {
        this.loginView = loginView;
        loginModel = new LoginModelImple();
    }

    /**
     * 登录方法
     */
    public void Login(){
        loginModel.Login(loginView.getUserAccount(),loginView.getUserPassword(),this);
    }
    public void Register(){
        loginModel.Register(loginView.getUserAccount(),loginView.getUserPassword(),this);
    }

    @Override
    public void onUserAccountError() {
        loginView.onLoginFailure("账号错误");
    }

    @Override
    public void onUserPasswordError() {
        loginView.onLoginFailure("密码错误");
    }

    @Override
    public void onLoginSuccess() {
        loginView.onLoginSuccess();
    }

    @Override
    public void onLoginFailure(Object msg) {
        loginView.onLoginFailure(msg);
    }

    @Override
    public void onRegisterSuccess() {
        loginView.onRegisterSuccess();
    }

    @Override
    public void onRegisterFailure(Object msg) {
        loginView.onRegisterFailure(msg);
    }
}
