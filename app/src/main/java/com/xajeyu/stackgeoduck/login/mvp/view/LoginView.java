package com.xajeyu.stackgeoduck.login.mvp.view;

/**
 * @PackageName: com.xajeyu.stackgeoduck.view.listener
 * @FileName: LoginView.java
 * @CreationTime: 2017/5/16  14:28
 * @author: XajeYu
 * @E-mail: xajeyu@gmail.com
 * @function: 登录时用的监听
 */
public interface LoginView {
    // 登录成功
    void onLoginSuccess();
    // 登录异常
    void onLoginFailure(Object msg);
    // 注册成功
    void onRegisterSuccess();
    //注册失败
    void onRegisterFailure(Object msg);
    // 获取账号
    String getUserAccount();
    // 获取密码
    String getUserPassword();
}
