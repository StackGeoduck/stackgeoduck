package com.xajeyu.stackgeoduck.login.mvp.imple;

import com.xajeyu.sdk.http.listener.RequestCallBack;
import com.xajeyu.stackgeoduck.login.mvp.presenter.LoginModel;
import com.xajeyu.stackgeoduck.login.mvp.presenter.OnLoginListener;
import com.xajeyu.stackgeoduck.network.http.AppNetwork;

/**
 * @PackageName: com.xajeyu.stackgeoduck.login.mvp.imple
 * @FileName: LoginModelImple.java
 * @CreationTime: 2017/5/16  14:39
 * @author: XajeYu
 * @E-mail: xajeyu@gmail.com
 * @function: 实现登录业务逻辑
 */
public class LoginModelImple implements LoginModel{

    /**
     * 登录逻辑
     * @param account 用户账号
     * @param pwd 用户密码
     * @param loginListener 业务逻辑层监听
     */
    @Override
    public void Login(final String account, String pwd, final OnLoginListener loginListener) {

        AppNetwork.getInstance().RequestLogin(account, pwd, new RequestCallBack() {
            @Override
            public void onSuccess(Object response) {
                // 判断状态信息是否正确
                if (response.equals("登录成功！")) {
                    loginListener.onLoginSuccess();
                } else if (response.equals("账号错误！")) {
                    loginListener.onUserAccountError();
                } else if (response.equals("当前用户已在线！")) {
                   loginListener.onLoginFailure("当前用户已在线！");
                } else if (response.equals("密码错误！")) {
                   loginListener.onUserPasswordError();
                }
            }

            @Override
            public void onFail(Object response) {
                loginListener.onLoginFailure(response);
            }

            @Override
            public void onDownloadProgress(long progress) {

            }
        });
    }

    @Override
    public void Register(String account, String pwd, final OnLoginListener loginListener) {
        AppNetwork.getInstance().RequestRegister(account, pwd, new RequestCallBack() {
            @Override
            public void onSuccess(Object response) {
                if (response.equals("注册成功")) {
                    loginListener.onRegisterSuccess();
                } else {
                    loginListener.onRegisterFailure(response);
                }
            }

            @Override
            public void onFail(Object response) {
                loginListener.onRegisterFailure(response);
            }

            @Override
            public void onDownloadProgress(long progress) {

            }
        });
    }
}
