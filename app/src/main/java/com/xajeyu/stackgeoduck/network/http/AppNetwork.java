package com.xajeyu.stackgeoduck.network.http;

import com.xajeyu.sdk.http.JGHttpClinent;
import com.xajeyu.sdk.http.listener.DisposeDataHandler;
import com.xajeyu.sdk.http.listener.RequestCallBack;
import com.xajeyu.sdk.http.request.CreateRequest;
import com.xajeyu.sdk.http.request.RequestParams;
import com.xajeyu.sdk.music.MusicDataObj;
import com.xajeyu.sdk.music.MusicPlaylist;
import com.xajeyu.sdk.music.MusicSongObj;

import com.xajeyu.sdk.news.NewsBean;
import com.xajeyu.stackgeoduck.cinema.bean.CinemaDetailsCommentaryInfo;
import com.xajeyu.stackgeoduck.cinema.bean.CinemaDetailsInfo;
import com.xajeyu.stackgeoduck.cinema.bean.CinemaInfo;
import com.xajeyu.stackgeoduck.dataobj.home.MvMp4Bean;
import com.xajeyu.stackgeoduck.network.api.Base.BaseUrl;
import com.xajeyu.stackgeoduck.cinema.api.CinemaApi;
import com.xajeyu.stackgeoduck.model.dataobj.home.HomeMvBean;
import com.xajeyu.stackgeoduck.model.dataobj.neihan.NeiHanData;

import com.xajeyu.stackgeoduck.login.mvp.model.UserInfo;
import com.xajeyu.stackgeoduck.network.api.jokes.NeiHanApi;
import com.xajeyu.stackgeoduck.network.api.music.MusicApi;
import com.xajeyu.stackgeoduck.network.api.news.NewsApi;

/**
 * @PackageName: com.xajeyu.bilibili.network.http
 * @FileName: AppNetwork.java
 * @CreationTime: 2017/4/21  11:18
 * @author: XajeYu
 * @E-mail: xajeyu@gmail.com
 * @function:  应用内请求
 */
public class AppNetwork {

    private static class AppNetworkHolder{
        private static final AppNetwork instance = new AppNetwork();
    }

    /**
     * 获取实例
     * @return
     */
    public static AppNetwork getInstance() {
        return AppNetworkHolder.instance;
    }
    private AppNetwork(){

    }
    /**
     * 请求无请求头的get请求的应用程序网络请求
     * @param url
     * @param params
     * @param callBack
     * @param clazz
     */
    private void getRequest(String url, RequestParams params, RequestCallBack callBack, Class<?> clazz){
        JGHttpClinent.get(CreateRequest.createHttpGetRequest(url,params), new DisposeDataHandler(callBack,clazz));
    }

    /**
     * POST请求无请求头
     * @param url
     * @param params
     * @param callBack
     * @param clazz
     */
    private void postRequest(String url, RequestParams params, RequestCallBack callBack, Class<?> clazz){
        JGHttpClinent.post(CreateRequest.createHttpPostRequest(url,params), new DisposeDataHandler(callBack,clazz));
    }

    /**
     * 添加有请求头的的请求
     * @param url 请求地址
     * @param params 请求参数
     * @param header  请求头参数
     * @param callBack  网络回掉
     * @param clazz 字节码
     */
    private void postRequest(String url, RequestParams params, RequestParams header, RequestCallBack callBack, Class<?> clazz){
        JGHttpClinent.post(CreateRequest.createHttpPostRequest(url,params,header), new DisposeDataHandler(callBack,clazz));
    }

    /**
     *  下载文件
     * @param url 文件远程地址
     * @param filepath  文件保存路径
     * @param callBack 文件下载回调
     */
    public void DownloadFile(String url,String filepath,String fileName,RequestCallBack callBack){
        JGHttpClinent.downloadFile(CreateRequest.createHttpGetRequest(url,null),new DisposeDataHandler(callBack,filepath,fileName));
    }
    /**
     * 请求内涵段子数据
     * @param callBack
     */
    public void RequestNeiHanApi(RequestCallBack callBack){
        getRequest(NeiHanApi.getInstance().DefaultRecommendedUrl(),null,callBack, NeiHanData.class);

    }
    // 请求网易云头部轮播图
    public void RequestMusicApi(RequestCallBack callBack){
        postRequest(MusicApi.getInstance().getMusicUrl(),MusicApi.getInstance().getMusicRequest(),MusicApi.getInstance().getMusicRequestHeader(),callBack, MusicDataObj.class);
    }
    // 请求影院
    public void RequestCinema(RequestCallBack callBack) {
        getRequest(CinemaApi.CINEMA_MIAN_URL + CinemaApi.CINEMA_HOME_URL, null, callBack, CinemaInfo.class);
    }
    //请求影院详情信息
    public void RequestCinemaDetails(String url, RequestCallBack callBack) {
        getRequest(url,null,callBack, CinemaDetailsInfo.class);
    }
    //请求影院详情加载更多评论
    public void RequestCinemaDetailsCommentary(String url,RequestCallBack callBack){
        getRequest(url,null,callBack, CinemaDetailsCommentaryInfo.class);
    }
    // 请求新闻数据
    public void RequestNewsData(RequestCallBack callBack){
        getRequest(NewsApi.NEWSURL,null,callBack, NewsBean.class);
    }

    public void RequsetNewsBefore(RequestCallBack callBack){
        getRequest(NewsApi.TIMEURL,null,callBack, NewsBean.class);
    }

    //请求MV的MP4数据
    public void RequsetMusicMvMp4(String id,RequestCallBack callBack){
        getRequest(NewsApi.MvMp4+id,null,callBack, MvMp4Bean.class);
    }

    // 请求MV
    public void RequsetMusicMv(RequestCallBack callBack){
        getRequest("http://music.163.com/api/personalized/mv",null,callBack, HomeMvBean.class);
    }

    /**
     * 发送登录请求
     * @param account
     * @param pwd
     * @param callBack
     */
    public void RequestLogin(String account,String pwd,RequestCallBack callBack){
        RequestParams requestParams = new RequestParams();
        requestParams.put("account",account);
        requestParams.put("pwd",pwd);
        postRequest(BaseUrl.LOGINURL,requestParams,callBack,null);
    }
    /**
     * 发送注册请求
     * @param account
     * @param pwd
     * @param callBack
     */
    public void RequestRegister(String account,String pwd,RequestCallBack callBack){
        RequestParams requestParams = new RequestParams();
        requestParams.put("account",account);
        requestParams.put("pwd",pwd);
        postRequest(BaseUrl.REGISTEURL,requestParams,callBack,null);
    }

    /**
     * 发送歌单请求
     * @param id
     * @param callBack
     */
    public void RequestPlaylist(int id,RequestCallBack callBack){
        getRequest(MusicApi.getInstance().getPlaylist(id),null,callBack, MusicPlaylist.class);
    }

    /**
     * 发送歌曲请求
     * @param id
     * @param callBack
     */
     public void RequestSong(int id,RequestCallBack callBack){
         getRequest(MusicApi.getInstance().getSongInfo(id),null,callBack, MusicSongObj.class);
     }

    /**
     * 请求用户信息
     * @param callBack
     */
     public void RequestUserInfo(String account,RequestCallBack callBack){
         RequestParams params = new RequestParams();
         params.put("account",account);
         postRequest(BaseUrl.USERINFOURL,params,callBack, UserInfo.class);
     }
}
