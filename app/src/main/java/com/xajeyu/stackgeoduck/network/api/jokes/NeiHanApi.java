package com.xajeyu.stackgeoduck.network.api.jokes;

/**
 * @PackageName: com.xajeyu.bilibili.network.requesturlapi
 * @FileName: NeiHanApi.java
 * @CreationTime: 2017/4/20  20:35
 * @author: XajeYu
 * @E-mail: xajeyu@gmail.com
 * @function: 封装 内涵段子api
 */
public class NeiHanApi {
    private static class NeiHanApiHolder{
        /**
         * 运用静态代码块优化单例模式
         */
        private static final NeiHanApi instance = new NeiHanApi();
    }

    /**
     * 获取实例
     * @return
     */
    public static NeiHanApi getInstance() {
        return NeiHanApiHolder.instance;
    }
    // 推荐URL地址
    private static final String URL = "http://is.snssdk.com/neihan/stream/mix/v1/?mpic=1&webp=1&essence=1&content_type=-102&message_cursor=-1&am_city=海口市&count=10&screen_width=1450&do00le_col_mode=0&iid=3216590132&device_id=32613520945&ac=wifi&channel=360&aid=7&app_name=joke_essay&version_code=612&version_name=6.1.2&device_platform=android&ssmix=a&device_type=sansung&device_brand=xiaomi&os_api=28&os_version=6.10.1&uuid=3222633745&openudid=3dg6s95rhg2a3dg5&manifest_version_code=612&resolution=1450*2800&dpi=620&update_version_code=6120";


    /**
     * 返回推荐数据
     * @return 返回推荐数据，
     */
    public String DefaultRecommendedUrl(){
//        StringBuilder res = new StringBuilder(RECOMMENDED_URL);
//        res.append("&")
//                .append("essence=1&")
//                .append("webp=1&")
//                .append("content_type=-101&")
//                .append("message_cursor=-1&")
//                .append("am_city=海口市&")
//                .append("count=20&")   //获取多少条数据
//                .append("double_col_mode=0&")
//                .append("iid=3216590132&")
//                .append()

        return URL;
    }

}
