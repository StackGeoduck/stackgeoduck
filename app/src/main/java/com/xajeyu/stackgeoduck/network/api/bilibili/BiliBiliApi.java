package com.xajeyu.stackgeoduck.network.api.bilibili;

import com.xajeyu.stackgeoduck.network.api.Base.BaseUrl;

/**
 * @PackageName: com.xajeyu.bilibili.network.api.bilibili
 * @FileName: BiliBiliApi.java
 * @CreationTime: 2017/4/24  10:26
 * @author: XajeYu
 * @E-mail: xajeyu@gmail.com
 * @function:
 */
public class BiliBiliApi {
    public String getBanner(){
        return BaseUrl.BILIBILIAPPBANNER+"x/banner?plat=4&build=411007&channel=bilih5";
    }
}
