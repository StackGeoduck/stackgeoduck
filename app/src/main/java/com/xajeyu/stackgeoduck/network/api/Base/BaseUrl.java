package com.xajeyu.stackgeoduck.network.api.Base;

/**
 * @PackageName: com.xajeyu.bilibili.network.api.Base
 * @FileName: BaseUrl.java
 * @CreationTime: 2017/4/24  10:22
 * @author: XajeYu
 * @E-mail: xajeyu@gmail.com
 * @function: 应用程序全部URL请求基地址
 */
public class BaseUrl {
    // 网易云请求API
    public static final String MUSICURL = "http://music.163.com/eapi/batch";
    // 网易云歌单请求API
    public static final String MUSICPLAYLIST = "http://music.163.com/api/playlist/detail?id=";
    // 网易云歌曲信息请求API
    public static final String MUSICSONGINFO = "http://music.163.com/api/song/detail/?&ids=";


    // bilibili首页Banner Api
    public static final String BILIBILIAPPBANNER = "http://app.bilibili.com/";

    //登录请求地址
    public static final String LOGINURL = "http://www.xajeyu.cn/api/login.php";
    // 注册请求地址
    public static final String REGISTEURL = "http://www.xajeyu.cn/api/register.php";
    // 请求用户信息
    public static final String USERINFOURL = "http://www.xajeyu.cn/api/get_user_info.php";

}
