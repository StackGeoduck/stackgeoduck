package com.xajeyu.stackgeoduck.model.dataobj.home;

import java.util.List;

/**
 * Created by 13517 on 2017/5/15.
 */

public class HomeMvBean {

    /**
     * code : 200
     * category : 3
     * result : [{"id":5527031,"type":5,"name":"大宝","copywriter":"编辑推荐：电影《抢红》推广曲MV发布，黎明张涵予魔性来袭","picUrl":"http://p1.music.126.net/v9tuS_ff1Ee4nyrNNPtz7w==/19224960811791674.jpg","canDislike":false,"duration":96000,"playCount":1788,"subed":false,"artists":[{"id":12399027,"name":"张涵予"},{"id":3701,"name":"黎明"}],"artistName":"张涵予","artistId":12399027,"alg":"featured"},{"id":5523073,"type":5,"name":"サイハテアイニ","copywriter":"编辑推荐：RADWIMPS新单最新现场释出，表白白衣洋子","picUrl":"http://p1.music.126.net/KcSIK_tfKw5oZ9zPKfMjnQ==/19205169602490193.jpg","canDislike":false,"duration":255000,"playCount":34060,"subed":false,"artists":[{"id":21132,"name":"RADWIMPS"}],"artistName":"RADWIMPS","artistId":21132,"alg":"featured"},{"id":5527017,"type":5,"name":"Bon Appétit","copywriter":"最新热门MV推荐","picUrl":"http://p1.music.126.net/lPHp5KmQQYapqkAAGLEv-g==/19096317951358825.jpg","canDislike":true,"duration":259000,"playCount":586212,"subed":false,"artists":[{"id":62888,"name":"Katy Perry"},{"id":776824,"name":"Migos"}],"artistName":"Katy Perry","artistId":62888,"alg":"hot_server"},{"id":5528017,"type":5,"name":"2017 SEVENTEEN Project Chapter1. Alone 预告(HOSHI版)","copywriter":"最新热门MV推荐","picUrl":"http://p1.music.126.net/2f93_gxGLNRhz4m4u9gS6g==/19160089625752788.jpg","canDislike":true,"duration":89000,"playCount":2668,"subed":false,"artists":[{"id":1080132,"name":"SEVENTEEN"}],"artistName":"SEVENTEEN","artistId":1080132,"alg":"hot_server"}]
     */

    public int code;
    public int category;
    public List<ResultBean> result;

    public static class ResultBean {
        /**
         * id : 5527031
         * type : 5
         * name : 大宝
         * copywriter : 编辑推荐：电影《抢红》推广曲MV发布，黎明张涵予魔性来袭
         * picUrl : http://p1.music.126.net/v9tuS_ff1Ee4nyrNNPtz7w==/19224960811791674.jpg
         * canDislike : false
         * duration : 96000
         * playCount : 1788
         * subed : false
         * artists : [{"id":12399027,"name":"张涵予"},{"id":3701,"name":"黎明"}]
         * artistName : 张涵予
         * artistId : 12399027
         * alg : featured
         */

        public int id;
        public int type;
        public String name;
        public String copywriter;
        public String picUrl;
        public boolean canDislike;
        public int duration;
        public int playCount;
        public boolean subed;
        public String artistName;
        public int artistId;
        public String alg;
        public List<ArtistsBean> artists;

        public static class ArtistsBean {
            /**
             * id : 12399027
             * name : 张涵予
             */

            public int id;
            public String name;
        }
    }
}
