package com.xajeyu.stackgeoduck.model.dataobj.neihan;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @PackageName: com.xajeyu.bilibili.dataobj.neihan
 * @FileName: NeiHanData.java
 * @CreationTime: 2017/4/20  22:31
 * @author: XajeYu
 * @E-mail: xajeyu@gmail.com
 * @function: 内涵段子数据模型
 */
public class NeiHanData {

    public String message;
    public DataBeanX data;

    public static class DataBeanX {
        public boolean has_more;
        public String tip;
        public boolean has_new_message;
        public double max_time;
        public double min_time;
        public List<DataBean> data;

        public static class DataBean {
            public GroupBean group;
            public int type;
            public double display_time;
            public double online_time;
            public AdBean ad;
            public List<?> comments;

            public static class GroupBean {
                public String video_id;
                @SerializedName("360p_video")
                public _$360pVideoBean _$360p_video;
                public String mp4_url;
                public String text;
                @SerializedName("720p_video")
                public _$720pVideoBean _$720p_video;
                public int digg_count;
                public double duration;
                @SerializedName("480p_video")
                public _$480pVideoBean _$480p_video;
                public float create_time;
                public String keywords;
                public long id;
                public int favorite_count;
                public DanmakuAttrsBean danmaku_attrs;
                public String m3u8_url;
                public LargeCoverBean large_cover;
                public float video_wh_ratio;
                public int user_favorite;
                public float share_type;
                public String title;
                public UserBean user;
                public float is_can_share;
                public float category_type;
                public String share_url;
                public float label;
                public String content;
                public float video_height;
                public int comment_count;
                public String cover_image_uri;
                public String id_str;
                public float media_type;
                public float share_count;
                public float type;
                public float category_id;
                public float status;
                public float has_comments;
                public String publish_time;
                public float go_detail_count;
                public ActivityBean activity;
                public String status_desc;
                public String neihan_hot_start_time;
                public float play_count;
                public float user_repin;
                public boolean quick_comment;
                public MediumCoverBean medium_cover;
                public String neihan_hot_end_time;
                public float user_digg;
                public float video_width;
                public float online_time;
                public String category_name;
                public String flash_url;
                public boolean category_visible;
                public int bury_count;
                public boolean is_anonymous;
                public float repin_count;
                public float is_video;
                public boolean is_neihan_hot;
                public String uri;
                public float is_public_url;
                public float has_hot_comments;
                public boolean allow_dislike;
                public OriginVideoBean origin_video;
                public String cover_image_url;
                public NeihanHotLinkBean neihan_hot_link;
                public long group_id;
                public float user_bury;
                public float display_type;
                public List<DislikeReasonBean> dislike_reason;

                public static class _$360pVideoBean {
                    /**
                     * width : 480
                     * url_list : [{"url":"http://ic.snssdk.com/neihan/video/playback/1492755862.74/?video_id=20d2e3419d4d45668509fcd1ff431b0a&quality=360p&line=0&is_gif=0&device_platform=android"},{"url":"http://ic.snssdk.com/neihan/video/playback/1492755862.74/?video_id=20d2e3419d4d45668509fcd1ff431b0a&quality=360p&line=1&is_gif=0&device_platform=android"}]
                     * uri : 360p/20d2e3419d4d45668509fcd1ff431b0a
                     * height : 854
                     */

                    public float width;
                    public String uri;
                    public float height;
                    public List<UrlListBean> url_list;

                    public static class UrlListBean {
                        /**
                         * url : http://ic.snssdk.com/neihan/video/playback/1492755862.74/?video_id=20d2e3419d4d45668509fcd1ff431b0a&quality=360p&line=0&is_gif=0&device_platform=android
                         */

                        public String url;
                    }
                }

                public static class _$720pVideoBean {
                    /**
                     * width : 480
                     * url_list : [{"url":"http://ic.snssdk.com/neihan/video/playback/1492755862.74/?video_id=20d2e3419d4d45668509fcd1ff431b0a&quality=720p&line=0&is_gif=0&device_platform=android"},{"url":"http://ic.snssdk.com/neihan/video/playback/1492755862.74/?video_id=20d2e3419d4d45668509fcd1ff431b0a&quality=720p&line=1&is_gif=0&device_platform=android"}]
                     * uri : 720p/20d2e3419d4d45668509fcd1ff431b0a
                     * height : 854
                     */

                    public float width;
                    public String uri;
                    public float height;
                    public List<UrlListBeanX> url_list;

                    public static class UrlListBeanX {
                        /**
                         * url : http://ic.snssdk.com/neihan/video/playback/1492755862.74/?video_id=20d2e3419d4d45668509fcd1ff431b0a&quality=720p&line=0&is_gif=0&device_platform=android
                         */

                        public String url;
                    }
                }

                public static class _$480pVideoBean {
                    /**
                     * width : 480
                     * url_list : [{"url":"http://ic.snssdk.com/neihan/video/playback/1492755862.74/?video_id=20d2e3419d4d45668509fcd1ff431b0a&quality=480p&line=0&is_gif=0&device_platform=android"},{"url":"http://ic.snssdk.com/neihan/video/playback/1492755862.74/?video_id=20d2e3419d4d45668509fcd1ff431b0a&quality=480p&line=1&is_gif=0&device_platform=android"}]
                     * uri : 480p/20d2e3419d4d45668509fcd1ff431b0a
                     * height : 854
                     */

                    public float width;
                    public String uri;
                    public float height;
                    public List<UrlListBeanXX> url_list;

                    public static class UrlListBeanXX {
                        /**
                         * url : http://ic.snssdk.com/neihan/video/playback/1492755862.74/?video_id=20d2e3419d4d45668509fcd1ff431b0a&quality=480p&line=0&is_gif=0&device_platform=android
                         */

                        public String url;
                    }
                }

                public static class DanmakuAttrsBean {
                    /**
                     * allow_show_danmaku : 1
                     * allow_send_danmaku : 1
                     */

                    public float allow_show_danmaku;
                    public float allow_send_danmaku;
                }

                public static class LargeCoverBean {
                    /**
                     * url_list : [{"url":"http://p1.pstatp.com/large/1ce6000e03ff69bdc9ef.webp"},{"url":"http://pb3.pstatp.com/large/1ce6000e03ff69bdc9ef.webp"},{"url":"http://pb3.pstatp.com/large/1ce6000e03ff69bdc9ef.webp"}]
                     * uri : large/1ce6000e03ff69bdc9ef
                     */

                    public String uri;
                    public List<UrlListBeanXXX> url_list;

                    public static class UrlListBeanXXX {
                        /**
                         * url : http://p1.pstatp.com/large/1ce6000e03ff69bdc9ef.webp
                         */

                        public String url;
                    }
                }

                public static class UserBean {
                    /**
                     * user_id : 3860548431
                     * name : 右手扬名天下
                     * followings : 0
                     * user_verified : false
                     * ugc_count : 5
                     * avatar_url : http://p1.pstatp.com/medium/6cb0024e842d00d11b9
                     * followers : 40
                     * is_following : false
                     * is_pro_user : false
                     */

                    public long user_id;
                    public String name;
                    public float followings;
                    public boolean user_verified;
                    public float ugc_count;
                    public String avatar_url;
                    public float followers;
                    public boolean is_following;
                    public boolean is_pro_user;
                }

                public static class ActivityBean {
                    /**
                     * status : 2
                     * title : 15秒假唱大赛
                     * text : 拼演技，对口型，看看谁是假唱王

                     活动细则
                     点击底部“发现”，进入顶部活动宣传图
                     使用内涵段子自带拍摄工具，录制15秒视频
                     在音乐库中选择背景片段，开始对口型表演
                     活动奖品
                     最佳表演奖 1名  （内涵T恤）
                     最逗搞笑奖 1名  （内涵T恤）
                     最广传播奖 1名  （内涵T恤）
                     幸运参与奖 3名  （内涵车贴）
                     * start_time : 1492656092
                     * end_time : 1492999200
                     * image : {"width":862,"url_list":[{"url":"http://p3.pstatp.com/large/1bf40011cdfac253d375"},{"url":"http://pb1.pstatp.com/large/1bf40011cdfac253d375"},{"url":"http://pb3.pstatp.com/large/1bf40011cdfac253d375"}],"uri":"large/1bf40011cdfac253d375","height":334}
                     * category_id : 65
                     * id : 107
                     */

                    public float status;
                    public String title;
                    public String text;
                    public float start_time;
                    public float end_time;
                    public ImageBean image;
                    public float category_id;
                    public float id;

                    public static class ImageBean {
                        /**
                         * width : 862
                         * url_list : [{"url":"http://p3.pstatp.com/large/1bf40011cdfac253d375"},{"url":"http://pb1.pstatp.com/large/1bf40011cdfac253d375"},{"url":"http://pb3.pstatp.com/large/1bf40011cdfac253d375"}]
                         * uri : large/1bf40011cdfac253d375
                         * height : 334
                         */

                        public float width;
                        public String uri;
                        public float height;
                        public List<UrlListBeanXXXX> url_list;

                        public static class UrlListBeanXXXX {
                            /**
                             * url : http://p3.pstatp.com/large/1bf40011cdfac253d375
                             */

                            public String url;
                        }
                    }
                }

                public static class MediumCoverBean {
                    /**
                     * url_list : [{"url":"http://p1.pstatp.com/w202/1ce6000e03ff69bdc9ef.webp"},{"url":"http://pb3.pstatp.com/w202/1ce6000e03ff69bdc9ef.webp"},{"url":"http://pb3.pstatp.com/w202/1ce6000e03ff69bdc9ef.webp"}]
                     * uri : medium/1ce6000e03ff69bdc9ef
                     */

                    public String uri;
                    public List<UrlListBeanXXXXX> url_list;

                    public static class UrlListBeanXXXXX {
                        /**
                         * url : http://p1.pstatp.com/w202/1ce6000e03ff69bdc9ef.webp
                         */

                        public String url;
                    }
                }

                public static class OriginVideoBean {
                    public static class UrlListBeanXXXXXX {
                    }
                }

                public static class NeihanHotLinkBean {
                }

                public static class DislikeReasonBean {
                    /**
                     * type : 2
                     * id : 65
                     * title : 吧:搞笑视频
                     */

                    public float type;
                    public float id;
                    public String title;
                }
            }

            public static class AdBean {
                /**
                 * log_extra : {"rit":11,"ad_price":"WPmAYQABq09Y-YBhAAGrT--8z4KbL-34H3hQJw","req_id":"201704211424210100030231669422EC","convert_id":0}
                 * open_url :
                 * track_url :
                 * display_info : 速8毁掉4000辆名车，爱奇艺VIP专享半价【点击抢购】
                 * web_url : http://vip.iqiyi.com/sevenyear.html?fv=013630a192a78583e5e46ddb4d9e6685
                 * avatar_name : 爱奇艺VIP会员
                 * id : 59253702831
                 * display_image_height : 326
                 * display_image_width : 580
                 * title : 速8毁掉4000辆名车，爱奇艺VIP专享半价【点击抢购】
                 * label : 广告
                 * track_url_list : []
                 * display_image : http://p3.pstatp.com/large/1c1a000f097f1d20e7a6
                 * filter_words : [{"id":"1:12","name":"互联网服务","is_selected":false},{"id":"1:126","name":"视频","is_selected":false},{"id":"2:0","name":"来源:爱奇艺VIP会员","is_selected":false},{"id":"4:2","name":"看过了","is_selected":false}]
                 * type : web
                 * is_ad : 1
                 * gif_url :
                 * ad_id : 59253702831
                 * button_text : 查看详情
                 * display_type : 5
                 * ab_show_style : 2
                 * video_info : {"video_url":"http://i.snssdk.com/neihan/video/playback/?video_id=3e86ab4be3184ac09a76a66c708b2f98&quality=360p&line=0","backup_url":"http://i.snssdk.com/neihan/video/playback/?video_id=3e86ab4be3184ac09a76a66c708b2f98&quality=360p&line=1","cover_url":"http://p3.pstatp.com/large/1c1a000f097f1d20e7a6","height":326,"width":580,"video_duration":207}
                 * avatar_url : http://p1.pstatp.com/origin/9197/1375840585
                 * end_time : 1808205066
                 */

                public LogExtraBean log_extra;
                public String open_url;
                public String track_url;
                public String display_info;
                public String web_url;
                public String avatar_name;
                public long id;
                public float display_image_height;
                public float display_image_width;
                public String title;
                public String label;
                public String display_image;
                public String type;
                public float is_ad;
                public String gif_url;
                public long ad_id;
                public String button_text;
                public float display_type;
                public float ab_show_style;
                public VideoInfoBean video_info;
                public String avatar_url;
                public float end_time;
                public List<?> track_url_list;
                public List<FilterWordsBean> filter_words;

                public static class LogExtraBean {
                    /**
                     * rit : 11
                     * ad_price : WPmAYQABq09Y-YBhAAGrT--8z4KbL-34H3hQJw
                     * req_id : 201704211424210100030231669422EC
                     * convert_id : 0
                     */

                    public float rit;
                    public String ad_price;
                    public String req_id;
                    public float convert_id;
                }

                public static class VideoInfoBean {
                    /**
                     * video_url : http://i.snssdk.com/neihan/video/playback/?video_id=3e86ab4be3184ac09a76a66c708b2f98&quality=360p&line=0
                     * backup_url : http://i.snssdk.com/neihan/video/playback/?video_id=3e86ab4be3184ac09a76a66c708b2f98&quality=360p&line=1
                     * cover_url : http://p3.pstatp.com/large/1c1a000f097f1d20e7a6
                     * height : 326
                     * width : 580
                     * video_duration : 207
                     */

                    public String video_url;
                    public String backup_url;
                    public String cover_url;
                    public float height;
                    public float width;
                    public float video_duration;
                }

                public static class FilterWordsBean {
                    /**
                     * id : 1:12
                     * name : 互联网服务
                     * is_selected : false
                     */

                    public String id;
                    public String name;
                    public boolean is_selected;
                }
            }
        }
    }
}
