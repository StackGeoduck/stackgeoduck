package com.xajeyu.stackgeoduck.model.dataobj.live;

import java.util.List;

/**
 * @PackageName: com.xajeyu.bilibili.dataobj.live
 * @FileName: LiveAppBannerInfo.java
 * @CreationTime: 2017/4/24  10:28
 * @author: XajeYu
 * @E-mail: xajeyu@gmail.com
 * @function:
 */
public class LiveAppBannerInfo {

    /**
     * code : 0
     * data : [{"title":"纪录片0422","value":"http://www.bilibili.com/blackboard/activity-ryxNqCF_Ax.html","image":"http://i0.hdslb.com/bfs/archive/53c72611bbb2921ed83af1ddbca4460c8226dfe4.jpg","type":2,"weight":1,"remark":"","hash":"dcb68758b2bd4c2e7769fb9110110745"},{"title":"科技53期","value":"http://www.bilibili.com/blackboard/activity-SklXimww0g.html","image":"http://i0.hdslb.com/bfs/archive/7fae2beeeb9db88496bb33a387378486edd8f62f.png","type":2,"weight":2,"remark":"","hash":"095bc84629f565b1f59800ad5514b3e5"},{"title":"弹幕3","value":"http://www.bilibili.com/blackboard/activity-bhyszmj-m.html","image":"http://i0.hdslb.com/bfs/archive/ffde8736357104685aaaa3e9dab31129932d9fd4.jpg","type":2,"weight":2,"remark":"","hash":"66ac56b6899359583c166838157f9f03"},{"title":"联通","value":"http://www.bilibili.com/blackboard/activity-unicomopenbeta-m2.html","image":"http://i0.hdslb.com/bfs/archive/0b3bd8e7545c4ff76b54e8b6a092be1d42442930.png","type":2,"weight":5,"remark":"","hash":"c140b7b4b868e7f19609b860aa8b573c"}]
     */

    public int code;
    public List<DataBean> data;

    public static class DataBean {
        /**
         * title : 纪录片0422
         * value : http://www.bilibili.com/blackboard/activity-ryxNqCF_Ax.html
         * image : http://i0.hdslb.com/bfs/archive/53c72611bbb2921ed83af1ddbca4460c8226dfe4.jpg
         * type : 2
         * weight : 1
         * remark :
         * hash : dcb68758b2bd4c2e7769fb9110110745
         */
        public String title;
        public String value;
        public String image;
        public int type;
        public int weight;
        public String remark;
        public String hash;
    }
}
