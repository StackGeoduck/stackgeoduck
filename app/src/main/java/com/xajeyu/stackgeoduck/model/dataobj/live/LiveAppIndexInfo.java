package com.xajeyu.stackgeoduck.model.dataobj.live;

import android.graphics.drawable.Drawable;

/**
 * @PackageName: com.xajeyu.bilibili.dataobj.live
 * @FileName: LiveAppIndexInfo.java
 * @CreationTime: 2017/4/19  10:32
 * @author: XajeYu
 * @E-mail: xajeyu@gmail.com
 * @function: 首页数据模型类
 */
public class LiveAppIndexInfo {

    private Drawable liveVideoImage; // 直播图片
    private String liveViewNumber; // 观看人数
    private String liveTypeText;  // 分类
    private String liveTitleText; // 标题
    private String liveIssuer; // 发布人

    public Drawable getLiveVideoImage() {
        return liveVideoImage;
    }

    public void setLiveVideoImage(Drawable liveVideoImage) {
        this.liveVideoImage = liveVideoImage;
    }

    public String getLiveViewNumber() {
        return liveViewNumber;
    }

    public void setLiveViewNumber(String liveViewNumber) {
        this.liveViewNumber = liveViewNumber;
    }

    public String getLiveTypeText() {
        return liveTypeText;
    }

    public void setLiveTypeText(String liveTypeText) {
        this.liveTypeText = liveTypeText;
    }

    public String getLiveTitleText() {
        return liveTitleText;
    }

    public void setLiveTitleText(String liveTitleText) {
        this.liveTitleText = liveTitleText;
    }

    public String getLiveIssuer() {
        return liveIssuer;
    }

    public void setLiveIssuer(String liveIssuer) {
        this.liveIssuer = liveIssuer;
    }
}
