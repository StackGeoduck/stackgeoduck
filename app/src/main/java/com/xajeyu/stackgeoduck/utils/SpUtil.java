
package com.xajeyu.stackgeoduck.utils;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Administrator on 2016/12/27.
 */

public class SpUtil {
    private static SharedPreferences sp;


    /**
     * @param context 上下文环境
     * @param key     存储节点名称
     * @param value   存储节点的值 boolean
     */
    //    写
    public static void putBoolean(Context context, String key, boolean value) {
        if (sp == null) {
            //（1.存储节点文件名称，2.读写方式）
            sp = context.getSharedPreferences("config", Context.MODE_PRIVATE);
        }
        sp.edit().putBoolean(key, value).commit();

    }

    //读

    /**
     *
     * @param context 上下文环境
     * @param key 存储节点名称
     * @param defvalue 没有此节点的默认值
     * @return 默认值或者此节点读取到的结果
     */
    public static boolean getBoolean(Context context, String key, boolean defvalue) {
        if (sp == null) {
            //（1.存储节点文件名称，2.读写方式）
            sp = context.getSharedPreferences("config", Context.MODE_PRIVATE);
        }
        return sp.getBoolean(key, defvalue);

    }

    //////////////////////////////////////////////////////////////////
    /**
     * @param context 上下文环境
     * @param key     存储节点名称
     * @param value   存储节点的值 boolean
     */
    //    写
    public static void putInt(Context context, String key, int value) {
        if (sp == null) {
            //（1.存储节点文件名称，2.读写方式）
            sp = context.getSharedPreferences("config", Context.MODE_PRIVATE);
        }
        sp.edit().putInt(key, value).commit();

    }

    /**
     *
     * @param context 上下文环境
     * @param key 存储节点名称
     * @param defvalue 没有此节点的默认值
     * @return 默认值或者此节点读取到的结果
     */
    public static int getInt(Context context, String key, int defvalue) {
        if (sp == null) {
            //（1.存储节点文件名称，2.读写方式）
            sp = context.getSharedPreferences("config", Context.MODE_PRIVATE);
        }
        return sp.getInt(key, defvalue);

    }

    public static void putString(Context context, String key, String value) {
        if (sp == null) {
            //（1.存储节点文件名称，2.读写方式）
            sp = context.getSharedPreferences("config", Context.MODE_PRIVATE);
        }
        sp.edit().putString(key, value).commit();

    }

    /**
     *
     * @param context 上下文环境
     * @param key 存储节点名称
     * @param defvalue 没有此节点的默认值
     * @return 默认值或者此节点读取到的结果
     */
    public static String getString(Context context, String key, String defvalue) {
        if (sp == null) {
            //（1.存储节点文件名称，2.读写方式）
            sp = context.getSharedPreferences("config", Context.MODE_PRIVATE);
        }
        return sp.getString(key, defvalue);

    }


    /**
     *从sp中移除指定节点
     * @param ctx 上下文环境
     * @param key  需要移除方法的名称
     */
    public static void remove(Context ctx , String key) {
        if (sp == null) {
            //（1.存储节点文件名称，2.读写方式）
            sp = ctx.getSharedPreferences("config", Context.MODE_PRIVATE);
        }
        sp.edit().remove(key).commit();

    }
}

