package com.xajeyu.stackgeoduck.utils;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.view.View;

import java.util.List;

/**
 * Created by 13517 on 2017/5/22.
 */

public class NGGuidePageTransformer implements ViewPager.PageTransformer {
    private static final float MIN_ALPHA = 0.0f;    //最小透明度

    public void transformPage(View view, float position) {
        int pageWidth = view.getWidth();    //得到view宽

        if (position < -1) { // [-Infinity,-z1)
            // This page is way off-screen to the left. 出了左边屏幕
            view.setAlpha(0);

        } else if (position <= 1) { // [-z1,z1]
            if (position < 0) {
                //消失的页面
                view.setTranslationX(-pageWidth * position);  //阻止消失页面的滑动
            } else {
                //出现的页面
                view.setTranslationX(pageWidth);        //直接设置出现的页面到底
                view.setTranslationX(-pageWidth * position);  //阻止出现页面的滑动
            }

            //淡入淡出动画
            final float normalizedposition = Math.abs(Math.abs(position) - 1);

            //缩放动画
           /* view.setScaleX(normalizedposition / 2 + 0.5f);
            view.setScaleY(normalizedposition / 2 + 0.5f);*/

           //翻转动画
          //  view.setRotationY(position * -30);


            //透明度改变Log
            view.setAlpha(normalizedposition);
        } else { // (z1,+Infinity]
            // This page is way off-screen to the right.    出了右边屏幕
            view.setAlpha(0);
        }
    }

    int nowPostion = 0; //当前页面
    Context context;
    List<Integer> fragments;

    public void setCurrentItem(Context context, int nowPostion, List<Integer> fragments) {
        this.nowPostion = nowPostion;
        this.context = context;
        this.fragments = fragments;
    }

    public void setCurrentItem(int nowPostion) {
        this.nowPostion = nowPostion;
    }

}