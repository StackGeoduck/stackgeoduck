package com.xajeyu.stackgeoduck.utils;

/**
 * @PackageName: com.xajeyu.bilibili.utils
 * @FileName: GetTimeStamp.java
 * @CreationTime: 2017/4/20  20:49
 * @author: XajeYu
 * @E-mail: xajeyu@gmail.com
 * @function: 获取当前系统时间戳
 */
public class GetTimeStamp {
    private static class GetTimeStampHolder{
        /**
         * 运用静态代码块优化单例模式
         */
        private static final GetTimeStamp instance = new GetTimeStamp();
    }

    /**
     * 获取实例
     * @return
     */
    public static GetTimeStamp getInstance() {
        return GetTimeStampHolder.instance;
    }

    /**
     * 获取当前时间戳
     * @return 返回以转换好的时间戳
     */
    public String getTimeStamp(){
        long time = System.currentTimeMillis();
        return String.valueOf(time);
    }
}
