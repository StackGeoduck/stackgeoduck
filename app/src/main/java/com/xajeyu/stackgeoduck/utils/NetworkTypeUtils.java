package com.xajeyu.stackgeoduck.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;

import static com.xajeyu.stackgeoduck.utils.NetworkTypeUtils.NetwornType.NETWORN_4G;
import static com.xajeyu.stackgeoduck.utils.NetworkTypeUtils.NetwornType.NETWORN_MOBILE;
import static com.xajeyu.stackgeoduck.utils.NetworkTypeUtils.NetwornType.NETWORN_NONE;

/**
 * Created by e on 2017/5/11.
 */

public class NetworkTypeUtils {

    /**
     * 当前网络类型
     */
    public NetwornType mNetWorkType;

    /**
     * 网络类型
     */
    public enum NetwornType {
        //无网络
        NETWORN_NONE,
        NETWORN_WIFI,
        NETWORN_MOBILE,
        NETWORN_2G,
        NETWORN_3G,
        NETWORN_4G;
    }

    /**
     * 获取NetworkTypeUtils实例
     *
     * @return NetworkTypeUtils
     */
    public static NetworkTypeUtils getNetworkTypeUtils() {
        return new NetworkTypeUtils();
    }

    /**
     * 获取NetworkInfo
     *
     * @param context 上下文
     * @return NetworkInfo
     */
    private NetworkInfo getNetwork(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo;
    }

    /**
     * /**
     * 判断网络类型
     * WIFI、2G、3G、4G
     *
     * @param context           上下文
     * @param isSpecificNetwork true判断具体2g、3g、4g    false直接返回移动网络
     * @return 网络类型
     */
    public NetwornType isNetworkType(Context context, boolean isSpecificNetwork) {
        NetworkInfo network = getNetwork(context);
        //判断当前是否有网络
        if (network != null && network.isConnected()) {
            String typeName = network.getTypeName();
            //比较字符串,忽略大小写
            if (typeName.equalsIgnoreCase("WIFI")) {    //WIFI
                mNetWorkType = NetwornType.NETWORN_WIFI;
            } else if (typeName.equalsIgnoreCase("MOBILE")) {    //移动网络
                if (isSpecificNetwork) {
                    mNetWorkType = isFastMobileNetwork(context);
                } else {
                    mNetWorkType = NETWORN_MOBILE;
                }
            }
        } else {
            //无网络
            mNetWorkType = NetwornType.NETWORN_NONE;
        }
        return mNetWorkType;
    }

    /**
     * 具体判断2G、3G、4G
     *
     * @param context 上下文
     * @return 网络类型
     */
    private NetwornType isFastMobileNetwork(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        switch (telephonyManager.getNetworkType()) {
            case TelephonyManager.NETWORK_TYPE_GPRS: // 联通2g
            case TelephonyManager.NETWORK_TYPE_CDMA: // 电信2g
            case TelephonyManager.NETWORK_TYPE_EDGE: // 移动2g
            case TelephonyManager.NETWORK_TYPE_1xRTT:
            case TelephonyManager.NETWORK_TYPE_IDEN:
                return NetwornType.NETWORN_2G;
            //如果是3g类型
            case TelephonyManager.NETWORK_TYPE_EVDO_A: // 电信3g
            case TelephonyManager.NETWORK_TYPE_UMTS:
            case TelephonyManager.NETWORK_TYPE_EVDO_0:
            case TelephonyManager.NETWORK_TYPE_HSDPA:
            case TelephonyManager.NETWORK_TYPE_HSUPA:
            case TelephonyManager.NETWORK_TYPE_HSPA:
            case TelephonyManager.NETWORK_TYPE_EVDO_B:
            case TelephonyManager.NETWORK_TYPE_EHRPD:
            case TelephonyManager.NETWORK_TYPE_HSPAP:
                return NetwornType.NETWORN_3G;
            //如果是4g类型
            case TelephonyManager.NETWORK_TYPE_LTE:
                return NETWORN_4G;
        }
        return NETWORN_NONE;
    }
}
