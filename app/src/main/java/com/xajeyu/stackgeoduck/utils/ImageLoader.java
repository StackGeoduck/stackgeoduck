package com.xajeyu.stackgeoduck.utils;

import android.content.Context;
import android.os.Build;
import android.widget.ImageView;

import com.xajeyu.sdk.imageloader.ImageLoaderManager;

/**
 * @PackageName: com.xajeyu.bilibili.utils
 * @FileName: ImageLoader.java
 * @CreationTime: 2017/4/14  16:03
 * @author: XajeYu
 * @E-mail: xajeyu@gmail.com
 * @function: 为Banner轮播器设置图片
 */
public class ImageLoader extends com.youth.banner.loader.ImageLoader {
    @Override
    public void displayImage(Context context, Object path, ImageView imageView) {
        // 图片加载器
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ImageLoaderManager.getInstance(context).display(imageView, (String) path);
        }
    }
}
