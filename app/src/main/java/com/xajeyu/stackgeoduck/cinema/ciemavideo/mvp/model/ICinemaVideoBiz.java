package com.xajeyu.stackgeoduck.cinema.ciemavideo.mvp.model;

import android.app.Activity;

/**
 * Created by e on 2017/5/18.
 */

public interface ICinemaVideoBiz {
    void requestData(Activity activity, CinemaVideoListener cinemaVideoListener);

}
