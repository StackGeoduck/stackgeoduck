package com.xajeyu.stackgeoduck.cinema.cinemaImg.mvp.presenter;

import android.app.Activity;
import android.content.Context;

import com.xajeyu.stackgeoduck.cinema.bean.CinemaDetailsInfo;
import com.xajeyu.stackgeoduck.cinema.cinemaImg.mvp.model.CinemaDetailsImgListener;
import com.xajeyu.stackgeoduck.cinema.cinemaImg.mvp.model.ICinemaDetailsimgBiz;
import com.xajeyu.stackgeoduck.cinema.cinemaImg.mvp.model.imple.CinemaDetailsImgBiz;
import com.xajeyu.stackgeoduck.cinema.cinemaImg.mvp.view.CinemaDetailsImgViews;

/**
 * Created by e on 2017/5/18.
 */

public class CinemaDetailsImgPresenter {
    CinemaDetailsImgViews mCinemaDetailsImgViews;
    ICinemaDetailsimgBiz mCinemaDetailsImgBiz;

    public CinemaDetailsImgPresenter(Context context,Activity activity, CinemaDetailsImgViews cinemaDetailsImgViews) {
        this.mCinemaDetailsImgViews = cinemaDetailsImgViews;
        mCinemaDetailsImgBiz = new CinemaDetailsImgBiz(context,activity);
    }

    public void receiveData() {
        mCinemaDetailsImgBiz.ReceiveData(new CinemaDetailsImgListener() {
            @Override
            public void Success(Object object) {
                mCinemaDetailsImgViews.onSuccess(object);
            }

            @Override
            public void Failed(Object object) {
                mCinemaDetailsImgViews.onFailed(object);
            }

            @Override
            public void showData(CinemaDetailsInfo cinemaDetailsInfo,int position) {
                mCinemaDetailsImgViews.showData(cinemaDetailsInfo,position);
            }
        });
    }
}
