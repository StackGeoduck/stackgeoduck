package com.xajeyu.stackgeoduck.cinema.cinemafragment.mvp.model.imple;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.xajeyu.sdk.http.listener.RequestCallBack;
import com.xajeyu.stackgeoduck.cinema.bean.CinemaInfo;
import com.xajeyu.stackgeoduck.cinema.key.Cinemakey;
import com.xajeyu.stackgeoduck.cinema.cinemafragment.mvp.model.CinemaListener;
import com.xajeyu.stackgeoduck.cinema.cinemafragment.mvp.model.ICinemaBiz;
import com.xajeyu.stackgeoduck.network.http.AppNetwork;
import com.xajeyu.stackgeoduck.utils.ACache;

/**
 * Created by e on 2017/5/17.
 * 业务逻辑层
 */

public class CinemaBiz implements ICinemaBiz {
    private Context context;

    private final ACache mACache;
    private final Gson gson;

    public CinemaBiz(Context context) {
        this.context = context;
        mACache = ACache.get(context);
        gson = new Gson();
    }

    @Override
    public void RequestData(final CinemaListener cinemaListener) {
        //获取缓存
        String asString = mACache.getAsString(Cinemakey.CINEMA_CACHE_KEY);
        //判断是否有缓存
        if (!TextUtils.isEmpty(asString)) {
            //解析json
            CinemaInfo cinemaInfo = gson.fromJson(asString, CinemaInfo.class);
            //设置适配器
            cinemaListener.showData(cinemaInfo);
        }
        getData(cinemaListener);
    }

    /**
     * 获取数据
     *
     * @param cinemaListener
     */
    public void getData(final CinemaListener cinemaListener) {
        //请求网络
        AppNetwork.getInstance().RequestCinema(new RequestCallBack() {
            @Override
            public void onSuccess(Object response) {
                CinemaInfo cinemaInfo = (CinemaInfo) response;
                cinemaListener.showData(cinemaInfo);
                mACache.put(Cinemakey.CINEMA_CACHE_KEY, gson.toJson(cinemaInfo));
                cinemaListener.Success(response);
            }

            @Override
            public void onFail(Object response) {
                cinemaListener.Failed(response);
            }

            @Override
            public void onDownloadProgress(long progress) {

            }
        });
    }


}
