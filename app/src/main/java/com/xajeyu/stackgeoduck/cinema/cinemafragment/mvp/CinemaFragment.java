package com.xajeyu.stackgeoduck.cinema.cinemafragment.mvp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.github.jdsjlzx.interfaces.OnItemClickListener;
import com.github.jdsjlzx.recyclerview.LRecyclerView;
import com.github.jdsjlzx.recyclerview.LRecyclerViewAdapter;
import com.xajeyu.stackgeoduck.R;
import com.xajeyu.stackgeoduck.cinema.adapter.CinemaAdapter;
import com.xajeyu.stackgeoduck.cinema.api.CinemaApi;
import com.xajeyu.stackgeoduck.cinema.bean.CinemaInfo;
import com.xajeyu.stackgeoduck.cinema.cinemadetails.mvp.CinemaDetailsActivity;
import com.xajeyu.stackgeoduck.cinema.cinemafragment.mvp.presenter.CinemaPresenter;
import com.xajeyu.stackgeoduck.cinema.cinemafragment.mvp.view.CinemaViews;
import com.xajeyu.stackgeoduck.view.fragment.Base.BaseFragment;

/**
 * @PackageName: com.xajeyu.bilibili.view.fragment
 * @FileName: CinemaFragment.java
 * @CreationTime: 2017/4/13  14:31
 * @author: XajeYu
 * @E-mail: xajeyu@gmail.com
 * @function: 影院Fragmentt
 */
public class CinemaFragment extends BaseFragment implements CinemaViews {

    private LRecyclerView mLRecyclerView;
    private SwipeRefreshLayout srl;
    private CinemaPresenter cinemaPresenter;
    private LRecyclerViewAdapter mLRecyclerViewAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = initView();
        return view;
    }

    public View initView() {
        View view = View.inflate(getActivity(), R.layout.fragment_found, null);
        mLRecyclerView = (LRecyclerView) view.findViewById(R.id.LRecyclerView);
        srl = (SwipeRefreshLayout) view.findViewById(R.id.cinema_srl);
        //刷新时的颜色变化
        srl.setColorSchemeResources(android.R.color.holo_blue_bright, android.R.color.holo_green_light,
                android.R.color.holo_orange_light, android.R.color.holo_red_light);
        //设置首次加载数据显示刷新动画
        srl.setRefreshing(true);
        srl.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                cinemaPresenter.RequestData();
                srl.setRefreshing(false);
            }
        });
        return view;
    }

    public void setAdapter(final CinemaInfo cinemaInfo) {
        mLRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        CinemaAdapter cinemaAdapter = new CinemaAdapter(getContext(), cinemaInfo);
        mLRecyclerViewAdapter = new LRecyclerViewAdapter(cinemaAdapter);
        mLRecyclerView.setAdapter(mLRecyclerViewAdapter);
        //设置是否使用下拉刷新
        mLRecyclerView.setPullRefreshEnabled(false);
        //设置是否使用上拉加载
        mLRecyclerView.setLoadMoreEnabled(false);
        mLRecyclerViewAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(getContext(), CinemaDetailsActivity.class);
                intent.putExtra("url", CinemaApi.CINEMA_MIAN_URL + (int) cinemaInfo.data.movies.get(position).id + CinemaApi.CINEMA_DETAILS_URL);
                getContext().startActivity(intent);
            }
        });
    }

    @Override
    public void initData() {
        cinemaPresenter = new CinemaPresenter(getContext(), this);
        cinemaPresenter.RequestData();
    }

    @Override
    public void onSuccess(Object object) {
        srl.setRefreshing(false);
    }

    @Override
    public void onFailed(Object object) {
        Toast.makeText(getContext(), "当前网络不可用", Toast.LENGTH_SHORT).show();
        srl.setRefreshing(false);
    }

    @Override
    public void showData(CinemaInfo cinemaInfo) {
        setAdapter(cinemaInfo);
    }
}
