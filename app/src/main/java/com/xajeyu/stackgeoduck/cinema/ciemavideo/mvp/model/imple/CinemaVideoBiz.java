package com.xajeyu.stackgeoduck.cinema.ciemavideo.mvp.model.imple;

import android.app.Activity;
import android.content.Intent;

import com.xajeyu.sdk.http.listener.RequestCallBack;
import com.xajeyu.stackgeoduck.cinema.api.CinemaApi;
import com.xajeyu.stackgeoduck.cinema.bean.CinemaDetailsInfo;
import com.xajeyu.stackgeoduck.cinema.ciemavideo.mvp.model.CinemaVideoListener;
import com.xajeyu.stackgeoduck.cinema.ciemavideo.mvp.model.ICinemaVideoBiz;
import com.xajeyu.stackgeoduck.cinema.ciemavideo.mvp.view.CinemaVideoViews;
import com.xajeyu.stackgeoduck.network.http.AppNetwork;

/**
 * Created by e on 2017/5/18.
 */

public class CinemaVideoBiz implements ICinemaVideoBiz {

    CinemaVideoViews cinemaVideoViews;
    CinemaVideoListener cinemaVideoListener;
    Activity activity;

    @Override
    public void requestData(Activity activity,CinemaVideoListener cinemaVideoListener) {
        this.cinemaVideoListener = cinemaVideoListener;
        this.activity = activity;
        getData(cinemaVideoListener);
    }

    /**
     * 获取数据
     */
    public void getData(final CinemaVideoListener cinemaVideoListener) {
        Intent intent = activity.getIntent();
        float id = intent.getFloatExtra("ID", 0);
        AppNetwork.getInstance().RequestCinemaDetails(CinemaApi.CINEMA_MIAN_URL + (int) id + CinemaApi.CINEMA_DETAILS_URL, new RequestCallBack() {
            @Override
            public void onSuccess(Object response) {
                CinemaDetailsInfo cinemaDetailsInfo = (CinemaDetailsInfo) response;

                cinemaVideoListener.showData(cinemaDetailsInfo);
            }

            @Override
            public void onFail(Object response) {
                cinemaVideoListener.Failed(response);


            }

            @Override
            public void onDownloadProgress(long progress) {

            }
        });
    }
}
