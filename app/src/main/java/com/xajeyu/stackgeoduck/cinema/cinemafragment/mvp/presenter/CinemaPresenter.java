package com.xajeyu.stackgeoduck.cinema.cinemafragment.mvp.presenter;

import android.content.Context;

import com.xajeyu.stackgeoduck.cinema.bean.CinemaInfo;
import com.xajeyu.stackgeoduck.cinema.cinemafragment.mvp.model.imple.CinemaBiz;
import com.xajeyu.stackgeoduck.cinema.cinemafragment.mvp.model.CinemaListener;
import com.xajeyu.stackgeoduck.cinema.cinemafragment.mvp.model.ICinemaBiz;
import com.xajeyu.stackgeoduck.cinema.cinemafragment.mvp.view.CinemaViews;

/**
 * Created by e on 2017/5/17.
 * 影院中间人
 */

public class CinemaPresenter implements CinemaListener{
    private ICinemaBiz mICinemaBiz;
    private CinemaViews mCinemaViews;
    private Context context;

    public CinemaPresenter(Context context,CinemaViews cinemaViews) {
        this.context = context;
        this.mCinemaViews = cinemaViews;
        mICinemaBiz = new CinemaBiz(context);
    }

    public void RequestData() {
        mICinemaBiz.RequestData(this);
    }

    @Override
    public void Success(Object object) {
        mCinemaViews.onSuccess(object);
    }

    @Override
    public void Failed(Object object) {
        mCinemaViews.onFailed(object);
    }

    @Override
    public void showData(CinemaInfo cinemaInfo) {
        mCinemaViews.showData(cinemaInfo);
    }

}
