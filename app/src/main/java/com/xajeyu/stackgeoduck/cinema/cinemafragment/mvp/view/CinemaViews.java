package com.xajeyu.stackgeoduck.cinema.cinemafragment.mvp.view;

import com.xajeyu.stackgeoduck.cinema.bean.CinemaInfo;

/**
 * Created by e on 2017/5/17.
 * View层
 */

public interface CinemaViews {
    /**
     * 成功
     * @param object
     */
    void onSuccess(Object object);

    /**
     * 失败
     * @param object
     */
    void onFailed(Object object);

    /**
     * 显示数据
     * @param cinemaInfo
     */
    void showData(CinemaInfo cinemaInfo);
}
