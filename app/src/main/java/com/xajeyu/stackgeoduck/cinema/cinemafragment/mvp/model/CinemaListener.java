package com.xajeyu.stackgeoduck.cinema.cinemafragment.mvp.model;

import com.xajeyu.stackgeoduck.cinema.bean.CinemaInfo;

/**
 * Created by e on 2017/5/17.  通知外界
 */

public interface CinemaListener {
    /**
     * 成功
     * @param object
     */
    void Success(Object object);

    /**
     * 失败
     * @param object
     */
    void Failed(Object object);

    /**
     * 显示数据
     * @param cinemaInfo
     */
    void showData(CinemaInfo cinemaInfo);


}
