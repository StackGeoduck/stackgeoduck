package com.xajeyu.stackgeoduck.cinema.cinemadetails.mvp;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.github.jdsjlzx.recyclerview.LRecyclerView;
import com.github.jdsjlzx.recyclerview.LRecyclerViewAdapter;
import com.google.gson.Gson;
import com.xajeyu.sdk.imageloader.ImageLoaderManager;
import com.xajeyu.stackgeoduck.R;
import com.xajeyu.stackgeoduck.activity.base.BaseActivity;
import com.xajeyu.stackgeoduck.cinema.adapter.CinemaDetailsAdapter;
import com.xajeyu.stackgeoduck.cinema.adapter.CinemaDetailsMediaAdapter;
import com.xajeyu.stackgeoduck.cinema.bean.CinemaDetailsInfo;
import com.xajeyu.stackgeoduck.cinema.cinemaImg.mvp.ImgActivity;
import com.xajeyu.stackgeoduck.cinema.cinemadetails.mvp.presenter.CinemaDetailsPresenter;
import com.xajeyu.stackgeoduck.cinema.cinemadetails.mvp.view.CinemaDetailsViews;
import com.xajeyu.stackgeoduck.utils.DividerItemDecoration;
import com.xajeyu.stackgeoduck.view.customview.SetCanNotScrollLayoutManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import jp.wasabeef.glide.transformations.BlurTransformation;


/**
 * Created by e on 2017/5/5.
 */

public class CinemaDetailsActivity extends BaseActivity implements CinemaDetailsViews {
    /**
     * 短评
     */
    @BindView(R.id.cinema_details_commentary_lrv)
    LRecyclerView mCommentaryLRecyclerView;
    /**
     * head高度
     */
    private int height;
    /**
     * 中文电影名
     */
    @BindView(R.id.cinema_details_head_chinese_movie_name)
    TextView mChineseName;
    /**
     * 英文电影名
     */
    @BindView(R.id.cinema_details_head_english_movie_name)
    TextView mEnglishName;
    /**
     * 顶部渐渐透明Toolbar
     */
    @BindView(R.id.cinema_top_toolbar)
    Toolbar toolbar;
    /**
     * 电影图标
     */
    @BindView(R.id.cinema_details_head_img)
    ImageView icon;
    /**
     * 观众评分
     */
    @BindView(R.id.cinema_details_head_audience_rating)
    TextView mAudienceRating;
    /**
     * 得分
     */
    @BindView(R.id.cinema_details_head_score)
    TextView mScore;
    /**
     * 评分人数量
     */
    @BindView(R.id.cinema_details_head_quantity)
    TextView mQuantity;
    /**
     * 电影类型
     */
    @BindView(R.id.cinema_details_head_type)
    TextView mType;
    /**
     * 地区和电影时长
     */
    @BindView(R.id.cinema_details_head_area_with_duration)
    TextView mAreaWithDuration;
    /**
     * 上映时间
     */
    @BindView(R.id.cinema_details_head_release_date)
    TextView mReleaseDate;
    /**
     * 高斯模糊背景
     */
    @BindView(R.id.bg_Img)
    ImageView bgImg;
    /**
     * 电影简介收缩图片
     */
    @BindView(R.id.cinema_details_introduction_img)
    ImageView shrinkImg;
    /**
     * 电影简介收缩文字
     */
    @BindView(R.id.cinema_details_introduction_text)
    TextView shrinkText;
    /**
     * 包裹电影简介的布局
     */
    @BindView(R.id.cinema_details_shrink_llayout)
    LinearLayout shrinkLayout;
    /**
     * 媒体库(图片)
     */
    @BindView(R.id.cinema_details_media_lrv)
    RecyclerView mMediavRecyclerView;


    /**
     * Toolbar文字
     */
    @BindView(R.id.cinema_details_toolbar_name)
    TextView mToolbarName;
    /**
     * 全局滚动控件
     */
    @BindView(R.id.cinema_details_scrollview)
    NestedScrollView mScrollView;

    Context context = this;
    private boolean isShrink;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cinema);
        //绑定
        ButterKnife.bind(this, this);
        initData();
    }

    private void initData() {
        CinemaDetailsPresenter cinemaDetailsPresenter = new CinemaDetailsPresenter(getApplicationContext(), this, this);
        cinemaDetailsPresenter.RequestData();
    }

    @Override
    public void onSuccess(Object object) {
    }

    @Override
    public void onFailed(Object object) {
        Toast.makeText(this, "当前网络不可用", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showData(CinemaDetailsInfo cinemaDetailsInfo) {
        setHeadData(cinemaDetailsInfo);
        setShrink(cinemaDetailsInfo);
        setMediaAdapter(cinemaDetailsInfo);
        setCommentaryAdapter(cinemaDetailsInfo);
    }

    /**
     * 初始化头布局数据
     *
     * @param cinemaDetailsInfo
     */
    public void setHeadData(CinemaDetailsInfo cinemaDetailsInfo) {
        ImageLoaderManager.getInstance(context).display(icon, cinemaDetailsInfo.data.MovieDetailModel.img);
        //设置高斯模糊
        Glide.with(context)
                .load(cinemaDetailsInfo.data.MovieDetailModel.img)
                .bitmapTransform(new BlurTransformation(context, 50))
                .into(bgImg);
        mChineseName.setText(cinemaDetailsInfo.data.MovieDetailModel.nm);
        mScore.setText(cinemaDetailsInfo.data.MovieDetailModel.sc + "");
        mQuantity.setText("(" + (int) cinemaDetailsInfo.data.MovieDetailModel.snum + "人评)");
        mType.setText(cinemaDetailsInfo.data.MovieDetailModel.cat);
        mAreaWithDuration.setText(cinemaDetailsInfo.data.MovieDetailModel.src + " / " + (int) cinemaDetailsInfo.data.MovieDetailModel.dur + "分钟");
        mReleaseDate.setText(cinemaDetailsInfo.data.MovieDetailModel.rt);
        mToolbarName.setText(cinemaDetailsInfo.data.MovieDetailModel.nm);
        height = toolbar.getLayoutParams().height;
        //设置滑动Toolbar慢慢显示
        mScrollView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY <= 0) {   //设置标题的背景颜色
                    toolbar.setBackgroundColor(Color.TRANSPARENT);
                    mToolbarName.setTextColor(Color.TRANSPARENT);
                } else if (scrollY > 0 && scrollY <= height) { //滑动距离小于banner图的高度时，设置背景和字体颜色颜色透明度渐变
                    float scale = (float) scrollY / height;
                    System.out.println(scrollY + "-----" + height);
                    float alpha = (255 * scale);
                    toolbar.setBackgroundColor(Color.argb((int) alpha, 2, 191, 205));
                    mToolbarName.setTextColor(Color.argb((int) alpha, 255, 255, 255));
                } else {    //滑动到banner下面设置普通颜色
                    toolbar.setBackgroundColor(context.getResources().getColor(R.color.overall));
                    mToolbarName.setTextColor(Color.WHITE);
                }
            }
        });

    }

    /**
     * 电影简介收缩布局初始化
     */
    public void setShrink(CinemaDetailsInfo cinemaDetailsInfo) {
        isShrink = true;
        //将dra中的<p>和</p>替换成空字符串
        String dra = cinemaDetailsInfo.data.MovieDetailModel.dra;
        dra = dra.replace("<p>", "");
        dra = dra.replace("</p>", "");
        shrinkText.setText(dra);
        shrinkText.setMaxLines(3);
        shrinkLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isShrink) {
                    setRotateAinimation(shrinkImg, 0f, 180f);
                    shrinkText.setMaxLines(Integer.MAX_VALUE);
                } else {
                    setRotateAinimation(shrinkImg, 180f, 0f);
                    shrinkText.setMaxLines(3);
                }
                isShrink = !isShrink;
            }
        });
    }

    /**
     * 旋转动画
     *
     * @param view  要旋转的View
     * @param start 动画开始位置
     * @param end   动画结束位置
     */
    public void setRotateAinimation(View view, float start, float end) {
        Animation animation = new RotateAnimation(start, end, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        animation.setFillAfter(true);
        animation.setDuration(200);
        view.startAnimation(animation);
    }

    public void setMediaAdapter(final CinemaDetailsInfo cinemaDetailsInfo) {
        CinemaDetailsMediaAdapter mCinemaDetailsMediaAdapter = new CinemaDetailsMediaAdapter(context, cinemaDetailsInfo);
        //设置一行,横向
        mMediavRecyclerView.setLayoutManager(new StaggeredGridLayoutManager(1,
                StaggeredGridLayoutManager.HORIZONTAL));
        mMediavRecyclerView.setAdapter(mCinemaDetailsMediaAdapter);
        mCinemaDetailsMediaAdapter.setmOnItemClickListener(new CinemaDetailsMediaAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int postion) {
                Intent intent = new Intent(context, ImgActivity.class);
                //把对象转成json字符串
                intent.putExtra("url", new Gson().toJson(cinemaDetailsInfo));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                Bundle bundle = new Bundle();
                bundle.putInt("postion", postion);
                intent.putExtra("bundle", bundle);
                context.startActivity(intent);
            }
        });
    }

    public void setCommentaryAdapter(CinemaDetailsInfo cinemaDetailsInfo) {
        CinemaDetailsAdapter cinemaDetailsAdapter = new CinemaDetailsAdapter(context, cinemaDetailsInfo);
        LRecyclerViewAdapter mLRecyclerViewAdapter = new LRecyclerViewAdapter(cinemaDetailsAdapter);
        SetCanNotScrollLayoutManager setCanNotScrollLayoutManager = new SetCanNotScrollLayoutManager(context);
        //禁止LRecyclerView滚动
        setCanNotScrollLayoutManager.setScrollEnabled(false);
        //设置布局管理器
        mCommentaryLRecyclerView.setLayoutManager(setCanNotScrollLayoutManager);
        //添加分割线
        mCommentaryLRecyclerView.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL_LIST));
        //设置适配器
        mCommentaryLRecyclerView.setAdapter(mLRecyclerViewAdapter);
        //设置上拉加载和下拉刷新是否可用
        mCommentaryLRecyclerView.setPullRefreshEnabled(false);
        mCommentaryLRecyclerView.setLoadMoreEnabled(false);
    }

}
