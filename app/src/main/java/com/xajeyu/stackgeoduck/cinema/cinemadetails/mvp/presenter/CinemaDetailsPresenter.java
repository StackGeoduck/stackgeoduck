package com.xajeyu.stackgeoduck.cinema.cinemadetails.mvp.presenter;

import android.app.Activity;
import android.content.Context;

import com.xajeyu.stackgeoduck.cinema.bean.CinemaDetailsInfo;
import com.xajeyu.stackgeoduck.cinema.cinemadetails.mvp.model.CinemaDetailsListener;
import com.xajeyu.stackgeoduck.cinema.cinemadetails.mvp.model.ICinemaDetailsBiz;
import com.xajeyu.stackgeoduck.cinema.cinemadetails.mvp.model.imple.CinemaDetailsBiz;
import com.xajeyu.stackgeoduck.cinema.cinemadetails.mvp.view.CinemaDetailsViews;

/**
 * Created by e on 2017/5/17.
 */

public class CinemaDetailsPresenter implements CinemaDetailsListener {
    private ICinemaDetailsBiz mCinemaDetailsBiz;
    private CinemaDetailsViews mCinemaDetailsViews;

    public CinemaDetailsPresenter(Context context, Activity activity, CinemaDetailsViews cinemaDetailsViews) {
        this.mCinemaDetailsViews = cinemaDetailsViews;
        mCinemaDetailsBiz = new CinemaDetailsBiz(context, activity, cinemaDetailsViews);
    }

    public void RequestData() {
        mCinemaDetailsBiz.RequestData(this);
    }

    @Override
    public void Success(Object object) {
        mCinemaDetailsViews.onSuccess(object);
    }

    @Override
    public void Failed(Object object) {
        mCinemaDetailsViews.onFailed(object);
    }

    @Override
    public void showData(CinemaDetailsInfo cinemaDetailsInfo) {
        mCinemaDetailsViews.showData(cinemaDetailsInfo);
    }

}
