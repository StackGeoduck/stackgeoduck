package com.xajeyu.stackgeoduck.cinema.cinemaImg.mvp.view;

import com.xajeyu.stackgeoduck.cinema.bean.CinemaDetailsInfo;

/**
 * Created by e on 2017/5/18.
 */

public interface CinemaDetailsImgViews {
    void onSuccess(Object object);

    void onFailed(Object object);

    /**
     * 显示数据
     * @param cinemaDetailsInfo  数据
     * @param position  点击的图片位置
     */
    void showData(CinemaDetailsInfo cinemaDetailsInfo, int position);
}
