package com.xajeyu.stackgeoduck.cinema.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by e on 2017/5/4.
 */

public class CinemaInfo {

    /**
     * control : {"expires":1800}
     * status : 0
     * data : {"hasNext":false,"movies":[{"late":false,"cnms":0,"sn":0,"showInfo":"今天29家影院放映39场","pn":188,"img":"http://p0.meituan.net/165.220/movie/fbe5f97c016c9f4520109dc70f458d4d83363.jpg","sc":0,"ver":"2D/3D/IMAX 3D/中国巨幕","showDate":"","src":"","imax":true,"snum":1877,"preSale":1,"vd":"","dir":"詹姆斯·古恩","star":"克里斯·帕拉特,佐伊·索尔达娜,戴夫·巴蒂斯塔","cat":"动作,冒险,科幻","wish":149696,"3d":true,"scm":"星爵身世迷，终于见爹地","rt":"本周五上映","dur":136,"nm":"银河护卫队2","time":"","id":248683},{"late":false,"cnms":0,"sn":0,"showInfo":"今天41家影院放映441场","pn":156,"img":"http://p1.meituan.net/165.220/movie/cc50791238502ae1fa08df764c5f5c97223987.jpg","sc":9.1,"ver":"2D/中国巨幕","showDate":"","src":"","imax":false,"snum":77425,"preSale":0,"vd":"","dir":"邱礼涛","star":"刘德华,姜武,宋佳","cat":"动作,悬疑,犯罪","wish":49007,"3d":false,"scm":"爆炸袭击案，拆弹反恐难","rt":"2017-04-28上映","dur":120,"nm":"拆弹·专家","time":"","id":346103},{"late":false,"cnms":0,"sn":0,"showInfo":"今天41家影院放映313场","pn":206,"img":"http://p1.meituan.net/165.220/movie/e92d824ca3d9e2dcb20622f72162d73d798396.jpg","sc":8.6,"ver":"2D/IMAX 2D/中国巨幕","showDate":"","src":"","imax":true,"snum":86217,"preSale":0,"vd":"","dir":"陈正道","star":"黄渤,徐静蕾,段奕宏","cat":"剧情,悬疑,犯罪","wish":121483,"3d":false,"scm":"记忆难区分，变危险证人","rt":"2017-04-28上映","dur":119,"nm":"记忆大师","time":"","id":345672},{"late":false,"cnms":0,"sn":0,"showInfo":"今天41家影院放映271场","pn":226,"img":"http://p1.meituan.net/165.220/movie/af297f59e363ce96290dfea22f6fea0c153020.jpg","sc":9.4,"ver":"2D/3D/IMAX 3D/中国巨幕/全景声","showDate":"","src":"","imax":true,"snum":858792,"preSale":0,"vd":"","dir":"F·加里·格雷","star":"范·迪塞尔,杰森·斯坦森,道恩·强森","cat":"动作,惊悚,犯罪","wish":320713,"3d":true,"scm":"车王要黑化，家族被击垮","rt":"2017-04-14上映","dur":136,"nm":"速度与激情8","time":"","id":248700},{"late":false,"cnms":0,"sn":0,"showInfo":"今天41家影院放映194场","pn":208,"img":"http://p1.meituan.net/165.220/movie/ea0131b3e9e40f226c7c2664f6185a3792752.jpg","sc":8.9,"ver":"2D","showDate":"","src":"","imax":false,"snum":58268,"preSale":0,"vd":"","dir":"许宏宇","star":"金城武,周冬雨,孙艺洲","cat":"喜剧,爱情","wish":30224,"3d":false,"scm":"美食嘉年华，爱情甜掉牙","rt":"2017-04-27上映","dur":107,"nm":"喜欢·你","time":"","id":672175},{"late":false,"cnms":0,"sn":0,"showInfo":"今天38家影院放映183场","pn":223,"img":"http://p0.meituan.net/165.220/movie/ff46b5edfbd2f737219b9eed78e25fa2969775.jpg","sc":8.8,"ver":"2D","showDate":"","src":"","imax":false,"snum":52319,"preSale":0,"vd":"","dir":"彭浩翔","star":"杨千嬅,余文乐,蒋梦婕","cat":"剧情,爱情","wish":73385,"3d":false,"scm":"春娇不再等，志明终长成","rt":"2017-04-28上映","dur":117,"nm":"春娇救志明","time":"","id":672114},{"late":false,"cnms":0,"sn":0,"showInfo":"今天5家影院放映5场","pn":51,"img":"http://p0.meituan.net/165.220/movie/aeb864fa21d578d845b9cefc056e40cb2874891.jpg","sc":9.6,"ver":"2D","showDate":"","src":"","imax":false,"snum":228,"preSale":1,"vd":"","dir":"尼特什·提瓦瑞","star":"阿米尔·汗,萨卡诗·泰瓦,法缇玛·萨那·纱卡","cat":"喜剧,动作,家庭","wish":25367,"3d":false,"scm":"为圆摔跤梦，女儿不心疼","rt":"本周五上映","dur":140,"nm":"摔跤吧！爸爸","time":"","id":588362},{"late":false,"cnms":0,"sn":0,"showInfo":"今天18家影院放映44场","pn":151,"img":"http://p0.meituan.net/165.220/movie/f0cfd38f05f52d6daaca212c2664db1a1770451.jpg","sc":9.1,"ver":"2D","showDate":"","src":"","imax":false,"snum":74363,"preSale":0,"vd":"","dir":"李海蜀,黄彦威","star":"迪丽热巴,张云龙,高伟光","cat":"喜剧,爱情","wish":63295,"3d":false,"scm":"落难富二代，从小被惯坏","rt":"2017-04-20上映","dur":108,"nm":"傲娇与偏见","time":"","id":343669},{"late":false,"cnms":0,"sn":0,"showInfo":"今天19家影院放映32场","pn":115,"img":"http://p1.meituan.net/165.220/movie/6fa181b782a72ff7cb1ef8aa874e1b7c998470.jpg","sc":8.9,"ver":"3D/中国巨幕","showDate":"","src":"","imax":false,"snum":38947,"preSale":0,"vd":"","dir":"凯利·阿斯博瑞","star":"曼迪·帕廷金,黛米·洛瓦托,雷恩·威尔森","cat":"动画,家庭","wish":104643,"3d":true,"scm":"反派格格巫，翻身做男主","rt":"2017-04-21上映","dur":90,"nm":"蓝精灵：寻找神秘村","time":"","id":78610},{"late":false,"cnms":0,"sn":0,"showInfo":"2017-05-12 下周五上映","pn":171,"img":"http://p1.meituan.net/165.220/movie/8aa7f961835f5718f500df066b14e7ac1008173.jpg","sc":0,"ver":"2D/3D/中国巨幕/全景声","showDate":"","src":"","imax":false,"snum":598,"preSale":1,"vd":"","dir":"迪恩·以色列特","star":"戴克·蒙哥马利,娜奥米·斯科特,林路迪","cat":"动作,冒险,科幻","wish":36816,"3d":true,"scm":"恐龙新战队，变身带你飞","rt":"下周五上映","dur":124,"nm":"超凡战队","time":"","id":338499},{"late":false,"cnms":0,"sn":0,"showInfo":"今天11家影院放映14场","pn":47,"img":"http://p1.meituan.net/165.220/movie/3f2a8507925fead7da67675aa3d26d6e2412651.jpg","sc":8.6,"ver":"2D/3D","showDate":"","src":"","imax":false,"snum":2657,"preSale":0,"vd":"","dir":"Maxim Fadeev,Maksim Fadeyev","star":"乔·佩西,乌比·戈德堡,Maksim Chukharyov","cat":"动画,冒险,奇幻","wish":3045,"3d":true,"scm":"大山坏猴王，白狼保村庄","rt":"2017-04-29上映","dur":88,"nm":"灵狼传奇","time":"","id":544704},{"late":false,"cnms":0,"sn":0,"showInfo":"今天9家影院放映9场","pn":9,"img":"http://p1.meituan.net/165.220/movie/0f654af88114c15bf168a32f5a98835c496768.jpg","sc":2.7,"ver":"2D","showDate":"","src":"","imax":false,"snum":3479,"preSale":0,"vd":"","dir":"王良","star":"曾帅,王良,苗青","cat":"恐怖,惊悚,悬疑","wish":652,"3d":false,"scm":"家中不安宁，挖出大案情","rt":"2017-04-27上映","dur":80,"nm":"午夜惊魂路","time":"","id":1196172},{"late":false,"cnms":0,"sn":0,"showInfo":"今天4家影院放映5场","pn":136,"img":"http://p1.meituan.net/165.220/movie/28c727b5a79b8bdffe7d98e54a4d512f61385.jpg","sc":8.7,"ver":"2D","showDate":"","src":"","imax":false,"snum":139438,"preSale":0,"vd":"","dir":"刘镇伟","star":"周星驰,朱茵,莫文蔚","cat":"喜剧,爱情,奇幻","wish":78053,"3d":false,"scm":"紫霞仙寻情,至尊宝娶亲","rt":"2017-04-13上映","dur":110,"nm":"大话西游之大圣娶亲","time":"","id":158},{"late":false,"cnms":0,"sn":0,"showInfo":"今天2家影院放映5场","pn":27,"img":"http://p0.meituan.net/165.220/movie/34ef78339ee461b6f55e64b131516284161066.jpg","sc":3.8,"ver":"2D","showDate":"","src":"","imax":false,"snum":872,"preSale":0,"vd":"","dir":"邢博","star":"王李丹妮,郭艳,倪大红","cat":"惊悚,悬疑,情色","wish":20162,"3d":false,"scm":"寡妇加光棍，神秘女儿村","rt":"2017-04-27上映","dur":88,"nm":"夜闯寡妇村","time":"","id":341135},{"late":false,"cnms":0,"sn":0,"showInfo":"今天2家影院放映4场","pn":45,"img":"http://p1.meituan.net/165.220/movie/bfb78eeaa1ba034bb4fea729c881f0d7923070.jpg","sc":6.5,"ver":"2D","showDate":"","src":"","imax":false,"snum":12962,"preSale":0,"vd":"","dir":"朴裕焕","star":"林依晨,姜武,陈晓","cat":"剧情,悬疑,犯罪","wish":37658,"3d":false,"scm":"少女遭失身，全家被灭门","rt":"2017-04-21上映","dur":93,"nm":"神秘家族","time":"","id":247086},{"late":false,"cnms":0,"sn":0,"showInfo":"今天2家影院放映3场","pn":97,"img":"http://p1.meituan.net/165.220/movie/906c4288c1e8cd9b93d305fe7776c4bd579225.jpg","sc":8.3,"ver":"2D","showDate":"","src":"","imax":false,"snum":5852,"preSale":0,"vd":"","dir":"刘建华","star":"哈雷,黄宏,刘向京","cat":"剧情,灾难","wish":3148,"3d":false,"scm":"忠犬救主人，浴血战狼群","rt":"2017-04-21上映","dur":90,"nm":"血狼犬","time":"","id":1201660},{"late":false,"cnms":0,"sn":0,"showInfo":"2017-05-05 本周五上映","pn":20,"img":"http://p1.meituan.net/165.220/movie/f808d2e9cf3737683dd2c21354c354071761645.jpg","sc":0,"ver":"2D","showDate":"","src":"","imax":false,"snum":2191,"preSale":1,"vd":"","dir":"袁杰","star":"王宁,葛天,来喜","cat":"喜剧","wish":4807,"3d":false,"scm":"咸鱼也有梦，跨越千年追","rt":"本周五上映","dur":96,"nm":"咸鱼传奇","time":"","id":1188417},{"late":false,"cnms":0,"sn":0,"showInfo":"今天1家影院放映1场","pn":101,"img":"http://p0.meituan.net/165.220/movie/__36226341__4007149.jpg","sc":7.5,"ver":"2D","showDate":"","src":"","imax":false,"snum":1124,"preSale":0,"vd":"","dir":"刘镇伟","star":"何炅,徐若瑄,王学兵","cat":"喜剧,爱情,悬疑","wish":2619,"3d":false,"scm":"人妻遭绑架，歹徒竟是他","rt":"2014-03-04上映","dur":93,"nm":"完美假妻168","time":"","id":78281},{"late":false,"cnms":0,"sn":0,"showInfo":"今天1家影院放映1场","pn":248,"img":"http://p1.meituan.net/165.220/movie/b6a6e36d8b40335fc3aeab6953bf18ce322318.jpg","sc":8.4,"ver":"2D","showDate":"","src":"","imax":false,"snum":42663,"preSale":0,"vd":"","dir":"徐静蕾","star":"白百何,黄立行,明道","cat":"动作,犯罪","wish":27666,"3d":false,"scm":"女儿失踪迹，嫌犯失记忆","rt":"2017-03-31上映","dur":95,"nm":"绑架者","time":"","id":344451},{"late":false,"cnms":0,"sn":0,"showInfo":"今天1家影院放映1场","pn":201,"img":"http://p0.meituan.net/165.220/movie/70f7bd4ecc4817ccd8d1287cbcf4dbb7772081.jpg","sc":7.6,"ver":"3D/IMAX 3D/中国巨幕/全景声","showDate":"","src":"","imax":true,"snum":84451,"preSale":0,"vd":"","dir":"鲁伯特·山德斯","star":"斯嘉丽·约翰逊,皮鲁·埃斯贝克,迈克尔·皮特","cat":"动作,科幻,犯罪","wish":56629,"3d":true,"scm":"少佐换嘉丽，攻壳终成立","rt":"2017-04-07上映","dur":107,"nm":"攻壳机动队","time":"","id":246969}]}
     */

    public ControlBean control;
    public int status;
    public DataBean data;

    public static class ControlBean {
        /**
         * expires : 1800
         */

        public int expires;
    }

    public static class DataBean {
        /**
         * hasNext : false
         * movies : [{"late":false,"cnms":0,"sn":0,"showInfo":"今天29家影院放映39场","pn":188,"img":"http://p0.meituan.net/165.220/movie/fbe5f97c016c9f4520109dc70f458d4d83363.jpg","sc":0,"ver":"2D/3D/IMAX 3D/中国巨幕","showDate":"","src":"","imax":true,"snum":1877,"preSale":1,"vd":"","dir":"詹姆斯·古恩","star":"克里斯·帕拉特,佐伊·索尔达娜,戴夫·巴蒂斯塔","cat":"动作,冒险,科幻","wish":149696,"3d":true,"scm":"星爵身世迷，终于见爹地","rt":"本周五上映","dur":136,"nm":"银河护卫队2","time":"","id":248683},{"late":false,"cnms":0,"sn":0,"showInfo":"今天41家影院放映441场","pn":156,"img":"http://p1.meituan.net/165.220/movie/cc50791238502ae1fa08df764c5f5c97223987.jpg","sc":9.1,"ver":"2D/中国巨幕","showDate":"","src":"","imax":false,"snum":77425,"preSale":0,"vd":"","dir":"邱礼涛","star":"刘德华,姜武,宋佳","cat":"动作,悬疑,犯罪","wish":49007,"3d":false,"scm":"爆炸袭击案，拆弹反恐难","rt":"2017-04-28上映","dur":120,"nm":"拆弹·专家","time":"","id":346103},{"late":false,"cnms":0,"sn":0,"showInfo":"今天41家影院放映313场","pn":206,"img":"http://p1.meituan.net/165.220/movie/e92d824ca3d9e2dcb20622f72162d73d798396.jpg","sc":8.6,"ver":"2D/IMAX 2D/中国巨幕","showDate":"","src":"","imax":true,"snum":86217,"preSale":0,"vd":"","dir":"陈正道","star":"黄渤,徐静蕾,段奕宏","cat":"剧情,悬疑,犯罪","wish":121483,"3d":false,"scm":"记忆难区分，变危险证人","rt":"2017-04-28上映","dur":119,"nm":"记忆大师","time":"","id":345672},{"late":false,"cnms":0,"sn":0,"showInfo":"今天41家影院放映271场","pn":226,"img":"http://p1.meituan.net/165.220/movie/af297f59e363ce96290dfea22f6fea0c153020.jpg","sc":9.4,"ver":"2D/3D/IMAX 3D/中国巨幕/全景声","showDate":"","src":"","imax":true,"snum":858792,"preSale":0,"vd":"","dir":"F·加里·格雷","star":"范·迪塞尔,杰森·斯坦森,道恩·强森","cat":"动作,惊悚,犯罪","wish":320713,"3d":true,"scm":"车王要黑化，家族被击垮","rt":"2017-04-14上映","dur":136,"nm":"速度与激情8","time":"","id":248700},{"late":false,"cnms":0,"sn":0,"showInfo":"今天41家影院放映194场","pn":208,"img":"http://p1.meituan.net/165.220/movie/ea0131b3e9e40f226c7c2664f6185a3792752.jpg","sc":8.9,"ver":"2D","showDate":"","src":"","imax":false,"snum":58268,"preSale":0,"vd":"","dir":"许宏宇","star":"金城武,周冬雨,孙艺洲","cat":"喜剧,爱情","wish":30224,"3d":false,"scm":"美食嘉年华，爱情甜掉牙","rt":"2017-04-27上映","dur":107,"nm":"喜欢·你","time":"","id":672175},{"late":false,"cnms":0,"sn":0,"showInfo":"今天38家影院放映183场","pn":223,"img":"http://p0.meituan.net/165.220/movie/ff46b5edfbd2f737219b9eed78e25fa2969775.jpg","sc":8.8,"ver":"2D","showDate":"","src":"","imax":false,"snum":52319,"preSale":0,"vd":"","dir":"彭浩翔","star":"杨千嬅,余文乐,蒋梦婕","cat":"剧情,爱情","wish":73385,"3d":false,"scm":"春娇不再等，志明终长成","rt":"2017-04-28上映","dur":117,"nm":"春娇救志明","time":"","id":672114},{"late":false,"cnms":0,"sn":0,"showInfo":"今天5家影院放映5场","pn":51,"img":"http://p0.meituan.net/165.220/movie/aeb864fa21d578d845b9cefc056e40cb2874891.jpg","sc":9.6,"ver":"2D","showDate":"","src":"","imax":false,"snum":228,"preSale":1,"vd":"","dir":"尼特什·提瓦瑞","star":"阿米尔·汗,萨卡诗·泰瓦,法缇玛·萨那·纱卡","cat":"喜剧,动作,家庭","wish":25367,"3d":false,"scm":"为圆摔跤梦，女儿不心疼","rt":"本周五上映","dur":140,"nm":"摔跤吧！爸爸","time":"","id":588362},{"late":false,"cnms":0,"sn":0,"showInfo":"今天18家影院放映44场","pn":151,"img":"http://p0.meituan.net/165.220/movie/f0cfd38f05f52d6daaca212c2664db1a1770451.jpg","sc":9.1,"ver":"2D","showDate":"","src":"","imax":false,"snum":74363,"preSale":0,"vd":"","dir":"李海蜀,黄彦威","star":"迪丽热巴,张云龙,高伟光","cat":"喜剧,爱情","wish":63295,"3d":false,"scm":"落难富二代，从小被惯坏","rt":"2017-04-20上映","dur":108,"nm":"傲娇与偏见","time":"","id":343669},{"late":false,"cnms":0,"sn":0,"showInfo":"今天19家影院放映32场","pn":115,"img":"http://p1.meituan.net/165.220/movie/6fa181b782a72ff7cb1ef8aa874e1b7c998470.jpg","sc":8.9,"ver":"3D/中国巨幕","showDate":"","src":"","imax":false,"snum":38947,"preSale":0,"vd":"","dir":"凯利·阿斯博瑞","star":"曼迪·帕廷金,黛米·洛瓦托,雷恩·威尔森","cat":"动画,家庭","wish":104643,"3d":true,"scm":"反派格格巫，翻身做男主","rt":"2017-04-21上映","dur":90,"nm":"蓝精灵：寻找神秘村","time":"","id":78610},{"late":false,"cnms":0,"sn":0,"showInfo":"2017-05-12 下周五上映","pn":171,"img":"http://p1.meituan.net/165.220/movie/8aa7f961835f5718f500df066b14e7ac1008173.jpg","sc":0,"ver":"2D/3D/中国巨幕/全景声","showDate":"","src":"","imax":false,"snum":598,"preSale":1,"vd":"","dir":"迪恩·以色列特","star":"戴克·蒙哥马利,娜奥米·斯科特,林路迪","cat":"动作,冒险,科幻","wish":36816,"3d":true,"scm":"恐龙新战队，变身带你飞","rt":"下周五上映","dur":124,"nm":"超凡战队","time":"","id":338499},{"late":false,"cnms":0,"sn":0,"showInfo":"今天11家影院放映14场","pn":47,"img":"http://p1.meituan.net/165.220/movie/3f2a8507925fead7da67675aa3d26d6e2412651.jpg","sc":8.6,"ver":"2D/3D","showDate":"","src":"","imax":false,"snum":2657,"preSale":0,"vd":"","dir":"Maxim Fadeev,Maksim Fadeyev","star":"乔·佩西,乌比·戈德堡,Maksim Chukharyov","cat":"动画,冒险,奇幻","wish":3045,"3d":true,"scm":"大山坏猴王，白狼保村庄","rt":"2017-04-29上映","dur":88,"nm":"灵狼传奇","time":"","id":544704},{"late":false,"cnms":0,"sn":0,"showInfo":"今天9家影院放映9场","pn":9,"img":"http://p1.meituan.net/165.220/movie/0f654af88114c15bf168a32f5a98835c496768.jpg","sc":2.7,"ver":"2D","showDate":"","src":"","imax":false,"snum":3479,"preSale":0,"vd":"","dir":"王良","star":"曾帅,王良,苗青","cat":"恐怖,惊悚,悬疑","wish":652,"3d":false,"scm":"家中不安宁，挖出大案情","rt":"2017-04-27上映","dur":80,"nm":"午夜惊魂路","time":"","id":1196172},{"late":false,"cnms":0,"sn":0,"showInfo":"今天4家影院放映5场","pn":136,"img":"http://p1.meituan.net/165.220/movie/28c727b5a79b8bdffe7d98e54a4d512f61385.jpg","sc":8.7,"ver":"2D","showDate":"","src":"","imax":false,"snum":139438,"preSale":0,"vd":"","dir":"刘镇伟","star":"周星驰,朱茵,莫文蔚","cat":"喜剧,爱情,奇幻","wish":78053,"3d":false,"scm":"紫霞仙寻情,至尊宝娶亲","rt":"2017-04-13上映","dur":110,"nm":"大话西游之大圣娶亲","time":"","id":158},{"late":false,"cnms":0,"sn":0,"showInfo":"今天2家影院放映5场","pn":27,"img":"http://p0.meituan.net/165.220/movie/34ef78339ee461b6f55e64b131516284161066.jpg","sc":3.8,"ver":"2D","showDate":"","src":"","imax":false,"snum":872,"preSale":0,"vd":"","dir":"邢博","star":"王李丹妮,郭艳,倪大红","cat":"惊悚,悬疑,情色","wish":20162,"3d":false,"scm":"寡妇加光棍，神秘女儿村","rt":"2017-04-27上映","dur":88,"nm":"夜闯寡妇村","time":"","id":341135},{"late":false,"cnms":0,"sn":0,"showInfo":"今天2家影院放映4场","pn":45,"img":"http://p1.meituan.net/165.220/movie/bfb78eeaa1ba034bb4fea729c881f0d7923070.jpg","sc":6.5,"ver":"2D","showDate":"","src":"","imax":false,"snum":12962,"preSale":0,"vd":"","dir":"朴裕焕","star":"林依晨,姜武,陈晓","cat":"剧情,悬疑,犯罪","wish":37658,"3d":false,"scm":"少女遭失身，全家被灭门","rt":"2017-04-21上映","dur":93,"nm":"神秘家族","time":"","id":247086},{"late":false,"cnms":0,"sn":0,"showInfo":"今天2家影院放映3场","pn":97,"img":"http://p1.meituan.net/165.220/movie/906c4288c1e8cd9b93d305fe7776c4bd579225.jpg","sc":8.3,"ver":"2D","showDate":"","src":"","imax":false,"snum":5852,"preSale":0,"vd":"","dir":"刘建华","star":"哈雷,黄宏,刘向京","cat":"剧情,灾难","wish":3148,"3d":false,"scm":"忠犬救主人，浴血战狼群","rt":"2017-04-21上映","dur":90,"nm":"血狼犬","time":"","id":1201660},{"late":false,"cnms":0,"sn":0,"showInfo":"2017-05-05 本周五上映","pn":20,"img":"http://p1.meituan.net/165.220/movie/f808d2e9cf3737683dd2c21354c354071761645.jpg","sc":0,"ver":"2D","showDate":"","src":"","imax":false,"snum":2191,"preSale":1,"vd":"","dir":"袁杰","star":"王宁,葛天,来喜","cat":"喜剧","wish":4807,"3d":false,"scm":"咸鱼也有梦，跨越千年追","rt":"本周五上映","dur":96,"nm":"咸鱼传奇","time":"","id":1188417},{"late":false,"cnms":0,"sn":0,"showInfo":"今天1家影院放映1场","pn":101,"img":"http://p0.meituan.net/165.220/movie/__36226341__4007149.jpg","sc":7.5,"ver":"2D","showDate":"","src":"","imax":false,"snum":1124,"preSale":0,"vd":"","dir":"刘镇伟","star":"何炅,徐若瑄,王学兵","cat":"喜剧,爱情,悬疑","wish":2619,"3d":false,"scm":"人妻遭绑架，歹徒竟是他","rt":"2014-03-04上映","dur":93,"nm":"完美假妻168","time":"","id":78281},{"late":false,"cnms":0,"sn":0,"showInfo":"今天1家影院放映1场","pn":248,"img":"http://p1.meituan.net/165.220/movie/b6a6e36d8b40335fc3aeab6953bf18ce322318.jpg","sc":8.4,"ver":"2D","showDate":"","src":"","imax":false,"snum":42663,"preSale":0,"vd":"","dir":"徐静蕾","star":"白百何,黄立行,明道","cat":"动作,犯罪","wish":27666,"3d":false,"scm":"女儿失踪迹，嫌犯失记忆","rt":"2017-03-31上映","dur":95,"nm":"绑架者","time":"","id":344451},{"late":false,"cnms":0,"sn":0,"showInfo":"今天1家影院放映1场","pn":201,"img":"http://p0.meituan.net/165.220/movie/70f7bd4ecc4817ccd8d1287cbcf4dbb7772081.jpg","sc":7.6,"ver":"3D/IMAX 3D/中国巨幕/全景声","showDate":"","src":"","imax":true,"snum":84451,"preSale":0,"vd":"","dir":"鲁伯特·山德斯","star":"斯嘉丽·约翰逊,皮鲁·埃斯贝克,迈克尔·皮特","cat":"动作,科幻,犯罪","wish":56629,"3d":true,"scm":"少佐换嘉丽，攻壳终成立","rt":"2017-04-07上映","dur":107,"nm":"攻壳机动队","time":"","id":246969}]
         */

        public boolean hasNext;
        public List<MoviesBean> movies;

        public static class MoviesBean {
            /**
             * late : false
             * cnms : 0
             * sn : 0
             * showInfo : 今天29家影院放映39场
             * pn : 188
             * img : http://p0.meituan.net/165.220/movie/fbe5f97c016c9f4520109dc70f458d4d83363.jpg
             * sc : 0
             * ver : 2D/3D/IMAX 3D/中国巨幕
             * showDate :
             * src :
             * imax : true
             * snum : 1877
             * preSale : 1
             * vd :
             * dir : 詹姆斯·古恩
             * star : 克里斯·帕拉特,佐伊·索尔达娜,戴夫·巴蒂斯塔
             * cat : 动作,冒险,科幻
             * wish : 149696
             * 3d : true
             * scm : 星爵身世迷，终于见爹地
             * rt : 本周五上映
             * dur : 136
             * nm : 银河护卫队2
             * time :
             * id : 248683
             */

            public boolean late;
            public float cnms;
            public float sn;
            public String showInfo;
            public float pn;
            public String img;
            public float sc;
            public String ver;
            public String showDate;
            public String src;
            public boolean imax;
            public float snum;
            public float preSale;
            public String vd;
            public String dir;
            public String star;
            public String cat;
            public float wish;
            @SerializedName("3d")
            public boolean _$3d;
            public String scm;
            public String rt;
            public float dur;
            public String nm;
            public String time;
            public float id;
        }
    }
}
