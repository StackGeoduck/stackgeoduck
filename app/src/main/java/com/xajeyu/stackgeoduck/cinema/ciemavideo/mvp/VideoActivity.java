package com.xajeyu.stackgeoduck.cinema.ciemavideo.mvp;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.jdsjlzx.recyclerview.LRecyclerView;
import com.github.jdsjlzx.recyclerview.LRecyclerViewAdapter;
import com.universalvideoview.UniversalMediaController;
import com.universalvideoview.UniversalVideoView;
import com.xajeyu.stackgeoduck.R;
import com.xajeyu.stackgeoduck.cinema.adapter.CinemaDetailsAdapter;
import com.xajeyu.stackgeoduck.cinema.bean.CinemaDetailsInfo;
import com.xajeyu.stackgeoduck.cinema.ciemavideo.mvp.presenter.CinemaVideoPresenter;
import com.xajeyu.stackgeoduck.cinema.ciemavideo.mvp.view.CinemaVideoViews;
import com.xajeyu.stackgeoduck.utils.DividerItemDecoration;
import com.xajeyu.stackgeoduck.utils.NetworkTypeUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 影院预告片和评论
 * Created by e on 2017/5/10.
 */

public class VideoActivity extends AppCompatActivity implements CinemaVideoViews {
    @BindView(R.id.bottomLayout)
    View mBottomLayout;
    @BindView(R.id.video_layout)
    View mVideoLayout;
    @BindView(R.id.videoView)
    UniversalVideoView mVideoView;
    @BindView(R.id.media_controller)
    UniversalMediaController mMediaController;

    private int cachedHeight;
    @BindView(R.id.video_continue_to_play_tv)
    TextView mPlayTv;
    @BindView(R.id.video_continue_to_play_llayout)
    LinearLayout mPlayLlayout;
    @BindView(R.id.video_progressBar)
    ProgressBar mProgressBar;
    @BindView(R.id.btn_reload)
    Button mReload;
    @BindView(R.id.video_reload_llyout)
    LinearLayout mReloadLlyout;
    @BindView(R.id.video_comment_lrv)
    LRecyclerView mCommentLRecyclerView;
    /**
     * 中间人
     */
    private CinemaVideoPresenter cinemaVideoPresenter;


    private View headView;
    private TextView nameTv;
    private TextView scoreTv;
    private TextView releaseTimeTv;
    private Context context = this;
    private LRecyclerViewAdapter lRecyclerViewAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        ButterKnife.bind(this, this);
        headView = View.inflate(this, R.layout.cinema_video_head, null);
        nameTv = (TextView) headView.findViewById(R.id.cinema_video_head_name);
        scoreTv = (TextView) headView.findViewById(R.id.cinema_video_head_score);
        releaseTimeTv = (TextView) headView.findViewById(R.id.cinema_video_head_release_time);
        mVideoView.setMediaController(mMediaController);
        //判断当前网络类型
        NetworkTypeUtils.NetwornType networkType = NetworkTypeUtils.getNetworkTypeUtils().isNetworkType(context, false);
        isNetworkType(networkType);
        //实例化中间人
        cinemaVideoPresenter = new CinemaVideoPresenter(this, this);
        cinemaVideoPresenter.RequestData();
        mVideoView.setVideoViewCallback(new UniversalVideoView.VideoViewCallback() {
            @Override
            public void onScaleChange(boolean isFullscreen) {
                if (isFullscreen) {
                    //全屏
                    setLandscape();
                } else {
                    //非全屏
                    setPortrait();
                }
            }

            @Override
            public void onPause(MediaPlayer mediaPlayer) { // 视频暂停
                System.out.println("视频暂停");
            }

            @Override
            public void onStart(MediaPlayer mediaPlayer) { // 视频播放
                System.out.println("视频播放");
                mPlayLlayout.setVisibility(View.GONE);
            }

            @Override
            public void onBufferingStart(MediaPlayer mediaPlayer) {// 视频开始缓冲
                Log.d("TAG", "onBufferingStart UniversalVideoView callback");
            }

            @Override
            public void onBufferingEnd(MediaPlayer mediaPlayer) {//视频结束缓冲
                Log.d("TAG", "onBufferingEnd UniversalVideoView callback");

            }

        });
    }

    /**
     * 设置竖屏视频区域大小
     */
    private void setVideoAreaSize(final CinemaDetailsInfo cinemaDetailsInfo) {
        mVideoLayout.post(new Runnable() {
            @Override
            public void run() {
                int width = mVideoLayout.getWidth();
                int cachedHeight = (int) (width * 405f / 720f);
//                cachedHeight = (int) (width * 3f / 4f);
//                cachedHeight = (int) (width * 9f / 16f);
                ViewGroup.LayoutParams videoLayoutParams = mVideoLayout.getLayoutParams();
                videoLayoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
                videoLayoutParams.height = cachedHeight;
                mVideoLayout.setLayoutParams(videoLayoutParams);
                //设置标题
                mMediaController.setTitle(cinemaDetailsInfo.data.MovieDetailModel.nm);
                mVideoView.setVideoURI(Uri.parse(cinemaDetailsInfo.data.MovieDetailModel.vd));
                mVideoView.requestFocus();
            }
        });
    }

    public void setAdapter(CinemaDetailsInfo cinemaDetailsInfo) {
        CinemaDetailsAdapter cinemaDetailsAdapter = new CinemaDetailsAdapter(context, cinemaDetailsInfo);
        lRecyclerViewAdapter = new LRecyclerViewAdapter(cinemaDetailsAdapter);
        mCommentLRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        mCommentLRecyclerView.setAdapter(lRecyclerViewAdapter);
        LRecyclerViewAdapter lRecyclerViewAdapter1 = lRecyclerViewAdapter;
        mCommentLRecyclerView.setAdapter(lRecyclerViewAdapter1);
        mCommentLRecyclerView.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL_LIST));
        mCommentLRecyclerView.setPullRefreshEnabled(false);
        mCommentLRecyclerView.setLoadMoreEnabled(false);
        setHean(cinemaDetailsInfo);
    }

    private void setHean(CinemaDetailsInfo cinemaDetailsInfo) {
        nameTv.setText(cinemaDetailsInfo.data.MovieDetailModel.nm);
        scoreTv.setText(cinemaDetailsInfo.data.MovieDetailModel.sc + "");
        releaseTimeTv.setText(cinemaDetailsInfo.data.MovieDetailModel.rt);
        lRecyclerViewAdapter.addHeaderView(headView);
    }

    /**
     * 设置竖屏宽高
     */
    public void setPortrait() {
        ViewGroup.LayoutParams layoutParams = mVideoLayout.getLayoutParams();
        layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
        layoutParams.height = cachedHeight;
        mVideoLayout.setLayoutParams(layoutParams);
        mBottomLayout.setVisibility(View.VISIBLE);
    }

    /**
     * 设置横屏宽高
     */
    public void setLandscape() {
        ViewGroup.LayoutParams layoutParams = mVideoLayout.getLayoutParams();
        layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
        layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT;
        mVideoLayout.setLayoutParams(layoutParams);
        //设置全屏时,无关的View消失,以便为视频控件和控制器控件留出最大化的位置
        mBottomLayout.setVisibility(View.GONE);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //判断按下返回键
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            boolean verticalScreen = isVerticalScreen(this);
            //判断当前横竖屏
            if (verticalScreen) {//全屏
                //设置横屏
                //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                //设置竖屏
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                setPortrait();
            } else { //非全屏
                return super.onKeyDown(keyCode, event);
            }
        }
        return false;
    }

    /**
     * 判断横竖屏
     *
     * @param activity
     * @return
     */
    public static boolean isVerticalScreen(Activity activity) {
        Configuration mConfiguration = activity.getResources().getConfiguration(); //获取设置的配置信息
        int ori = mConfiguration.orientation; //获取屏幕方向
        if (ori == mConfiguration.ORIENTATION_LANDSCAPE) {
            //横屏
            return true;
        } else if (ori == mConfiguration.ORIENTATION_PORTRAIT) {
            //竖屏
            return false;
        }
        return false;
    }

    /**
     * 判断当前是移动网络还是Wi-fi
     *
     * @param networnType
     */
    public void isNetworkType(NetworkTypeUtils.NetwornType networnType) {
        switch (networnType) {
            case NETWORN_NONE:
                mProgressBar.setVisibility(View.VISIBLE);
                Toast.makeText(this, "请检查当前网络是否可用", Toast.LENGTH_SHORT).show();
                break;
            case NETWORN_WIFI:
                mVideoView.start();
                break;
            case NETWORN_MOBILE:
                mPlayLlayout.setVisibility(View.VISIBLE);
                mPlayTv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mVideoView.start();
                        mPlayLlayout.setVisibility(View.GONE);
                    }
                });
                break;
        }
    }


    @Override
    public void onSuccess(Object object) {

    }

    @Override
    public void onFailed(Object object) {
        mReloadLlyout.setVisibility(View.VISIBLE);
        mReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mProgressBar.setVisibility(View.GONE);
                mReloadLlyout.setVisibility(View.GONE);
                cinemaVideoPresenter.RequestData();
            }
        });
    }

    @Override
    public void showData(CinemaDetailsInfo cinemaDetailsInfo) {
        setVideoAreaSize(cinemaDetailsInfo);
        setAdapter(cinemaDetailsInfo);
    }

}
