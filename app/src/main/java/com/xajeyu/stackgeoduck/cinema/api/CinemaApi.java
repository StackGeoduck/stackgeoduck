package com.xajeyu.stackgeoduck.cinema.api;

/**
 * Created by e on 2017/5/4.
 */

public class CinemaApi {
    public static final String CINEMA_MIAN_URL = "http://m.maoyan.com/movie/";
    /**
     * 电影首页
     */
    public static final String CINEMA_HOME_URL = "list.json?type=hot&offset=0&limit=1000";
    /**
     * 电影详情  id.json
     */
    public static final String CINEMA_DETAILS_URL = ".json";
    /**
     * 加载更多评论
     * movieid=id
     * limit=数据开始位置
     * offset=偏移量
     */
    public static final String CINEMA_MORE_COMMENTARY_URL = "http://m.maoyan.com/comments.json";
    /**
     * ID
     * 示例：?movieid=248683
     */
    public static final String CINEMA_MORE_ID = "?movieid=";
    /**
     * 请求多少条数据
     * 示例：&limit=10
     */
    public static final String CINEMA_MORE_limit = "&limit=";
    /**
     * 从那个位置开始拿
     * 示例：&offset=7
     */
    public static final String CINEMA_MORE_offse = "&offset=";

}
