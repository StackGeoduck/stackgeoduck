package com.xajeyu.stackgeoduck.cinema.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.xajeyu.sdk.imageloader.ImageLoaderManager;
import com.xajeyu.stackgeoduck.R;
import com.xajeyu.stackgeoduck.cinema.bean.CinemaDetailsInfo;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by e on 2017/5/9.
 */

public class CinemaDetailsMediaAdapter extends RecyclerView.Adapter implements View.OnClickListener {
    private CinemaDetailsInfo mCinemaDetailsInfo;
    private Context mContext;
    private OnItemClickListener mOnItemClickListener = null;
    public CinemaDetailsMediaAdapter(Context context, CinemaDetailsInfo cinemaDetailsInfo) {
        this.mContext = context;
        this.mCinemaDetailsInfo = cinemaDetailsInfo;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_cinema_details_media, parent, false);
        view.setOnClickListener(this);
        return new itemViewHodler(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        holder.itemView.setTag(position);
        itemViewHodler itemViewHodler = (CinemaDetailsMediaAdapter.itemViewHodler) holder;
        System.out.println("位置---" + position);
        //图片
        String photos = mCinemaDetailsInfo.data.MovieDetailModel.photos.get(position);
        photos = photos.replace("w.h/", "");
        ImageLoaderManager.getInstance(mContext).display(itemViewHodler.img, photos);
    }

    @Override
    public int getItemCount() {
        return mCinemaDetailsInfo.data.MovieDetailModel.photos.size();
    }

    @Override
    public void onClick(View v) {
        if (mOnItemClickListener != null){
            mOnItemClickListener.onItemClick(v, (Integer) v.getTag());
        }
    }

    class itemViewHodler extends RecyclerView.ViewHolder {
        @BindView(R.id.cinema_details_media_img)
        ImageView img;

        public itemViewHodler(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int postion);
    }
    public void setmOnItemClickListener(OnItemClickListener listener){
        this.mOnItemClickListener = listener;
    }
}
