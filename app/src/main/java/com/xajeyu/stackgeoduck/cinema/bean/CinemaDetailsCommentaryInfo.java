package com.xajeyu.stackgeoduck.cinema.bean;

import java.util.List;

/**
 * Created by e on 2017/5/10.
 */

public class CinemaDetailsCommentaryInfo {

    /**
     * control : {"expires":300}
     * status : 0
     * data : {"CommentResponseModel":{"total":39148,"cmts":[{"vipInfo":"","nickName":"qqb134452376","userId":103400420,"score":4,"avatarurl":"https://img.meituan.net/avatar/1f9a369acef431e1df96c3f53789dc5f82232.jpg","nick":"qqb134452376","approve":0,"oppose":0,"reply":0,"time":"2017-05-10 09:47","id":105713441,"content":"剧情很精彩，如果有粤语版的就更好了"},{"vipInfo":"","nickName":"我怀念啊我的小镇","userId":85722268,"score":5,"avatarurl":"http://p0.meituan.net/movie/__40465654__9539763.png","nick":"我怀念啊我的小镇","approve":0,"oppose":0,"reply":0,"time":"2017-05-10 09:47","id":105713428,"content":"电影很好看。"},{"vipInfo":"","nickName":"kBV174966209","userId":742100219,"score":2,"avatarurl":"http://p0.meituan.net/movie/__40465654__9539763.png","nick":"kBV174966209","approve":0,"oppose":0,"reply":0,"time":"2017-05-10 09:47","id":105713403,"content":"就是去看看华仔"},{"vipInfo":"","nickName":"kcG338766370","userId":668318320,"score":5,"avatarurl":"http://p0.meituan.net/movie/__40465654__9539763.png","nick":"kcG338766370","approve":0,"oppose":0,"reply":0,"time":"2017-05-10 09:47","id":105713394,"content":"结局有点草率"},{"vipInfo":"","nickName":"aihongju","userId":140954934,"score":5,"avatarurl":"http://p0.meituan.net/movie/__40465654__9539763.png","nick":"aihongju","approve":0,"oppose":0,"reply":0,"time":"2017-05-10 09:46","id":105713333,"content":"小鲜肉警察死的时候说要远离人群车群的时候我哭了，这是我印象最深刻的片段"},{"vipInfo":"","nickName":"爱德华Tony2016","userId":392901085,"score":4.5,"avatarurl":"https://img.meituan.net/avatar/e760c1e3128838df09d89a8b337299b017536.jpg","nick":"爱德华Tony2016","approve":0,"oppose":0,"reply":0,"time":"2017-05-10 09:45","id":105713295,"content":"好久没有看到这么精彩的港产的警匪枪战片了，华仔的表演真的很精彩，很不错，很喜欢。"},{"vipInfo":"","nickName":"暁芳305287414","userId":62629472,"score":4.5,"avatarurl":"https://img.meituan.net/avatar/6d8a551e6d178a6d147f95a0f36eb87479838.jpg","nick":"暁芳305287414","approve":0,"oppose":0,"reply":0,"time":"2017-05-10 09:43","id":105713192,"content":"不愧是刘德华主演的，很有正能量"},{"vipInfo":"","nickName":"sGr968984411","userId":286365521,"score":5,"avatarurl":"http://p0.meituan.net/movie/__40465654__9539763.png","nick":"sGr968984411","approve":0,"oppose":0,"reply":0,"time":"2017-05-10 09:41","id":105713109,"content":"对得起刘德华三个字，影片里吴卓羲表现也很棒，我觉得和湄公河行动一样好看，影片最后爆炸时的情景，很揪心，总之很赞"},{"vipInfo":"","nickName":"Joeyi陳","userId":100642719,"score":5,"avatarurl":"http://p0.meituan.net/movie/__40465654__9539763.png","nick":"Joeyi陳","approve":0,"oppose":0,"reply":0,"time":"2017-05-10 09:41","id":105713103,"content":"每次看完港片的动作犯罪片心情久久不能平复，好片。"},{"vipInfo":"","nickName":"飘零随风","userId":9714867,"score":4,"avatarurl":"https://img.meituan.net/avatar/2cfba85feffe86ae9f351bf0f5099442148568.jpg","nick":"飘零随风","approve":0,"oppose":0,"reply":0,"time":"2017-05-10 09:41","id":105713088,"content":"故事平铺直叙，但是结尾有些突破常规，而是还是华仔主演，的确也是加分不少，总体来说还是不错。"}]},"hasNext":true,"movieid":346103,"offset":7,"limit":10}
     */

    public ControlBean control;
    public int status;
    public DataBean data;

    public static class ControlBean {
        /**
         * expires : 300
         */

        public int expires;
    }

    public static class DataBean {
        /**
         * CommentResponseModel : {"total":39148,"cmts":[{"vipInfo":"","nickName":"qqb134452376","userId":103400420,"score":4,"avatarurl":"https://img.meituan.net/avatar/1f9a369acef431e1df96c3f53789dc5f82232.jpg","nick":"qqb134452376","approve":0,"oppose":0,"reply":0,"time":"2017-05-10 09:47","id":105713441,"content":"剧情很精彩，如果有粤语版的就更好了"},{"vipInfo":"","nickName":"我怀念啊我的小镇","userId":85722268,"score":5,"avatarurl":"http://p0.meituan.net/movie/__40465654__9539763.png","nick":"我怀念啊我的小镇","approve":0,"oppose":0,"reply":0,"time":"2017-05-10 09:47","id":105713428,"content":"电影很好看。"},{"vipInfo":"","nickName":"kBV174966209","userId":742100219,"score":2,"avatarurl":"http://p0.meituan.net/movie/__40465654__9539763.png","nick":"kBV174966209","approve":0,"oppose":0,"reply":0,"time":"2017-05-10 09:47","id":105713403,"content":"就是去看看华仔"},{"vipInfo":"","nickName":"kcG338766370","userId":668318320,"score":5,"avatarurl":"http://p0.meituan.net/movie/__40465654__9539763.png","nick":"kcG338766370","approve":0,"oppose":0,"reply":0,"time":"2017-05-10 09:47","id":105713394,"content":"结局有点草率"},{"vipInfo":"","nickName":"aihongju","userId":140954934,"score":5,"avatarurl":"http://p0.meituan.net/movie/__40465654__9539763.png","nick":"aihongju","approve":0,"oppose":0,"reply":0,"time":"2017-05-10 09:46","id":105713333,"content":"小鲜肉警察死的时候说要远离人群车群的时候我哭了，这是我印象最深刻的片段"},{"vipInfo":"","nickName":"爱德华Tony2016","userId":392901085,"score":4.5,"avatarurl":"https://img.meituan.net/avatar/e760c1e3128838df09d89a8b337299b017536.jpg","nick":"爱德华Tony2016","approve":0,"oppose":0,"reply":0,"time":"2017-05-10 09:45","id":105713295,"content":"好久没有看到这么精彩的港产的警匪枪战片了，华仔的表演真的很精彩，很不错，很喜欢。"},{"vipInfo":"","nickName":"暁芳305287414","userId":62629472,"score":4.5,"avatarurl":"https://img.meituan.net/avatar/6d8a551e6d178a6d147f95a0f36eb87479838.jpg","nick":"暁芳305287414","approve":0,"oppose":0,"reply":0,"time":"2017-05-10 09:43","id":105713192,"content":"不愧是刘德华主演的，很有正能量"},{"vipInfo":"","nickName":"sGr968984411","userId":286365521,"score":5,"avatarurl":"http://p0.meituan.net/movie/__40465654__9539763.png","nick":"sGr968984411","approve":0,"oppose":0,"reply":0,"time":"2017-05-10 09:41","id":105713109,"content":"对得起刘德华三个字，影片里吴卓羲表现也很棒，我觉得和湄公河行动一样好看，影片最后爆炸时的情景，很揪心，总之很赞"},{"vipInfo":"","nickName":"Joeyi陳","userId":100642719,"score":5,"avatarurl":"http://p0.meituan.net/movie/__40465654__9539763.png","nick":"Joeyi陳","approve":0,"oppose":0,"reply":0,"time":"2017-05-10 09:41","id":105713103,"content":"每次看完港片的动作犯罪片心情久久不能平复，好片。"},{"vipInfo":"","nickName":"飘零随风","userId":9714867,"score":4,"avatarurl":"https://img.meituan.net/avatar/2cfba85feffe86ae9f351bf0f5099442148568.jpg","nick":"飘零随风","approve":0,"oppose":0,"reply":0,"time":"2017-05-10 09:41","id":105713088,"content":"故事平铺直叙，但是结尾有些突破常规，而是还是华仔主演，的确也是加分不少，总体来说还是不错。"}]}
         * hasNext : true
         * movieid : 346103
         * offset : 7
         * limit : 10
         */

        public CommentResponseModelBean CommentResponseModel;
        public boolean hasNext;
        public int movieid;
        public int offset;
        public int limit;

        public static class CommentResponseModelBean {
            /**
             * total : 39148
             * cmts : [{"vipInfo":"","nickName":"qqb134452376","userId":103400420,"score":4,"avatarurl":"https://img.meituan.net/avatar/1f9a369acef431e1df96c3f53789dc5f82232.jpg","nick":"qqb134452376","approve":0,"oppose":0,"reply":0,"time":"2017-05-10 09:47","id":105713441,"content":"剧情很精彩，如果有粤语版的就更好了"},{"vipInfo":"","nickName":"我怀念啊我的小镇","userId":85722268,"score":5,"avatarurl":"http://p0.meituan.net/movie/__40465654__9539763.png","nick":"我怀念啊我的小镇","approve":0,"oppose":0,"reply":0,"time":"2017-05-10 09:47","id":105713428,"content":"电影很好看。"},{"vipInfo":"","nickName":"kBV174966209","userId":742100219,"score":2,"avatarurl":"http://p0.meituan.net/movie/__40465654__9539763.png","nick":"kBV174966209","approve":0,"oppose":0,"reply":0,"time":"2017-05-10 09:47","id":105713403,"content":"就是去看看华仔"},{"vipInfo":"","nickName":"kcG338766370","userId":668318320,"score":5,"avatarurl":"http://p0.meituan.net/movie/__40465654__9539763.png","nick":"kcG338766370","approve":0,"oppose":0,"reply":0,"time":"2017-05-10 09:47","id":105713394,"content":"结局有点草率"},{"vipInfo":"","nickName":"aihongju","userId":140954934,"score":5,"avatarurl":"http://p0.meituan.net/movie/__40465654__9539763.png","nick":"aihongju","approve":0,"oppose":0,"reply":0,"time":"2017-05-10 09:46","id":105713333,"content":"小鲜肉警察死的时候说要远离人群车群的时候我哭了，这是我印象最深刻的片段"},{"vipInfo":"","nickName":"爱德华Tony2016","userId":392901085,"score":4.5,"avatarurl":"https://img.meituan.net/avatar/e760c1e3128838df09d89a8b337299b017536.jpg","nick":"爱德华Tony2016","approve":0,"oppose":0,"reply":0,"time":"2017-05-10 09:45","id":105713295,"content":"好久没有看到这么精彩的港产的警匪枪战片了，华仔的表演真的很精彩，很不错，很喜欢。"},{"vipInfo":"","nickName":"暁芳305287414","userId":62629472,"score":4.5,"avatarurl":"https://img.meituan.net/avatar/6d8a551e6d178a6d147f95a0f36eb87479838.jpg","nick":"暁芳305287414","approve":0,"oppose":0,"reply":0,"time":"2017-05-10 09:43","id":105713192,"content":"不愧是刘德华主演的，很有正能量"},{"vipInfo":"","nickName":"sGr968984411","userId":286365521,"score":5,"avatarurl":"http://p0.meituan.net/movie/__40465654__9539763.png","nick":"sGr968984411","approve":0,"oppose":0,"reply":0,"time":"2017-05-10 09:41","id":105713109,"content":"对得起刘德华三个字，影片里吴卓羲表现也很棒，我觉得和湄公河行动一样好看，影片最后爆炸时的情景，很揪心，总之很赞"},{"vipInfo":"","nickName":"Joeyi陳","userId":100642719,"score":5,"avatarurl":"http://p0.meituan.net/movie/__40465654__9539763.png","nick":"Joeyi陳","approve":0,"oppose":0,"reply":0,"time":"2017-05-10 09:41","id":105713103,"content":"每次看完港片的动作犯罪片心情久久不能平复，好片。"},{"vipInfo":"","nickName":"飘零随风","userId":9714867,"score":4,"avatarurl":"https://img.meituan.net/avatar/2cfba85feffe86ae9f351bf0f5099442148568.jpg","nick":"飘零随风","approve":0,"oppose":0,"reply":0,"time":"2017-05-10 09:41","id":105713088,"content":"故事平铺直叙，但是结尾有些突破常规，而是还是华仔主演，的确也是加分不少，总体来说还是不错。"}]
             */

            public int total;
            public List<CmtsBean> cmts;

            public static class CmtsBean {
                /**
                 * vipInfo :
                 * nickName : qqb134452376
                 * userId : 103400420
                 * score : 4
                 * avatarurl : https://img.meituan.net/avatar/1f9a369acef431e1df96c3f53789dc5f82232.jpg
                 * nick : qqb134452376
                 * approve : 0
                 * oppose : 0
                 * reply : 0
                 * time : 2017-05-10 09:47
                 * id : 105713441
                 * content : 剧情很精彩，如果有粤语版的就更好了
                 */

                public String vipInfo;
                public String nickName;
                public int userId;
                public int score;
                public String avatarurl;
                public String nick;
                public int approve;
                public int oppose;
                public int reply;
                public String time;
                public int id;
                public String content;
            }
        }
    }
}
