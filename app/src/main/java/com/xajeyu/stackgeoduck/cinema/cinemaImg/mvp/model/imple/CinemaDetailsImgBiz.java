package com.xajeyu.stackgeoduck.cinema.cinemaImg.mvp.model.imple;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.google.gson.Gson;
import com.xajeyu.stackgeoduck.cinema.bean.CinemaDetailsInfo;
import com.xajeyu.stackgeoduck.cinema.cinemaImg.mvp.model.CinemaDetailsImgListener;
import com.xajeyu.stackgeoduck.cinema.cinemaImg.mvp.model.ICinemaDetailsimgBiz;

/**
 * Created by e on 2017/5/18.
 */

public class CinemaDetailsImgBiz implements ICinemaDetailsimgBiz {
    private int mPostion;
    CinemaDetailsInfo mCinemaDetailsInfo;
    Context context;
    Activity activity;

    public CinemaDetailsImgBiz(Context context, Activity activity) {
        this.context = context;
        this.activity = activity;
    }

    @Override
    public void ReceiveData(CinemaDetailsImgListener cinemaDetailsImgListener) {
        //接收数据
        Intent intent = activity.getIntent();
        Bundle bundle = intent.getBundleExtra("bundle");
        if (bundle != null) {
            cinemaDetailsImgListener.Success("加载成功");
            //获取位置
            mPostion = bundle.getInt("postion");
            //获取json
            String json = intent.getStringExtra("url");
            //解析json
            mCinemaDetailsInfo = new Gson().fromJson(json, CinemaDetailsInfo.class);
            cinemaDetailsImgListener.showData(mCinemaDetailsInfo,mPostion);
        } else {
            cinemaDetailsImgListener.Success("加载失败");
        }
    }


}
