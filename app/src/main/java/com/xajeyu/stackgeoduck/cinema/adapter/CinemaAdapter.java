package com.xajeyu.stackgeoduck.cinema.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.xajeyu.sdk.imageloader.ImageLoaderManager;
import com.xajeyu.stackgeoduck.R;
import com.xajeyu.stackgeoduck.cinema.bean.CinemaInfo;
import com.xajeyu.stackgeoduck.cinema.ciemavideo.mvp.VideoActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by e on 2017/5/4.
 */

public class CinemaAdapter extends RecyclerView.Adapter {
    private Context mContext;
    private CinemaInfo mCinemaInfo;

    public CinemaAdapter(Context context, CinemaInfo cinemaInfo) {
        this.mContext = context;
        this.mCinemaInfo = cinemaInfo;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemViewHolder(View.inflate(mContext, R.layout.item_cinema, null));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
        ImageLoaderManager.getInstance(mContext).display(itemViewHolder.icon, mCinemaInfo.data.movies.get(position).img);
        itemViewHolder.name.setText(mCinemaInfo.data.movies.get(position).nm);
        itemViewHolder.scm.setText(mCinemaInfo.data.movies.get(position).scm);
        itemViewHolder.showInfo.setText(mCinemaInfo.data.movies.get(position).showInfo);
        itemViewHolder.scOrwish.setText(mCinemaInfo.data.movies.get(position).sc + "");
        itemViewHolder.Tickets.setText("购票");
        itemViewHolder.Tickets.setTextColor(Color.parseColor("#33AFF3"));
        itemViewHolder.Tickets.setBackgroundResource(R.drawable.tickets_btn_frame_color);
        //0.0修改成多少人想看
        if (mCinemaInfo.data.movies.get(position).sc == 0.0) {
            System.out.println("评分" + mCinemaInfo.data.movies.get(position).sc);
            itemViewHolder.score.setTextColor(Color.parseColor("#ecab5b"));
            itemViewHolder.scOrwish.setTextColor(Color.GRAY);
            itemViewHolder.score.setText((int) mCinemaInfo.data.movies.get(position).wish + "");
            itemViewHolder.scOrwish.setText("人想看");
            itemViewHolder.Tickets.setText("预售");
            itemViewHolder.Tickets.setTextColor(Color.parseColor("#F46451"));
            itemViewHolder.Tickets.setBackgroundResource(R.drawable.presale_btn_frame_color);
        }
        itemViewHolder.icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float id = mCinemaInfo.data.movies.get(position).id;
                System.out.println("id---" + id);
                Intent intent = new Intent(mContext, VideoActivity.class);
                intent.putExtra("ID", id);
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mCinemaInfo.data.movies.size();
    }


    static class ItemViewHolder extends RecyclerView.ViewHolder {
        /**
         * 电影图标
         */
        @BindView(R.id.cinema_icon)
        ImageView icon;
        /**
         * 电影名
         */
        @BindView(R.id.cinema_name)
        TextView name;
        /**
         * 如果已上映拿评分，未上映拿多少人想看
         * 电影评分或人想看
         */
        @BindView(R.id.cinema_sc_or_wish)
        TextView scOrwish;
        /**
         * 电影简介
         */
        @BindView(R.id.cinema_scm)
        TextView scm;
        /**
         * 上映影院数量
         */
        @BindView(R.id.cinema_showinfo)
        TextView showInfo;
        /**
         * 评分或想看人数
         */
        @BindView(R.id.cinema_score)
        TextView score;
        @BindView(R.id.tickets)
        Button Tickets;

        ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

