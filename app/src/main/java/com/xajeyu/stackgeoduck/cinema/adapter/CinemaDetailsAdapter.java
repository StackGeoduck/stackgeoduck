package com.xajeyu.stackgeoduck.cinema.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xajeyu.sdk.imageloader.ImageLoaderManager;
import com.xajeyu.stackgeoduck.R;
import com.xajeyu.stackgeoduck.cinema.bean.CinemaDetailsInfo;
import com.xajeyu.stackgeoduck.view.ratingbar.CustomRatingbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * 影院详情
 * Created by e on 2017/5/5.
 */

public class CinemaDetailsAdapter extends RecyclerView.Adapter {
    private Context mContext;
    private CinemaDetailsInfo mCinemaDetailsInfo;
    public CinemaDetailsAdapter(Context context, CinemaDetailsInfo cinemaDetailsInfo) {
        this.mContext = context;
        this.mCinemaDetailsInfo = cinemaDetailsInfo;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CommentViewHodler(View.inflate(mContext, R.layout.item_cinema_details, null));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        CommentViewHodler commentViewHodler = (CommentViewHodler) holder;
        ImageLoaderManager.getInstance(mContext).display(commentViewHodler.circleImageView, mCinemaDetailsInfo.data.CommentResponseModel.hcmts.get(position).avatarurl);
        commentViewHodler.userName.setText(mCinemaDetailsInfo.data.CommentResponseModel.hcmts.get(position).nickName);
        commentViewHodler.star.setRating((int) mCinemaDetailsInfo.data.CommentResponseModel.hcmts.get(position).score);
        commentViewHodler.comment.setText(mCinemaDetailsInfo.data.CommentResponseModel.hcmts.get(position).content);
        commentViewHodler.time.setText(mCinemaDetailsInfo.data.CommentResponseModel.hcmts.get(position).time);
    }

    @Override
    public int getItemCount() {
        return mCinemaDetailsInfo.data.CommentResponseModel.hcmts.size();
    }

    class CommentViewHodler extends RecyclerView.ViewHolder {
        /**
         * 用户头像
         */
        @BindView(R.id.cinema_details_commentary_avatar)
        CircleImageView circleImageView;
        /**
         * 用户名
         */
        @BindView(R.id.cinema_details_commentary_username)
        TextView userName;
        /**
         * 用户评分,星星
         */
        @BindView(R.id.cinema_details_commentary_star)
        CustomRatingbar star;
        /**
         * 评论内容
         */
        @BindView(R.id.cinema_details_commentary_comment)
        TextView comment;
        /**
         * 评论时间
         */
        @BindView(R.id.cinema_details_commentary_time)
        TextView time;
        public CommentViewHodler(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
