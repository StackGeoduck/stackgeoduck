package com.xajeyu.stackgeoduck.cinema.cinemaImg.mvp;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.widget.TextView;

import com.xajeyu.stackgeoduck.R;
import com.xajeyu.stackgeoduck.cinema.adapter.ViewPagerAdapter;
import com.xajeyu.stackgeoduck.cinema.bean.CinemaDetailsInfo;
import com.xajeyu.stackgeoduck.cinema.cinemaImg.mvp.presenter.CinemaDetailsImgPresenter;
import com.xajeyu.stackgeoduck.cinema.cinemaImg.mvp.view.CinemaDetailsImgViews;

/**
 * Created by e on 2017/5/9.
 */

public class ImgActivity extends Activity implements CinemaDetailsImgViews {

    /**
     * 当前图片页数
     */
    private TextView currentTv;
    /**
     * 图片总页数
     */
    private TextView sizeTv;
    private Context mContext = this;
    private ViewPager bigPictureViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_img);
        currentTv = (TextView) findViewById(R.id.cinema_details_media_current);
        sizeTv = (TextView) findViewById(R.id.cinema_details_media_size);
        bigPictureViewPager = (ViewPager) findViewById(R.id.cinema_details_media_bigpicture);
        CinemaDetailsImgPresenter cinemaDetailsImgPresenter = new CinemaDetailsImgPresenter(this, this, this);
        cinemaDetailsImgPresenter.receiveData();
    }

    @Override
    public void onSuccess(Object object) {

    }

    @Override
    public void onFailed(Object object) {

    }

    @Override
    public void showData(CinemaDetailsInfo cinemaDetailsInfo, int position) {
        setAdapter(cinemaDetailsInfo,position);
    }

    public void setAdapter(CinemaDetailsInfo cinemaDetailsInfo,int position) {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(this, cinemaDetailsInfo);
        //设置适配器
        bigPictureViewPager.setAdapter(viewPagerAdapter);
        //设置当前图片位置
        bigPictureViewPager.setCurrentItem(position);

        sizeTv.setText(cinemaDetailsInfo.data.MovieDetailModel.photos.size() + "");
        bigPictureViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                //让currentTv随着用户的翻页,翻到第几页而改动
                currentTv.setText(position + 1 + "/");
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }
}
