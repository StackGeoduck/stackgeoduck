package com.xajeyu.stackgeoduck.cinema.cinemadetails.mvp.model.imple;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.xajeyu.sdk.http.listener.RequestCallBack;
import com.xajeyu.stackgeoduck.cinema.bean.CinemaDetailsInfo;
import com.xajeyu.stackgeoduck.cinema.cinemadetails.mvp.model.CinemaDetailsListener;
import com.xajeyu.stackgeoduck.cinema.cinemadetails.mvp.model.ICinemaDetailsBiz;
import com.xajeyu.stackgeoduck.cinema.cinemadetails.mvp.view.CinemaDetailsViews;
import com.xajeyu.stackgeoduck.network.http.AppNetwork;


/**
 * Created by e on 2017/5/17.
 */

public class CinemaDetailsBiz implements ICinemaDetailsBiz {
    private Context context;
    private Activity activity;
    private CinemaDetailsViews cinemaDetailsViews;
    private boolean is;
    private int height;

    public CinemaDetailsBiz(Context context, Activity activity, CinemaDetailsViews cinemaDetailsViews) {
        this.context = context;
        this.activity = activity;
        this.cinemaDetailsViews = cinemaDetailsViews;

    }

    @Override
    public void RequestData(CinemaDetailsListener cinemaDetailsListener) {
        getData(cinemaDetailsListener);
    }

    public void getData(final CinemaDetailsListener cinemaDetailsListener) {
        Intent intent = activity.getIntent();
        String url = intent.getStringExtra("url");
        activity.setResult(20, intent.putExtra("vd", "123"));
        AppNetwork.getInstance().RequestCinemaDetails(url, new RequestCallBack() {
            @Override
            public void onSuccess(Object response) {
                CinemaDetailsInfo cinemaDetailsInfo = (CinemaDetailsInfo) response;
                cinemaDetailsListener.Success(response);
                cinemaDetailsListener.showData(cinemaDetailsInfo);

            }

            @Override
            public void onFail(Object response) {
                cinemaDetailsListener.Failed(response);
            }

            @Override
            public void onDownloadProgress(long progress) {

            }
        });
    }
}
