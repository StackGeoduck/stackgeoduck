package com.xajeyu.stackgeoduck.cinema.adapter;

import android.app.Activity;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import com.xajeyu.sdk.imageloader.ImageLoaderManager;
import com.xajeyu.stackgeoduck.cinema.bean.CinemaDetailsInfo;

import uk.co.senab.photoview.PhotoView;
import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * Created by e on 2017/5/18.
 */

public class ViewPagerAdapter extends PagerAdapter {
    CinemaDetailsInfo mCinemaDetailsInfo;
    Activity activity;
    public ViewPagerAdapter(Activity activity,CinemaDetailsInfo cinemaDetailsInfo) {
        mCinemaDetailsInfo = cinemaDetailsInfo;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return mCinemaDetailsInfo.data.MovieDetailModel.photos.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        PhotoView photoView = new PhotoView(activity);
        //点击事件
        photoView.setOnViewTapListener(new PhotoViewAttacher.OnViewTapListener() {
            @Override
            public void onViewTap(View view, float x, float y) {
                activity.finish();
            }
        });

        String phosos = mCinemaDetailsInfo.data.MovieDetailModel.photos.get(position);
        //把图片url的w.h/替换成空字符串
        phosos = phosos.replace("w.h/", "");
        //@100w_100h_1e_1c是小图,  .webp大图
        phosos = phosos.replace("@100w_100h_1e_1c", ".webp");
        ImageLoaderManager.getInstance(activity).display(photoView, phosos);
        container.addView(photoView);
        return photoView;
    }
}