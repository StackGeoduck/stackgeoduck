package com.xajeyu.stackgeoduck.cinema.ciemavideo.mvp.presenter;

import android.app.Activity;

import com.xajeyu.stackgeoduck.cinema.bean.CinemaDetailsInfo;
import com.xajeyu.stackgeoduck.cinema.ciemavideo.mvp.model.CinemaVideoListener;
import com.xajeyu.stackgeoduck.cinema.ciemavideo.mvp.model.imple.CinemaVideoBiz;
import com.xajeyu.stackgeoduck.cinema.ciemavideo.mvp.view.CinemaVideoViews;


/**
 * Created by e on 2017/5/18.
 */

public class CinemaVideoPresenter {
    CinemaVideoViews mCinemaVideoViews;
    CinemaVideoBiz cinemaVideoBiz;
    Activity activity ;
    public CinemaVideoPresenter(Activity activity,CinemaVideoViews cinemaVideoViews) {
        this.activity = activity;
        mCinemaVideoViews = cinemaVideoViews;
        cinemaVideoBiz = new CinemaVideoBiz();

    }

    public void RequestData() {
        cinemaVideoBiz.requestData(activity,new CinemaVideoListener() {
            @Override
            public void Success(Object object) {
                mCinemaVideoViews.onSuccess(object);
            }

            @Override
            public void Failed(Object object) {
                mCinemaVideoViews.onFailed(object);
            }

            @Override
            public void showData(CinemaDetailsInfo cinemaDetailsInfo) {
                mCinemaVideoViews.showData(cinemaDetailsInfo);
            }

        });
    }
}
