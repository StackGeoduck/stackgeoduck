package com.xajeyu.stackgeoduck.cinema.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by e on 2017/5/5.
 */

public class CinemaDetailsInfo implements Serializable{


    /**
     * control : {"expires":3600}
     * status : 0
     * data : {"MovieDetailModel":{"cat":"动作,悬疑,犯罪","dealsum":0,"dir":"邱礼涛 ","dra":"章在山（刘德华 饰）是香港警队爆炸品处理科的一名高级督察七年前他潜伏到头号通缉犯火爆（姜武 饰）的犯罪团伙中，在一次打劫金库的行动中，章在山表露了其拆弹组卧底的身份，与警方里应外合，成功阻止炸弹引爆，并将火爆及其弟的犯罪组织一网打尽，可惜在千钧一发之间，火爆逃脱并扬言誓要报仇。复职后的章在山很快被晋升为警队的拆弹专家。七年后，香港接二连三遭遇炸弹恐怖袭击，警方更收到线报大批爆炸品已偷运入港，一切迹象显示香港将有大案发生。就在香港人心惶惶之际，城中最繁忙的红磡海底隧道被悍匪围堵拦截，数百名人质被胁持，终于现身的火爆威胁警方炸毁隧道。章在山唯有将火爆绳之于法，才能拆解这场反恐风暴背后的惊天阴谋\u2026\u2026","dur":120,"id":346103,"imax":false,"img":"http://p1.meituan.net/165.220/movie/cc50791238502ae1fa08df764c5f5c97223987.jpg","isShowing":true,"late":false,"mk":0,"nm":"拆弹·专家","photos":["http://p0.meituan.net/w.h/movie/611ece99e56c91eecfaa136ced1d85b890443.jpg@100w_100h_1e_1c","http://p0.meituan.net/w.h/movie/afee7d049bd3b06e56ae714483d04d2287560.jpg@100w_100h_1e_1c","http://p1.meituan.net/w.h/movie/d1ade374c7feee659581311844f1953d90949.jpg@100w_100h_1e_1c","http://p1.meituan.net/w.h/movie/eec36492e7ca1d078535782d5fb04498164473.jpg@100w_100h_1e_1c","http://p0.meituan.net/w.h/movie/6be700f4b7f22b1e305c7a1a819396ea86254.jpg@100w_100h_1e_1c","http://p1.meituan.net/w.h/movie/a7f8cd743494c08bcb931deb591668fa100780.jpg@100w_100h_1e_1c","http://p0.meituan.net/w.h/movie/d8e6dd2d2017e5aaa38d53ec086ffa67101682.jpg@100w_100h_1e_1c","http://p0.meituan.net/w.h/movie/44f185355d48ace33007eb74963b6b2387237.jpg@100w_100h_1e_1c","http://p1.meituan.net/w.h/movie/012636314c4f3727999247d12774720b78038.jpg@100w_100h_1e_1c","http://p0.meituan.net/w.h/movie/666152967bc414f9b721eb58b404311d83363.jpg@100w_100h_1e_1c","http://p1.meituan.net/w.h/movie/d15cac0586bc39f8eb2205e4c7cea965161108.jpg@100w_100h_1e_1c","http://p0.meituan.net/w.h/movie/39754960068d5e0a2670fe60392b4ef661713.jpg@100w_100h_1e_1c","http://p0.meituan.net/w.h/movie/698649938e700e1f671a9f5d938778b869302.jpg@100w_100h_1e_1c","http://p0.meituan.net/w.h/movie/40117c495aeb24b0edf05691ab1ca03d88538.jpg@100w_100h_1e_1c","http://p1.meituan.net/w.h/movie/c1995c9423f8af110ed01a2aadc6a1cb98397.jpg@100w_100h_1e_1c","http://p1.meituan.net/w.h/movie/1287d9f762e3f47883da6577fab36d0d90854.jpg@100w_100h_1e_1c","http://p1.meituan.net/w.h/movie/80469143be9cf39a721f24b4dd4ce9ff57253.jpg@100w_100h_1e_1c","http://p0.meituan.net/w.h/movie/6fc5e2e0ad20567d3913ad14e6b3059377812.jpg@100w_100h_1e_1c","http://p0.meituan.net/w.h/movie/698e14866e0402648eb55ff53ee5c8b5118269.jpg@100w_100h_1e_1c","http://p1.meituan.net/w.h/movie/1993413ee48dc0ab79536e8b9cdc764a95293.jpg@100w_100h_1e_1c"],"pn":167,"preSale":0,"rt":"2017-04-28上映","sc":9.1,"scm":"爆炸袭击案，拆弹反恐难","showSnum":true,"snum":116008,"src":"中国香港","star":"刘德华 姜武 宋佳 姜皓文 吴卓羲 王紫逸 黄日华 石修 廖启智 张竣杰 张继聪 蔡瀚亿 李国麟 骆应钧 尹扬明 何华超 卢惠光 陈彼得 林迪安 ","vd":"http://maoyan.meituan.net/movie/videos/854x480b259081817f14721b6fc40ad32402581.mp4","ver":"2D/中国巨幕","vnum":22,"wish":49007,"wishst":0},"CommentResponseModel":{"cmts":[{"userId":31589876,"score":5,"vipInfo":"","nickName":"KpOnLG","time":"2017-05-09 20:26","approve":0,"avatarurl":"http://p0.meituan.net/movie/__40465654__9539763.png","nick":"KpOnLG","oppose":0,"reply":0,"id":105682067,"content":"真的很棒！作为一名大陆人民警察，看到了很多很真实的画面，感同身受"},{"userId":84727322,"score":4,"vipInfo":"","nickName":"Nyz353655865","time":"2017-05-09 20:25","approve":0,"avatarurl":"https://img.meituan.net/avatar/76a7fbe21519ed3ac00df81a4763f4f683689.jpg","nick":"Nyz353655865","oppose":0,"reply":0,"id":105682048,"content":"很好看 非常的好看"},{"userId":224411314,"score":5,"vipInfo":"","nickName":"crd594752025","time":"2017-05-09 20:25","approve":0,"avatarurl":"http://p0.meituan.net/movie/__40465654__9539763.png","nick":"crd594752025","oppose":0,"reply":0,"id":105682046,"content":"完美！赞赞赞👍"},{"userId":133514988,"score":4.5,"vipInfo":"","nickName":"读懂你7993","time":"2017-05-09 20:24","approve":0,"avatarurl":"http://p0.meituan.net/movie/__40465654__9539763.png","nick":"读懂你7993","oppose":0,"reply":0,"id":105681914,"content":"满满的正义感，拍的剧情觉得很真实，看着都感动得掉眼泪，非常值得看。"},{"userId":268838196,"score":3.5,"vipInfo":"","nickName":"jxZ644207910","time":"2017-05-09 20:24","approve":0,"avatarurl":"http://p0.meituan.net/movie/__40465654__9539763.png","nick":"jxZ644207910","oppose":0,"reply":0,"id":105681891,"content":"影片有太多的刻意画面，没有想象中精彩。"}],"hcmts":[{"userId":41640225,"score":5,"vipInfo":"","nickName":"Patrick徐焱","time":"2017-04-28 02:07","approve":686,"avatarurl":"https://img.meituan.net/avatar/4388e595d0742799abb29490ee6c59d8108620.jpg","nick":"悲伤的Patrick","oppose":0,"reply":57,"id":104236005,"content":"谁说香港的警匪片没落了......拉出去斩首...我们这场的观众没有说不好看的...我拉肚子都特么忍了1个小时..."},{"userId":230551170,"score":5,"vipInfo":"","nickName":"T1ng.33","time":"2017-04-28 02:29","approve":275,"avatarurl":"https://img.meituan.net/avatar/0bf2bf73854e2299a7739849475f3438136295.jpg","nick":"T1ng.33","oppose":0,"reply":25,"id":104236866,"content":"可以 可以,我最后统计了一下,刘德华一共死了21次,加上这部片子,刘德华已经死了22次了.真是难为刘德华了"},{"userId":317582276,"score":4.5,"vipInfo":"","nickName":"aeN404311775","time":"2017-04-28 02:35","approve":178,"avatarurl":"http://p0.meituan.net/movie/__40465654__9539763.png","nick":"aeN404311775","oppose":0,"reply":30,"id":104236996,"content":"我觉得华仔的助手在最后听了华仔的话后就要自己大义泯然的剪下去！"},{"userId":176671911,"score":4.5,"vipInfo":"","nickName":"修罗领域","time":"2017-04-29 00:39","approve":70,"avatarurl":"http://p0.meituan.net/movie/__40465654__9539763.png","nick":"Fpp599533337","oppose":0,"reply":9,"id":104362034,"content":"我是警察，我有责任！有多少人可以做到？两个男人的对吼，从自信到无奈，从迷茫畏惧到坚定不移。做一个有责任感的警察不容易，但是做一个有责任感的警察的家人更不容易，因为责任可能凌驾于家庭之上。面临生死决择勇往直前，只为心中的执着，肩上的责任。这电影真的很好看！因为是粤语，所以刘德华的表情和语气都很好，如果是国语的话，可能会有一点😄...贼弟，让我见识到了什么是真正的躺枪，完美！还有就是反应了香港政府对人性的坚持，机会=希望，给予机会，需要多大的勇气和代价！虽然都支持坏蛋都必须死，但是可以昄依佛门或者耶稣还是很不错的，教化改过自身比结束生命的意义和难度都大得多。我说了...结尾会有\u201c但对不起！\u201d吗？"},{"userId":328743555,"score":0.5,"vipInfo":"","nickName":"ndB562331408","time":"2017-04-28 02:27","approve":67,"avatarurl":"http://p0.meituan.net/movie/__40465654__9539763.png","nick":"ndB562331408","oppose":0,"reply":58,"id":104236808,"content":"真他吗的烂，很多细节多此一举"},{"userId":162452474,"score":5,"vipInfo":"","nickName":"怪D与怪花","time":"2017-04-28 15:06","approve":50,"avatarurl":"https://img.meituan.net/avatar/a5384fc9b7eb3488270ad3d7cce6376e62677.jpg","nick":"最爱Andy华","oppose":0,"reply":2,"id":104275591,"content":"节奏紧凑，人物刻画的也比较饱满。能看得出主创人员的野心，不仅仅是一个警匪片动作片，更像是一个灾难片。在一个隧道里，一个封闭的环境下，面对困境，我们该怎么选择？一个人重要还是一百个人重要？这是一个值得我们思考的问题。很棒！值得带家人和朋友一起去看。不过，小心炸弹💣，带好纸巾！"},{"userId":309209582,"score":5,"vipInfo":"","nickName":"阿祖大帅","time":"2017-04-29 17:03","approve":39,"avatarurl":"https://img.meituan.net/avatar/41227cf495c6a95bec3393206ede8e4348953.jpg","nick":"阿祖大帅","oppose":0,"reply":3,"id":104437285,"content":"香港电影拍的越来越有档次了 这几年的片子不断升华 敬畏生命 华仔再一次提升了他在我眼中的地位 导演很有思想 多拍好电影 情节给满分 细节可以再向外国电影多学习 给10分不怕你骄傲！"},{"userId":281342329,"score":5,"vipInfo":"","nickName":"浪子高达","time":"2017-04-30 07:04","approve":39,"avatarurl":"https://img.meituan.net/avatar/2bea0f38679f61e4d667250a67832052167433.jpg","nick":"YIj713359829","oppose":0,"reply":1,"id":104519902,"content":"虽然好贵，不过值回票价。刘德华作为香港形象大使，亲自监制，投资，演出一部香港警匪电影而且效果十分好。感谢你付出。电影评分10分，警匪电影应该有枪战，飞车都有。值得一赞，一部警匪电影可以将感情戏表现得甘细腻绝对一流，剧情将香港警察形象提升了。这部电影感动了我不小于5次，感觉距完胜了寒战了。感谢上帝可以比我用生命保护生命。这是一部会流泪的警匪电影"},{"userId":279470824,"score":5,"vipInfo":"","nickName":"zdm20081221","time":"2017-05-02 01:40","approve":17,"avatarurl":"https://img.meituan.net/avatar/f0d3ac5b4613b076109d0f6320029323100481.jpg","nick":"zdm20081221","oppose":0,"reply":2,"id":104824064,"content":"英雄没能救人还是英雄吗？英雄没能自救还是英雄吗？当然是，还是很感人的英雄。不仅刘德华是英雄，不肯离开旅游团的导游也是英雄，被绑上炸弹吓到不行依旧努力控制自己的小警察也是英雄。很感人的一部关于英雄的电影，如果能把刘德华女朋友换成杨采妮就好了，港产片硬插几个内地演员，诡异违和。"},{"userId":65712521,"score":5,"vipInfo":"","nickName":"ALiliyu","time":"2017-05-09 11:55","approve":1,"avatarurl":"http://p0.meituan.net/movie/__40465654__9539763.png","nick":"ALiliyu","oppose":0,"reply":0,"id":105637830,"content":"全程在紧张度过，一节扣一节，好久没有看到刘影帝风范了，还是宝刀未老，其实好希望结局可以扭转了，让人伤心了一把"}],"total":38637,"hasNext":true}}
     */

    public ControlBean control;
    public float status;
    public DataBean data;

    public static class ControlBean {
        /**
         * expires : 3600
         */

        public float expires;
    }

    public static class DataBean {
        /**
         * MovieDetailModel : {"cat":"动作,悬疑,犯罪","dealsum":0,"dir":"邱礼涛 ","dra":"章在山（刘德华 饰）是香港警队爆炸品处理科的一名高级督察七年前他潜伏到头号通缉犯火爆（姜武 饰）的犯罪团伙中，在一次打劫金库的行动中，章在山表露了其拆弹组卧底的身份，与警方里应外合，成功阻止炸弹引爆，并将火爆及其弟的犯罪组织一网打尽，可惜在千钧一发之间，火爆逃脱并扬言誓要报仇。复职后的章在山很快被晋升为警队的拆弹专家。七年后，香港接二连三遭遇炸弹恐怖袭击，警方更收到线报大批爆炸品已偷运入港，一切迹象显示香港将有大案发生。就在香港人心惶惶之际，城中最繁忙的红磡海底隧道被悍匪围堵拦截，数百名人质被胁持，终于现身的火爆威胁警方炸毁隧道。章在山唯有将火爆绳之于法，才能拆解这场反恐风暴背后的惊天阴谋\u2026\u2026","dur":120,"id":346103,"imax":false,"img":"http://p1.meituan.net/165.220/movie/cc50791238502ae1fa08df764c5f5c97223987.jpg","isShowing":true,"late":false,"mk":0,"nm":"拆弹·专家","photos":["http://p0.meituan.net/w.h/movie/611ece99e56c91eecfaa136ced1d85b890443.jpg@100w_100h_1e_1c","http://p0.meituan.net/w.h/movie/afee7d049bd3b06e56ae714483d04d2287560.jpg@100w_100h_1e_1c","http://p1.meituan.net/w.h/movie/d1ade374c7feee659581311844f1953d90949.jpg@100w_100h_1e_1c","http://p1.meituan.net/w.h/movie/eec36492e7ca1d078535782d5fb04498164473.jpg@100w_100h_1e_1c","http://p0.meituan.net/w.h/movie/6be700f4b7f22b1e305c7a1a819396ea86254.jpg@100w_100h_1e_1c","http://p1.meituan.net/w.h/movie/a7f8cd743494c08bcb931deb591668fa100780.jpg@100w_100h_1e_1c","http://p0.meituan.net/w.h/movie/d8e6dd2d2017e5aaa38d53ec086ffa67101682.jpg@100w_100h_1e_1c","http://p0.meituan.net/w.h/movie/44f185355d48ace33007eb74963b6b2387237.jpg@100w_100h_1e_1c","http://p1.meituan.net/w.h/movie/012636314c4f3727999247d12774720b78038.jpg@100w_100h_1e_1c","http://p0.meituan.net/w.h/movie/666152967bc414f9b721eb58b404311d83363.jpg@100w_100h_1e_1c","http://p1.meituan.net/w.h/movie/d15cac0586bc39f8eb2205e4c7cea965161108.jpg@100w_100h_1e_1c","http://p0.meituan.net/w.h/movie/39754960068d5e0a2670fe60392b4ef661713.jpg@100w_100h_1e_1c","http://p0.meituan.net/w.h/movie/698649938e700e1f671a9f5d938778b869302.jpg@100w_100h_1e_1c","http://p0.meituan.net/w.h/movie/40117c495aeb24b0edf05691ab1ca03d88538.jpg@100w_100h_1e_1c","http://p1.meituan.net/w.h/movie/c1995c9423f8af110ed01a2aadc6a1cb98397.jpg@100w_100h_1e_1c","http://p1.meituan.net/w.h/movie/1287d9f762e3f47883da6577fab36d0d90854.jpg@100w_100h_1e_1c","http://p1.meituan.net/w.h/movie/80469143be9cf39a721f24b4dd4ce9ff57253.jpg@100w_100h_1e_1c","http://p0.meituan.net/w.h/movie/6fc5e2e0ad20567d3913ad14e6b3059377812.jpg@100w_100h_1e_1c","http://p0.meituan.net/w.h/movie/698e14866e0402648eb55ff53ee5c8b5118269.jpg@100w_100h_1e_1c","http://p1.meituan.net/w.h/movie/1993413ee48dc0ab79536e8b9cdc764a95293.jpg@100w_100h_1e_1c"],"pn":167,"preSale":0,"rt":"2017-04-28上映","sc":9.1,"scm":"爆炸袭击案，拆弹反恐难","showSnum":true,"snum":116008,"src":"中国香港","star":"刘德华 姜武 宋佳 姜皓文 吴卓羲 王紫逸 黄日华 石修 廖启智 张竣杰 张继聪 蔡瀚亿 李国麟 骆应钧 尹扬明 何华超 卢惠光 陈彼得 林迪安 ","vd":"http://maoyan.meituan.net/movie/videos/854x480b259081817f14721b6fc40ad32402581.mp4","ver":"2D/中国巨幕","vnum":22,"wish":49007,"wishst":0}
         * CommentResponseModel : {"cmts":[{"userId":31589876,"score":5,"vipInfo":"","nickName":"KpOnLG","time":"2017-05-09 20:26","approve":0,"avatarurl":"http://p0.meituan.net/movie/__40465654__9539763.png","nick":"KpOnLG","oppose":0,"reply":0,"id":105682067,"content":"真的很棒！作为一名大陆人民警察，看到了很多很真实的画面，感同身受"},{"userId":84727322,"score":4,"vipInfo":"","nickName":"Nyz353655865","time":"2017-05-09 20:25","approve":0,"avatarurl":"https://img.meituan.net/avatar/76a7fbe21519ed3ac00df81a4763f4f683689.jpg","nick":"Nyz353655865","oppose":0,"reply":0,"id":105682048,"content":"很好看 非常的好看"},{"userId":224411314,"score":5,"vipInfo":"","nickName":"crd594752025","time":"2017-05-09 20:25","approve":0,"avatarurl":"http://p0.meituan.net/movie/__40465654__9539763.png","nick":"crd594752025","oppose":0,"reply":0,"id":105682046,"content":"完美！赞赞赞👍"},{"userId":133514988,"score":4.5,"vipInfo":"","nickName":"读懂你7993","time":"2017-05-09 20:24","approve":0,"avatarurl":"http://p0.meituan.net/movie/__40465654__9539763.png","nick":"读懂你7993","oppose":0,"reply":0,"id":105681914,"content":"满满的正义感，拍的剧情觉得很真实，看着都感动得掉眼泪，非常值得看。"},{"userId":268838196,"score":3.5,"vipInfo":"","nickName":"jxZ644207910","time":"2017-05-09 20:24","approve":0,"avatarurl":"http://p0.meituan.net/movie/__40465654__9539763.png","nick":"jxZ644207910","oppose":0,"reply":0,"id":105681891,"content":"影片有太多的刻意画面，没有想象中精彩。"}],"hcmts":[{"userId":41640225,"score":5,"vipInfo":"","nickName":"Patrick徐焱","time":"2017-04-28 02:07","approve":686,"avatarurl":"https://img.meituan.net/avatar/4388e595d0742799abb29490ee6c59d8108620.jpg","nick":"悲伤的Patrick","oppose":0,"reply":57,"id":104236005,"content":"谁说香港的警匪片没落了......拉出去斩首...我们这场的观众没有说不好看的...我拉肚子都特么忍了1个小时..."},{"userId":230551170,"score":5,"vipInfo":"","nickName":"T1ng.33","time":"2017-04-28 02:29","approve":275,"avatarurl":"https://img.meituan.net/avatar/0bf2bf73854e2299a7739849475f3438136295.jpg","nick":"T1ng.33","oppose":0,"reply":25,"id":104236866,"content":"可以 可以,我最后统计了一下,刘德华一共死了21次,加上这部片子,刘德华已经死了22次了.真是难为刘德华了"},{"userId":317582276,"score":4.5,"vipInfo":"","nickName":"aeN404311775","time":"2017-04-28 02:35","approve":178,"avatarurl":"http://p0.meituan.net/movie/__40465654__9539763.png","nick":"aeN404311775","oppose":0,"reply":30,"id":104236996,"content":"我觉得华仔的助手在最后听了华仔的话后就要自己大义泯然的剪下去！"},{"userId":176671911,"score":4.5,"vipInfo":"","nickName":"修罗领域","time":"2017-04-29 00:39","approve":70,"avatarurl":"http://p0.meituan.net/movie/__40465654__9539763.png","nick":"Fpp599533337","oppose":0,"reply":9,"id":104362034,"content":"我是警察，我有责任！有多少人可以做到？两个男人的对吼，从自信到无奈，从迷茫畏惧到坚定不移。做一个有责任感的警察不容易，但是做一个有责任感的警察的家人更不容易，因为责任可能凌驾于家庭之上。面临生死决择勇往直前，只为心中的执着，肩上的责任。这电影真的很好看！因为是粤语，所以刘德华的表情和语气都很好，如果是国语的话，可能会有一点😄...贼弟，让我见识到了什么是真正的躺枪，完美！还有就是反应了香港政府对人性的坚持，机会=希望，给予机会，需要多大的勇气和代价！虽然都支持坏蛋都必须死，但是可以昄依佛门或者耶稣还是很不错的，教化改过自身比结束生命的意义和难度都大得多。我说了...结尾会有\u201c但对不起！\u201d吗？"},{"userId":328743555,"score":0.5,"vipInfo":"","nickName":"ndB562331408","time":"2017-04-28 02:27","approve":67,"avatarurl":"http://p0.meituan.net/movie/__40465654__9539763.png","nick":"ndB562331408","oppose":0,"reply":58,"id":104236808,"content":"真他吗的烂，很多细节多此一举"},{"userId":162452474,"score":5,"vipInfo":"","nickName":"怪D与怪花","time":"2017-04-28 15:06","approve":50,"avatarurl":"https://img.meituan.net/avatar/a5384fc9b7eb3488270ad3d7cce6376e62677.jpg","nick":"最爱Andy华","oppose":0,"reply":2,"id":104275591,"content":"节奏紧凑，人物刻画的也比较饱满。能看得出主创人员的野心，不仅仅是一个警匪片动作片，更像是一个灾难片。在一个隧道里，一个封闭的环境下，面对困境，我们该怎么选择？一个人重要还是一百个人重要？这是一个值得我们思考的问题。很棒！值得带家人和朋友一起去看。不过，小心炸弹💣，带好纸巾！"},{"userId":309209582,"score":5,"vipInfo":"","nickName":"阿祖大帅","time":"2017-04-29 17:03","approve":39,"avatarurl":"https://img.meituan.net/avatar/41227cf495c6a95bec3393206ede8e4348953.jpg","nick":"阿祖大帅","oppose":0,"reply":3,"id":104437285,"content":"香港电影拍的越来越有档次了 这几年的片子不断升华 敬畏生命 华仔再一次提升了他在我眼中的地位 导演很有思想 多拍好电影 情节给满分 细节可以再向外国电影多学习 给10分不怕你骄傲！"},{"userId":281342329,"score":5,"vipInfo":"","nickName":"浪子高达","time":"2017-04-30 07:04","approve":39,"avatarurl":"https://img.meituan.net/avatar/2bea0f38679f61e4d667250a67832052167433.jpg","nick":"YIj713359829","oppose":0,"reply":1,"id":104519902,"content":"虽然好贵，不过值回票价。刘德华作为香港形象大使，亲自监制，投资，演出一部香港警匪电影而且效果十分好。感谢你付出。电影评分10分，警匪电影应该有枪战，飞车都有。值得一赞，一部警匪电影可以将感情戏表现得甘细腻绝对一流，剧情将香港警察形象提升了。这部电影感动了我不小于5次，感觉距完胜了寒战了。感谢上帝可以比我用生命保护生命。这是一部会流泪的警匪电影"},{"userId":279470824,"score":5,"vipInfo":"","nickName":"zdm20081221","time":"2017-05-02 01:40","approve":17,"avatarurl":"https://img.meituan.net/avatar/f0d3ac5b4613b076109d0f6320029323100481.jpg","nick":"zdm20081221","oppose":0,"reply":2,"id":104824064,"content":"英雄没能救人还是英雄吗？英雄没能自救还是英雄吗？当然是，还是很感人的英雄。不仅刘德华是英雄，不肯离开旅游团的导游也是英雄，被绑上炸弹吓到不行依旧努力控制自己的小警察也是英雄。很感人的一部关于英雄的电影，如果能把刘德华女朋友换成杨采妮就好了，港产片硬插几个内地演员，诡异违和。"},{"userId":65712521,"score":5,"vipInfo":"","nickName":"ALiliyu","time":"2017-05-09 11:55","approve":1,"avatarurl":"http://p0.meituan.net/movie/__40465654__9539763.png","nick":"ALiliyu","oppose":0,"reply":0,"id":105637830,"content":"全程在紧张度过，一节扣一节，好久没有看到刘影帝风范了，还是宝刀未老，其实好希望结局可以扭转了，让人伤心了一把"}],"total":38637,"hasNext":true}
         */

        public MovieDetailModelBean MovieDetailModel;
        public CommentResponseModelBean CommentResponseModel;

        public static class MovieDetailModelBean {
            /**
             * cat : 动作,悬疑,犯罪
             * dealsum : 0
             * dir : 邱礼涛
             * dra : 章在山（刘德华 饰）是香港警队爆炸品处理科的一名高级督察七年前他潜伏到头号通缉犯火爆（姜武 饰）的犯罪团伙中，在一次打劫金库的行动中，章在山表露了其拆弹组卧底的身份，与警方里应外合，成功阻止炸弹引爆，并将火爆及其弟的犯罪组织一网打尽，可惜在千钧一发之间，火爆逃脱并扬言誓要报仇。复职后的章在山很快被晋升为警队的拆弹专家。七年后，香港接二连三遭遇炸弹恐怖袭击，警方更收到线报大批爆炸品已偷运入港，一切迹象显示香港将有大案发生。就在香港人心惶惶之际，城中最繁忙的红磡海底隧道被悍匪围堵拦截，数百名人质被胁持，终于现身的火爆威胁警方炸毁隧道。章在山唯有将火爆绳之于法，才能拆解这场反恐风暴背后的惊天阴谋……
             * dur : 120
             * id : 346103
             * imax : false
             * img : http://p1.meituan.net/165.220/movie/cc50791238502ae1fa08df764c5f5c97223987.jpg
             * isShowing : true
             * late : false
             * mk : 0
             * nm : 拆弹·专家
             * photos : ["http://p0.meituan.net/w.h/movie/611ece99e56c91eecfaa136ced1d85b890443.jpg@100w_100h_1e_1c","http://p0.meituan.net/w.h/movie/afee7d049bd3b06e56ae714483d04d2287560.jpg@100w_100h_1e_1c","http://p1.meituan.net/w.h/movie/d1ade374c7feee659581311844f1953d90949.jpg@100w_100h_1e_1c","http://p1.meituan.net/w.h/movie/eec36492e7ca1d078535782d5fb04498164473.jpg@100w_100h_1e_1c","http://p0.meituan.net/w.h/movie/6be700f4b7f22b1e305c7a1a819396ea86254.jpg@100w_100h_1e_1c","http://p1.meituan.net/w.h/movie/a7f8cd743494c08bcb931deb591668fa100780.jpg@100w_100h_1e_1c","http://p0.meituan.net/w.h/movie/d8e6dd2d2017e5aaa38d53ec086ffa67101682.jpg@100w_100h_1e_1c","http://p0.meituan.net/w.h/movie/44f185355d48ace33007eb74963b6b2387237.jpg@100w_100h_1e_1c","http://p1.meituan.net/w.h/movie/012636314c4f3727999247d12774720b78038.jpg@100w_100h_1e_1c","http://p0.meituan.net/w.h/movie/666152967bc414f9b721eb58b404311d83363.jpg@100w_100h_1e_1c","http://p1.meituan.net/w.h/movie/d15cac0586bc39f8eb2205e4c7cea965161108.jpg@100w_100h_1e_1c","http://p0.meituan.net/w.h/movie/39754960068d5e0a2670fe60392b4ef661713.jpg@100w_100h_1e_1c","http://p0.meituan.net/w.h/movie/698649938e700e1f671a9f5d938778b869302.jpg@100w_100h_1e_1c","http://p0.meituan.net/w.h/movie/40117c495aeb24b0edf05691ab1ca03d88538.jpg@100w_100h_1e_1c","http://p1.meituan.net/w.h/movie/c1995c9423f8af110ed01a2aadc6a1cb98397.jpg@100w_100h_1e_1c","http://p1.meituan.net/w.h/movie/1287d9f762e3f47883da6577fab36d0d90854.jpg@100w_100h_1e_1c","http://p1.meituan.net/w.h/movie/80469143be9cf39a721f24b4dd4ce9ff57253.jpg@100w_100h_1e_1c","http://p0.meituan.net/w.h/movie/6fc5e2e0ad20567d3913ad14e6b3059377812.jpg@100w_100h_1e_1c","http://p0.meituan.net/w.h/movie/698e14866e0402648eb55ff53ee5c8b5118269.jpg@100w_100h_1e_1c","http://p1.meituan.net/w.h/movie/1993413ee48dc0ab79536e8b9cdc764a95293.jpg@100w_100h_1e_1c"]
             * pn : 167
             * preSale : 0
             * rt : 2017-04-28上映
             * sc : 9.1
             * scm : 爆炸袭击案，拆弹反恐难
             * showSnum : true
             * snum : 116008
             * src : 中国香港
             * star : 刘德华 姜武 宋佳 姜皓文 吴卓羲 王紫逸 黄日华 石修 廖启智 张竣杰 张继聪 蔡瀚亿 李国麟 骆应钧 尹扬明 何华超 卢惠光 陈彼得 林迪安
             * vd : http://maoyan.meituan.net/movie/videos/854x480b259081817f14721b6fc40ad32402581.mp4
             * ver : 2D/中国巨幕
             * vnum : 22
             * wish : 49007
             * wishst : 0
             */

            public String cat;
            public float dealsum;
            public String dir;
            public String dra;
            public float dur;
            public float id;
            public boolean imax;
            public String img;
            public boolean isShowing;
            public boolean late;
            public float mk;
            public String nm;
            public float pn;
            public float preSale;
            public String rt;
            public double sc;
            public String scm;
            public boolean showSnum;
            public float snum;
            public String src;
            public String star;
            public String vd;
            public String ver;
            public float vnum;
            public float wish;
            public float wishst;
            public List<String> photos;
        }

        public static class CommentResponseModelBean {
            /**
             * cmts : [{"userId":31589876,"score":5,"vipInfo":"","nickName":"KpOnLG","time":"2017-05-09 20:26","approve":0,"avatarurl":"http://p0.meituan.net/movie/__40465654__9539763.png","nick":"KpOnLG","oppose":0,"reply":0,"id":105682067,"content":"真的很棒！作为一名大陆人民警察，看到了很多很真实的画面，感同身受"},{"userId":84727322,"score":4,"vipInfo":"","nickName":"Nyz353655865","time":"2017-05-09 20:25","approve":0,"avatarurl":"https://img.meituan.net/avatar/76a7fbe21519ed3ac00df81a4763f4f683689.jpg","nick":"Nyz353655865","oppose":0,"reply":0,"id":105682048,"content":"很好看 非常的好看"},{"userId":224411314,"score":5,"vipInfo":"","nickName":"crd594752025","time":"2017-05-09 20:25","approve":0,"avatarurl":"http://p0.meituan.net/movie/__40465654__9539763.png","nick":"crd594752025","oppose":0,"reply":0,"id":105682046,"content":"完美！赞赞赞👍"},{"userId":133514988,"score":4.5,"vipInfo":"","nickName":"读懂你7993","time":"2017-05-09 20:24","approve":0,"avatarurl":"http://p0.meituan.net/movie/__40465654__9539763.png","nick":"读懂你7993","oppose":0,"reply":0,"id":105681914,"content":"满满的正义感，拍的剧情觉得很真实，看着都感动得掉眼泪，非常值得看。"},{"userId":268838196,"score":3.5,"vipInfo":"","nickName":"jxZ644207910","time":"2017-05-09 20:24","approve":0,"avatarurl":"http://p0.meituan.net/movie/__40465654__9539763.png","nick":"jxZ644207910","oppose":0,"reply":0,"id":105681891,"content":"影片有太多的刻意画面，没有想象中精彩。"}]
             * hcmts : [{"userId":41640225,"score":5,"vipInfo":"","nickName":"Patrick徐焱","time":"2017-04-28 02:07","approve":686,"avatarurl":"https://img.meituan.net/avatar/4388e595d0742799abb29490ee6c59d8108620.jpg","nick":"悲伤的Patrick","oppose":0,"reply":57,"id":104236005,"content":"谁说香港的警匪片没落了......拉出去斩首...我们这场的观众没有说不好看的...我拉肚子都特么忍了1个小时..."},{"userId":230551170,"score":5,"vipInfo":"","nickName":"T1ng.33","time":"2017-04-28 02:29","approve":275,"avatarurl":"https://img.meituan.net/avatar/0bf2bf73854e2299a7739849475f3438136295.jpg","nick":"T1ng.33","oppose":0,"reply":25,"id":104236866,"content":"可以 可以,我最后统计了一下,刘德华一共死了21次,加上这部片子,刘德华已经死了22次了.真是难为刘德华了"},{"userId":317582276,"score":4.5,"vipInfo":"","nickName":"aeN404311775","time":"2017-04-28 02:35","approve":178,"avatarurl":"http://p0.meituan.net/movie/__40465654__9539763.png","nick":"aeN404311775","oppose":0,"reply":30,"id":104236996,"content":"我觉得华仔的助手在最后听了华仔的话后就要自己大义泯然的剪下去！"},{"userId":176671911,"score":4.5,"vipInfo":"","nickName":"修罗领域","time":"2017-04-29 00:39","approve":70,"avatarurl":"http://p0.meituan.net/movie/__40465654__9539763.png","nick":"Fpp599533337","oppose":0,"reply":9,"id":104362034,"content":"我是警察，我有责任！有多少人可以做到？两个男人的对吼，从自信到无奈，从迷茫畏惧到坚定不移。做一个有责任感的警察不容易，但是做一个有责任感的警察的家人更不容易，因为责任可能凌驾于家庭之上。面临生死决择勇往直前，只为心中的执着，肩上的责任。这电影真的很好看！因为是粤语，所以刘德华的表情和语气都很好，如果是国语的话，可能会有一点😄...贼弟，让我见识到了什么是真正的躺枪，完美！还有就是反应了香港政府对人性的坚持，机会=希望，给予机会，需要多大的勇气和代价！虽然都支持坏蛋都必须死，但是可以昄依佛门或者耶稣还是很不错的，教化改过自身比结束生命的意义和难度都大得多。我说了...结尾会有\u201c但对不起！\u201d吗？"},{"userId":328743555,"score":0.5,"vipInfo":"","nickName":"ndB562331408","time":"2017-04-28 02:27","approve":67,"avatarurl":"http://p0.meituan.net/movie/__40465654__9539763.png","nick":"ndB562331408","oppose":0,"reply":58,"id":104236808,"content":"真他吗的烂，很多细节多此一举"},{"userId":162452474,"score":5,"vipInfo":"","nickName":"怪D与怪花","time":"2017-04-28 15:06","approve":50,"avatarurl":"https://img.meituan.net/avatar/a5384fc9b7eb3488270ad3d7cce6376e62677.jpg","nick":"最爱Andy华","oppose":0,"reply":2,"id":104275591,"content":"节奏紧凑，人物刻画的也比较饱满。能看得出主创人员的野心，不仅仅是一个警匪片动作片，更像是一个灾难片。在一个隧道里，一个封闭的环境下，面对困境，我们该怎么选择？一个人重要还是一百个人重要？这是一个值得我们思考的问题。很棒！值得带家人和朋友一起去看。不过，小心炸弹💣，带好纸巾！"},{"userId":309209582,"score":5,"vipInfo":"","nickName":"阿祖大帅","time":"2017-04-29 17:03","approve":39,"avatarurl":"https://img.meituan.net/avatar/41227cf495c6a95bec3393206ede8e4348953.jpg","nick":"阿祖大帅","oppose":0,"reply":3,"id":104437285,"content":"香港电影拍的越来越有档次了 这几年的片子不断升华 敬畏生命 华仔再一次提升了他在我眼中的地位 导演很有思想 多拍好电影 情节给满分 细节可以再向外国电影多学习 给10分不怕你骄傲！"},{"userId":281342329,"score":5,"vipInfo":"","nickName":"浪子高达","time":"2017-04-30 07:04","approve":39,"avatarurl":"https://img.meituan.net/avatar/2bea0f38679f61e4d667250a67832052167433.jpg","nick":"YIj713359829","oppose":0,"reply":1,"id":104519902,"content":"虽然好贵，不过值回票价。刘德华作为香港形象大使，亲自监制，投资，演出一部香港警匪电影而且效果十分好。感谢你付出。电影评分10分，警匪电影应该有枪战，飞车都有。值得一赞，一部警匪电影可以将感情戏表现得甘细腻绝对一流，剧情将香港警察形象提升了。这部电影感动了我不小于5次，感觉距完胜了寒战了。感谢上帝可以比我用生命保护生命。这是一部会流泪的警匪电影"},{"userId":279470824,"score":5,"vipInfo":"","nickName":"zdm20081221","time":"2017-05-02 01:40","approve":17,"avatarurl":"https://img.meituan.net/avatar/f0d3ac5b4613b076109d0f6320029323100481.jpg","nick":"zdm20081221","oppose":0,"reply":2,"id":104824064,"content":"英雄没能救人还是英雄吗？英雄没能自救还是英雄吗？当然是，还是很感人的英雄。不仅刘德华是英雄，不肯离开旅游团的导游也是英雄，被绑上炸弹吓到不行依旧努力控制自己的小警察也是英雄。很感人的一部关于英雄的电影，如果能把刘德华女朋友换成杨采妮就好了，港产片硬插几个内地演员，诡异违和。"},{"userId":65712521,"score":5,"vipInfo":"","nickName":"ALiliyu","time":"2017-05-09 11:55","approve":1,"avatarurl":"http://p0.meituan.net/movie/__40465654__9539763.png","nick":"ALiliyu","oppose":0,"reply":0,"id":105637830,"content":"全程在紧张度过，一节扣一节，好久没有看到刘影帝风范了，还是宝刀未老，其实好希望结局可以扭转了，让人伤心了一把"}]
             * total : 38637
             * hasNext : true
             */

            public float total;
            public boolean hasNext;
            public List<CmtsBean> cmts;
            public List<HcmtsBean> hcmts;

            public static class CmtsBean {
                /**
                 * userId : 31589876
                 * score : 5
                 * vipInfo :
                 * nickName : KpOnLG
                 * time : 2017-05-09 20:26
                 * approve : 0
                 * avatarurl : http://p0.meituan.net/movie/__40465654__9539763.png
                 * nick : KpOnLG
                 * oppose : 0
                 * reply : 0
                 * id : 105682067
                 * content : 真的很棒！作为一名大陆人民警察，看到了很多很真实的画面，感同身受
                 */

                public float userId;
                public float score;
                public String vipInfo;
                public String nickName;
                public String time;
                public float approve;
                public String avatarurl;
                public String nick;
                public float oppose;
                public float reply;
                public float id;
                public String content;
            }

            public static class HcmtsBean {
                /**
                 * userId : 41640225
                 * score : 5
                 * vipInfo :
                 * nickName : Patrick徐焱
                 * time : 2017-04-28 02:07
                 * approve : 686
                 * avatarurl : https://img.meituan.net/avatar/4388e595d0742799abb29490ee6c59d8108620.jpg
                 * nick : 悲伤的Patrick
                 * oppose : 0
                 * reply : 57
                 * id : 104236005
                 * content : 谁说香港的警匪片没落了......拉出去斩首...我们这场的观众没有说不好看的...我拉肚子都特么忍了1个小时...
                 */

                public float userId;
                public float score;
                public String vipInfo;
                public String nickName;
                public String time;
                public float approve;
                public String avatarurl;
                public String nick;
                public float oppose;
                public float reply;
                public float id;
                public String content;
            }
        }
    }
}
