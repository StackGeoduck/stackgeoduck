package com.xajeyu.stackgeoduck.cinema.cinemadetails.mvp.view;

import com.xajeyu.stackgeoduck.cinema.bean.CinemaDetailsInfo;

/**
 * Created by e on 2017/5/17.
 */

public interface CinemaDetailsViews {
    void onSuccess(Object object);

    void onFailed(Object object);

    /**
     * 显示数据
     *
     * @param cinemaDetailsInfo
     */
    void showData(CinemaDetailsInfo cinemaDetailsInfo);

}
