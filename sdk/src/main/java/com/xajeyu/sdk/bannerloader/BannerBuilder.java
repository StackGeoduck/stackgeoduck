package com.xajeyu.sdk.bannerloader;

import android.content.Context;
import android.util.AttributeSet;

import com.youth.banner.Banner;

/**
 * @PackageName: com.xajeyu.sdk.bannerloader
 * @FileName: BannerBuilder.java
 * @CreationTime: 2017/4/14  14:53
 * @author: XajeYu
 * @E-mail: xajeyu@gmail.com
 * @function:
 */
public class BannerBuilder extends Banner {
    public BannerBuilder(Context context) {
        super(context);
    }

    public BannerBuilder(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BannerBuilder(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
    private Banner Buidler(){
        return null;
    }
}
