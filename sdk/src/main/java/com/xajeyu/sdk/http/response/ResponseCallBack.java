package com.xajeyu.sdk.http.response;

import android.os.Handler;
import android.os.Looper;

import com.google.gson.Gson;
import com.xajeyu.sdk.http.exception.OkhttpException;
import com.xajeyu.sdk.http.listener.DisposeDataHandler;
import com.xajeyu.sdk.http.listener.RequestCallBack;
import com.xajeyu.sdk.music.MusicDataObj;
import com.xajeyu.sdk.news.NewsBean;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.zip.GZIPInputStream;
import java.util.zip.InflaterInputStream;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;


/**
 * @PackageName: com.xajeyu.sdk.http.response
 * @FileName: ResponseCallBack.java
 * @CreationTime: 2017/4/21  10:56
 * @author: XajeYu
 * @E-mail: xajeyu@gmail.com
 * @function: 处理Okhttp的CallBack数据
 */
public class ResponseCallBack implements Callback {
    // 服务器返回的信息
    protected final String RESULT_CODE = "success";
    // 服务器返回的状态码
    protected final int RESULT_CODE_VALUE = 0;
    // 服务器返回的错误信息
    protected final String ERROR_MSG = "emsg";
    // 服务器返回空信息
    protected final String EMPTY_MSG = "";
    private String gzip;
    // 网络异常
    protected final int NETWORK_ERROR = -1;
    // JSON转换异常
    protected final int JSON_ERROR = -2;
    // 其他异常
    protected final int OTHER_ERROR = -3;
    // GZIP解压开关
    private static boolean isGzip = false;

    // 声明一个Handler以便于发送消息至UI
    private Handler mHandler;
    // 将处理过的数据赋值给DisposeDataListener中的函数
    private RequestCallBack mLisener;
    // 定义一个class字节码文件 以便将请求到的数据插入到数据模型中
    private Class<?> mClass;

    /**
     * 构造方法
     */
    public ResponseCallBack(DisposeDataHandler handler) {
        this.mLisener = handler.mListener;
        this.mClass = handler.mClass;
        this.mHandler = new Handler(Looper.getMainLooper());
    }

    /**
     * 请求失败
     *
     * @param call
     * @param e
     */
    @Override
    public void onFailure(Call call, final IOException e) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                // 默认网络异常
                mLisener.onFail(new OkhttpException(NETWORK_ERROR, e));
            }
        });
    }

    /**
     * 请求成功
     *
     * @param call
     * @param response
     * @throws IOException
     */
    @Override
    public void onResponse(Call call, final Response response) throws IOException {
        // 将网易云音乐的流转换成JSON
        if (mClass == MusicDataObj.class) {
            gzip = analyticalGZIP(response.body().byteStream());
        }
        final String reslut = response.body().string();
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                // 网易云音乐需要GZIP解压判断传入的是否是MusicDataObj字节码
                if (mClass == MusicDataObj.class) {
                    handleResponse(gzip);
                }
                handleResponse(reslut);
            }
        });
    }

    /**
     * 处理请求成功后返回的数据
     *
     * @param object
     */
    private void handleResponse(Object object) {
        // 如果传入的值为空
        if (object == null || object.toString().trim().equals("")) {
            mLisener.onFail(new OkhttpException(NETWORK_ERROR, EMPTY_MSG));
            return;
        }
        // 判断是否传入class字节码
        if (mClass == null) {
            mLisener.onSuccess(object);
            return;
        }
        Gson gson = new Gson();
        Object responseObj = gson.fromJson((String) object, mClass);
        // 判断转换后是否为空
        if (responseObj != null) {
            mLisener.onSuccess(responseObj);
        } else {
            // json异常
            mLisener.onFail(new OkhttpException(JSON_ERROR, EMPTY_MSG));
        }

    }

    /**
     * 将GZIP格式转换成String
     *
     * @param inputStream
     * @return
     */
    private String analyticalGZIP(final InputStream inputStream) {

        try {
            GZIPInputStream gzipInputStream = new GZIPInputStream(inputStream);
            ByteArrayOutputStream byteArrayOutputStrea = new ByteArrayOutputStream();
            int length = -1;
            byte[] cache = new byte[1024];

            while ((length = gzipInputStream.read(cache)) != -1) {
                byteArrayOutputStrea.write(cache, 0, length);
            }

            return byteArrayOutputStrea.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }


}
