package com.xajeyu.sdk.http.listener;
/**
  * @PackageName: com.xajeyu.sdk.http.listener
  * @FileName: DisposeDataListener.java
  * @CreationTime: 2017/5/4  14:00
  * @author: XajeYu
  * @E-mail: xajeyu@gmail.com
  * @类说明:
  */
public interface DisposeDataListener {

	/**
	 * 请求成功回调事件处理
	 */
	 void onSuccess(Object responseObj);

	/**
	 * 请求失败回调事件处理
	 */
	 void onFailure(Object reasonObj);

}
