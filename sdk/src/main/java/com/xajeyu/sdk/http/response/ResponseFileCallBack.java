package com.xajeyu.sdk.http.response;

import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import com.xajeyu.sdk.http.exception.OkhttpException;
import com.xajeyu.sdk.http.listener.DisposeDataHandler;
import com.xajeyu.sdk.http.listener.RequestCallBack;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;


/**
 * @PackageName: com.xajeyu.sdk.http.response
 * @FileName: ResponseFileCallBack.java
 * @CreationTime: 2017/5/25  10:07
 * @author: XajeYu
 * @E-mail: xajeyu@gmail.com
 * @function: 下载文件回调
 */
public class ResponseFileCallBack implements Callback {
    /**
     * 异常状态码
     */
    protected final int NETWORK_ERROR = -1; // 网络异常
    protected final int IO_ERROR = -2; // IO异常
    protected final String EMPTY_MSG = "";

    /**
     * 将其它线程的数据转发到UI线程
     */
    private static final int PROGRESS_MESSAGE = 0x01;
    private Handler mDelieryHandler;
    private RequestCallBack mListener;
    private String mFilePath; // 文件路径
    private String mFileName;
    private int mProgress;   //下载进度

    public ResponseFileCallBack(DisposeDataHandler handler){
        this.mListener = handler.mListener;
        this.mFilePath = handler.mSource;
        this.mFileName = handler.mFileName;
        // 发送消息
        this.mDelieryHandler = new Handler(Looper.getMainLooper()){
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what){
                    case PROGRESS_MESSAGE:{
                        mListener.onDownloadProgress((int) msg.obj);
                        break;
                    }
                }
            }
        };

    }


    @Override
    public void onFailure(Call call, final IOException e) {
        mDelieryHandler.post(new Runnable() {
            @Override
            public void run() {
                mListener.onFail(new OkhttpException(NETWORK_ERROR,e));
            }
        });
    }

    @Override
    public void onResponse(Call call, Response response) throws IOException {
        final File file = handleResponse(response);
        mDelieryHandler.post(new Runnable() {
            @Override
            public void run() {
                if (file != null){
                    // 成功回调
                    mListener.onSuccess(file);
                } else {
                    // 失败
                    mListener.onFail(new OkhttpException(IO_ERROR,EMPTY_MSG));
                }
            }
        });
    }

    /**
     * 将网络请求到的信息处理成文件
     * @param response
     * @return
     */
    private File handleResponse(Response response){
        if (response == null){
            return null;
        }
        InputStream inputStream = null;
        File file = null;
        FileOutputStream fileOutputStream = null;
        byte[] bytes = new byte[2048];
        int length;
        // 当前下载
        int currentLength = 0;
        // 文件总大小
        double sumLength;
        try {
            file = checkLocalFilePath(mFilePath,mFileName);
            // 获取网络请求到输入流
            inputStream = response.body().byteStream();
            fileOutputStream = new FileOutputStream(file);
            // 获取文件大小
            sumLength = (double) response.body().contentLength();

            while ((length = inputStream.read(bytes)) != -1){
                fileOutputStream.write(bytes,0,length);
                currentLength += length;
                // 获取当前下载进度
                mProgress = (int )(currentLength / sumLength * 100);
                mDelieryHandler.obtainMessage(PROGRESS_MESSAGE,mProgress).sendToTarget();
            }
            // 刷新缓冲区
            fileOutputStream.flush();
        } catch (Exception e) {
            file = null;
        } finally {
            try {
                if (fileOutputStream != null) {
                    fileOutputStream.close();
                }
                if (inputStream != null) {

                    inputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return file;
    }

    /**
     *
     * @param localFilePath
     */
    private File checkLocalFilePath(String localFilePath,String fileName){
        // 设置文件路径
        File path = Environment.getExternalStorageDirectory();
        File file = new File(path,fileName);
        // 判断文件夹是否存在，如果不存在创建文件夹
        if (!path.exists()){
            path.mkdirs();
        }

        // 判断文件不存在即创建空文件
        if (!file.exists()){
            try {
                file.createNewFile();
                file.setWritable(Boolean.TRUE);
                return file;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }
        return file;
    }
}
