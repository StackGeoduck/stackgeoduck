package com.xajeyu.sdk.http.listener;

/**
 * @ProjectName: UISDK
 * @PackageName：com.xajeyu.okhttp.listener
 * @author：XajeYu
 * @date: 2017/3/26
 * @time：22:19
 * @E-mail: xajeyu@gmail.com
 * @function: 处理JSON对象到实体对象
 */
public class DisposeDataHandler {
    // 回调函数
    public RequestCallBack mListener;
    // 定义一个字节码判断是否需要转换JSON
    public Class<?> mClass = null;
    // 文件资源
    public String mSource = null;
    // 文件名
    public String mFileName = null;

    public DisposeDataHandler(RequestCallBack mListener) {
        this.mListener = mListener;
    }
    // 请求网页
    public DisposeDataHandler(RequestCallBack mListener, Class<?> mClass) {
        this.mListener = mListener;
        this.mClass = mClass;
    }
    // 下载文件
    public DisposeDataHandler(RequestCallBack mListener,String source,String mFileName){
        this.mListener = mListener;
        this.mSource = source;
        this.mFileName = mFileName;
    }
}
