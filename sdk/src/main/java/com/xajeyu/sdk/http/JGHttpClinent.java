package com.xajeyu.sdk.http;

import com.xajeyu.sdk.http.listener.DisposeDataHandler;
import com.xajeyu.sdk.http.response.ResponseCallBack;
import com.xajeyu.sdk.http.response.ResponseFileCallBack;

import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;

/**
 * @PackageName: com.xajeyu.sdk.http
 * @FileName: JGHttpClinent.java
 * @CreationTime: 2017/4/21  10:32
 * @author: XajeYu
 * @E-mail: xajeyu@gmail.com
 * @function: 封装网络请求
 */
public class JGHttpClinent {
    //超时时间
    private static final int TIME_OUT = 30;
    private static OkHttpClient mOkhttpClient;

    /**
     * 配置OkhttpClient
     */
    static {
        OkHttpClient.Builder okhttpBuilder = new OkHttpClient.Builder();
        // 设置超时时间
        okhttpBuilder.connectTimeout(TIME_OUT, TimeUnit.SECONDS);
        okhttpBuilder.readTimeout(TIME_OUT, TimeUnit.SECONDS);
        okhttpBuilder.writeTimeout(TIME_OUT, TimeUnit.SECONDS);
        // 支持重定向
        okhttpBuilder.followRedirects(true);
        // 支持HTTPS
        okhttpBuilder.hostnameVerifier(new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        });
        // 创建一个构建好的Client
        mOkhttpClient = okhttpBuilder.build();
    }

    /**
     * 发送具体的post http/https请求
     *
     * @param request
     * @param handle
     * @return call
     */
    public static Call post(Request request, DisposeDataHandler handle) {
        Call call = mOkhttpClient.newCall(request);
        call.enqueue(new ResponseCallBack(handle));
        return call;
    }

    /**
     * get请求
     * @param request
     * @param handle
     * @return
     */
    public static Call get(Request request, DisposeDataHandler handle) {
        Call call = mOkhttpClient.newCall(request);
        call.enqueue(new ResponseCallBack(handle));
        return call;
    }

    /**
     *s下载文件方法
     * @param request
     * @param handler
     * @return
     */
    public static Call downloadFile(Request request, DisposeDataHandler handler) {
        Call call = mOkhttpClient.newCall(request);
        call.enqueue(new ResponseFileCallBack(handler));
        return call;

    }

}
