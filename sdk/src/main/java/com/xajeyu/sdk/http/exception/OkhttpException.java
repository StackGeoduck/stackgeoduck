package com.xajeyu.sdk.http.exception;

/**
  * @PackageName: com.xajeyu.okhttp.exception
  * @FileName: OkhttpException.java
  * @CreationTime: 2017/3/26  22:32
  * @author: XajeYu
  * @E-mail: xajeyu@gmail.com
  * @类说明: 网络请求异常状态码，以及异常信息
  */
public class OkhttpException{
    // 异常码
    private int ecode;
    // 异常信息
    private Object emsg;

    public OkhttpException(int code, Object msg) {
        this.ecode = code;
        this.emsg = msg;
    }

    public int getEcode() {
        return ecode;
    }

    public Object getEmsg() {
        return emsg;
    }
}
