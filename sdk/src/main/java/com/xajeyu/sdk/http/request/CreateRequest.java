package com.xajeyu.sdk.http.request;

import java.io.File;
import java.util.Map;

import okhttp3.FormBody;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.RequestBody;

/**
 * @PackageName: com.xajeyu.sdk.http.request
 * @FileName: CreateRequest.java
 * @CreationTime: 2017/4/21  10:40
 * @author: XajeYu
 * @E-mail: xajeyu@gmail.com
 * @function: 根据传入的请求方式返回相应的请求
 */
public class CreateRequest {
    /**
     *  视屏
     * @param url
     * @param params
     * @return
     */
    public static Request createMonitorRequest(String url, RequestParams params) {
        StringBuilder urlBuilder = new StringBuilder(url).append("&");
        if (params != null && params.hasParams()) {
            for (Map.Entry<String, String> entry : params.urlParams.entrySet()) {
                urlBuilder.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
            }
        }
        return new Request.Builder().url(urlBuilder.substring(0, urlBuilder.length() - 1)).get().build();
    }

    public static Request createMultiPostRequest(String url, RequestParams params) {

        MultipartBody.Builder requestBody = new MultipartBody.Builder();
        requestBody.setType(MultipartBody.FORM);
        if (params != null) {

            for (Map.Entry<String, Object> entry : params.fileParams.entrySet()) {
                if (entry.getValue() instanceof File) {
                    requestBody.addPart(Headers.of("Content-Disposition", "form-data; name=\"" + entry.getKey() + "\""),
                            RequestBody.create(FILE_TYPE, (File) entry.getValue()));
                } else if (entry.getValue() instanceof String) {

                    requestBody.addPart(Headers.of("Content-Disposition", "form-data; name=\"" + entry.getKey() + "\""),
                            RequestBody.create(null, (String) entry.getValue()));
                }
            }
        }
        return new Request.Builder().url(url).post(requestBody.build()).build();
    }
    /**
     * 文件上传请求
     *
     * @return
     */
    private static final MediaType FILE_TYPE = MediaType.parse("application/octet-stream");
    /**
     * 获取POST请求方式
     * @return
     */
    public static Request createHttpPostRequest(String url, RequestParams params) {
        return createHttpPostRequest(url, params, null);
    }

    /**
     * 创建POST请求
     * @param url
     * @param params
     * @param headers
     * @return
     */
    public static Request createHttpPostRequest(String url,RequestParams params,RequestParams headers){
        // 创建一个FormBody.Buider 构造器
        FormBody.Builder mFormBody = new FormBody.Builder();
        // 判断params 并添加请求参数
        if (params != null) {
            // 利用迭代器将params中的urlParams遍历并添加到bodyBuider
            for (Map.Entry<String, String> entry : params.urlParams.entrySet()) {
                mFormBody.add(entry.getKey(), entry.getValue());
            }
        }
        // 创建头信息构建者
        Headers.Builder mHeaderBuild = new Headers.Builder();
        // 判断传入的头信息是否为空
        if (headers != null) {
            // 利用迭代器将头信息传入到此次请求中
            for (Map.Entry<String, String> entry : headers.urlParams.entrySet()) {
                mHeaderBuild.add(entry.getKey(), entry.getValue());
            }
        }
        // 通过bodyBuider获取frombody
        FormBody formBody = mFormBody.build();
        Headers mHeader = mHeaderBuild.build();
        // 创建Request对象
        // 返回一个创建好的对象
        return new Request.Builder()
                .url(url)
                .post(formBody)
                .headers(mHeader)
                .build();
    }




    /**
     * 获取GET请求
     * @param url 请求地址
     * @param params 请求参数
     * @return 创建好的Request
     */
    public static Request createHttpGetRequest(String url, RequestParams params) {
        return createHttpGetRequest(url, params, null);
    }
    /**
     * 创建GET请求
     * @param url
     * @param params
     * @param headers
     * @return
     */
    public static Request createHttpGetRequest(String url, RequestParams params, RequestParams headers) {
        StringBuilder stringBuilder = new StringBuilder(url).append("?");
        // 判断如果params不为空时进行请求拼接
        if (params != null) {
            // 迭代params并对stringBuilder进行赋值拼接
            for (Map.Entry<String, String> entry : params.urlParams.entrySet()) {
                stringBuilder.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
            }
        }
        // 创建头信息构造器
        Headers.Builder mHeaders = new Headers.Builder();
        // 判断头信息是否为空
        if (headers != null) {
            // 利用迭代器添加请求头信息
            for (Map.Entry<String, String> entry : headers.urlParams.entrySet()) {
                mHeaders.add(entry.getKey(), entry.getValue());
            }
        }
        // 构建header
        Headers mHeader = mHeaders.build();
        // 返回一个创建好的Request对象
        // url地址中读取stringBuilder.length()-1是因为在拼接时末尾有一个&符号
        return new Request.Builder()
                .url(stringBuilder.substring(0, stringBuilder.length() - 1))
                .get()
                .headers(mHeader)
                .build();
    }

}
