package com.xajeyu.sdk.http.listener;

/**
 * @PackageName: com.xajeyu.sdk.http.listener
 * @FileName: RequestCallBack.java
 * @CreationTime: 2017/4/21  10:36
 * @author: XajeYu
 * @E-mail: xajeyu@gmail.com
 * @function: 网络请求回调
 */
public interface RequestCallBack {
    // 成功
    void onSuccess(Object response);
    // 失败
    void onFail(Object response);
    // 下载进度
    void onDownloadProgress(long progress);
}
