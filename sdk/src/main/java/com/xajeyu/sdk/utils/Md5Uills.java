package com.xajeyu.sdk.utils;

import android.text.TextUtils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
  * @PackageName: com.xajeyu.sdk.utils
  * @FileName: Md5Uills.java
  * @CreationTime: 2017/5/11  15:43
  * @author: XajeYu
  * @E-mail: xajeyu@gmail.com
  * @类说明:  MD5名称加密工具
  */
public class Md5Uills {

    public static String generateCode(String url) {
        if (TextUtils.isEmpty(url)) {
            return null;
        }

        StringBuffer buffer = new StringBuffer();
        try {
            MessageDigest digest = MessageDigest.getInstance("md5");
            digest.update(url.getBytes());
            byte[] cipher = digest.digest();
            for (byte b : cipher) {
                String hexStr = Integer.toHexString(b & 0xff);
                buffer.append(hexStr.length() == 1 ? "0" + hexStr : hexStr);
            }

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return buffer.toString();
    }
}
