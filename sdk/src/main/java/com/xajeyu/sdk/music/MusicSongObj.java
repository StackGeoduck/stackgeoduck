package com.xajeyu.sdk.music;

import java.util.List;

/**
 * @PackageName: com.xajeyu.sdk.music
 * @FileName: MusicSongObj.java
 * @CreationTime: 2017/5/12  13:32
 * @author: XajeYu
 * @E-mail: xajeyu@gmail.com
 * @function: 歌曲实体类
 */
public class MusicSongObj {

    /**
     * songs : [{"name":"爱","id":387948,"position":6,"alias":[],"status":0,"fee":8,"copyrightId":13005,"disc":"","no":6,"artists":[{"name":"小虎队","id":13286,"picId":0,"img1v1Id":0,"briefDesc":"","picUrl":"http://p1.music.126.net/6y-UleORITEDbvrOLV0Q8A==/5639395138885805.jpg","img1v1Url":"http://p1.music.126.net/6y-UleORITEDbvrOLV0Q8A==/5639395138885805.jpg","albumSize":0,"alias":[],"trans":"","musicSize":0}],"album":{"name":"爱","id":38429,"type":"专辑","size":11,"picId":79164837216132,"blurPicUrl":"http://p1.music.126.net/Jtu3sxp_0vWqCzFDZZgTfQ==/79164837216132.jpg","companyId":0,"pic":79164837216132,"picUrl":"http://p1.music.126.net/Jtu3sxp_0vWqCzFDZZgTfQ==/79164837216132.jpg","publishTime":680972400000,"description":"","tags":"","company":"飞碟唱片","briefDesc":"","artist":{"name":"","id":0,"picId":0,"img1v1Id":0,"briefDesc":"","picUrl":"http://p1.music.126.net/6y-UleORITEDbvrOLV0Q8A==/5639395138885805.jpg","img1v1Url":"http://p1.music.126.net/6y-UleORITEDbvrOLV0Q8A==/5639395138885805.jpg","albumSize":0,"alias":[],"trans":"","musicSize":0},"songs":[],"alias":[],"status":1,"copyrightId":0,"commentThreadId":"R_AL_3_38429","artists":[{"name":"小虎队","id":13286,"picId":0,"img1v1Id":0,"briefDesc":"","picUrl":"http://p1.music.126.net/6y-UleORITEDbvrOLV0Q8A==/5639395138885805.jpg","img1v1Url":"http://p1.music.126.net/6y-UleORITEDbvrOLV0Q8A==/5639395138885805.jpg","albumSize":0,"alias":[],"trans":"","musicSize":0}],"subType":"录音室版"},"starred":false,"popularity":100,"score":100,"starredNum":0,"duration":269584,"playedNum":0,"dayPlays":0,"hearTime":0,"ringtone":"600902000007947765","crbt":"7cd0398efc00aa963b343770a62d82c4","audition":null,"copyFrom":"","commentThreadId":"R_SO_4_387948","rtUrl":null,"ftype":0,"rtUrls":[],"copyright":0,"mvid":0,"rtype":0,"rurl":null,"bMusic":{"name":"爱","id":10202122,"size":3266267,"extension":"mp3","sr":44100,"dfsId":1129198441733707,"bitrate":96000,"playTime":269584,"volumeDelta":-2.65076E-4},"mp3Url":"http://m2.music.126.net/yWlC3Oda2lCo-Ir_xC7Fjw==/1129198441733707.mp3","hMusic":{"name":"爱","id":10202120,"size":10808444,"extension":"mp3","sr":44100,"dfsId":1085217976622695,"bitrate":320000,"playTime":269584,"volumeDelta":-2.65076E-4},"mMusic":{"name":"爱","id":10202121,"size":5421474,"extension":"mp3","sr":44100,"dfsId":1187472558006034,"bitrate":160000,"playTime":269584,"volumeDelta":-2.65076E-4},"lMusic":{"name":"爱","id":10202122,"size":3266267,"extension":"mp3","sr":44100,"dfsId":1129198441733707,"bitrate":96000,"playTime":269584,"volumeDelta":-2.65076E-4}}]
     * equalizers : {}
     * code : 200
     */

    public EqualizersBean equalizers;
    public int code;
    public List<SongsBean> songs;

    public static class EqualizersBean {
    }

    public static class SongsBean {
        /**
         * name : 爱
         * id : 387948
         * position : 6
         * alias : []
         * status : 0
         * fee : 8
         * copyrightId : 13005
         * disc :
         * no : 6
         * artists : [{"name":"小虎队","id":13286,"picId":0,"img1v1Id":0,"briefDesc":"","picUrl":"http://p1.music.126.net/6y-UleORITEDbvrOLV0Q8A==/5639395138885805.jpg","img1v1Url":"http://p1.music.126.net/6y-UleORITEDbvrOLV0Q8A==/5639395138885805.jpg","albumSize":0,"alias":[],"trans":"","musicSize":0}]
         * album : {"name":"爱","id":38429,"type":"专辑","size":11,"picId":79164837216132,"blurPicUrl":"http://p1.music.126.net/Jtu3sxp_0vWqCzFDZZgTfQ==/79164837216132.jpg","companyId":0,"pic":79164837216132,"picUrl":"http://p1.music.126.net/Jtu3sxp_0vWqCzFDZZgTfQ==/79164837216132.jpg","publishTime":680972400000,"description":"","tags":"","company":"飞碟唱片","briefDesc":"","artist":{"name":"","id":0,"picId":0,"img1v1Id":0,"briefDesc":"","picUrl":"http://p1.music.126.net/6y-UleORITEDbvrOLV0Q8A==/5639395138885805.jpg","img1v1Url":"http://p1.music.126.net/6y-UleORITEDbvrOLV0Q8A==/5639395138885805.jpg","albumSize":0,"alias":[],"trans":"","musicSize":0},"songs":[],"alias":[],"status":1,"copyrightId":0,"commentThreadId":"R_AL_3_38429","artists":[{"name":"小虎队","id":13286,"picId":0,"img1v1Id":0,"briefDesc":"","picUrl":"http://p1.music.126.net/6y-UleORITEDbvrOLV0Q8A==/5639395138885805.jpg","img1v1Url":"http://p1.music.126.net/6y-UleORITEDbvrOLV0Q8A==/5639395138885805.jpg","albumSize":0,"alias":[],"trans":"","musicSize":0}],"subType":"录音室版"}
         * starred : false
         * popularity : 100.0
         * score : 100
         * starredNum : 0
         * duration : 269584
         * playedNum : 0
         * dayPlays : 0
         * hearTime : 0
         * ringtone : 600902000007947765
         * crbt : 7cd0398efc00aa963b343770a62d82c4
         * audition : null
         * copyFrom :
         * commentThreadId : R_SO_4_387948
         * rtUrl : null
         * ftype : 0
         * rtUrls : []
         * copyright : 0
         * mvid : 0
         * rtype : 0
         * rurl : null
         * bMusic : {"name":"爱","id":10202122,"size":3266267,"extension":"mp3","sr":44100,"dfsId":1129198441733707,"bitrate":96000,"playTime":269584,"volumeDelta":-2.65076E-4}
         * mp3Url : http://m2.music.126.net/yWlC3Oda2lCo-Ir_xC7Fjw==/1129198441733707.mp3
         * hMusic : {"name":"爱","id":10202120,"size":10808444,"extension":"mp3","sr":44100,"dfsId":1085217976622695,"bitrate":320000,"playTime":269584,"volumeDelta":-2.65076E-4}
         * mMusic : {"name":"爱","id":10202121,"size":5421474,"extension":"mp3","sr":44100,"dfsId":1187472558006034,"bitrate":160000,"playTime":269584,"volumeDelta":-2.65076E-4}
         * lMusic : {"name":"爱","id":10202122,"size":3266267,"extension":"mp3","sr":44100,"dfsId":1129198441733707,"bitrate":96000,"playTime":269584,"volumeDelta":-2.65076E-4}
         */

        public String name;
        public int id;
        public int position;
        public int status;
        public int fee;
        public int copyrightId;
        public String disc;
        public int no;
        public AlbumBean album;
        public boolean starred;
        public double popularity;
        public int score;
        public int starredNum;
        public int duration;
        public int playedNum;
        public int dayPlays;
        public int hearTime;
        public String ringtone;
        public String crbt;
        public Object audition;
        public String copyFrom;
        public String commentThreadId;
        public Object rtUrl;
        public int ftype;
        public int copyright;
        public int mvid;
        public int rtype;
        public Object rurl;
        public BMusicBean bMusic;
        public String mp3Url;
        public HMusicBean hMusic;
        public MMusicBean mMusic;
        public LMusicBean lMusic;
        public List<?> alias;
        public List<ArtistsBeanX> artists;
        public List<?> rtUrls;

        public static class AlbumBean {
            /**
             * name : 爱
             * id : 38429
             * type : 专辑
             * size : 11
             * picId : 79164837216132
             * blurPicUrl : http://p1.music.126.net/Jtu3sxp_0vWqCzFDZZgTfQ==/79164837216132.jpg
             * companyId : 0
             * pic : 79164837216132
             * picUrl : http://p1.music.126.net/Jtu3sxp_0vWqCzFDZZgTfQ==/79164837216132.jpg
             * publishTime : 680972400000
             * description :
             * tags :
             * company : 飞碟唱片
             * briefDesc :
             * artist : {"name":"","id":0,"picId":0,"img1v1Id":0,"briefDesc":"","picUrl":"http://p1.music.126.net/6y-UleORITEDbvrOLV0Q8A==/5639395138885805.jpg","img1v1Url":"http://p1.music.126.net/6y-UleORITEDbvrOLV0Q8A==/5639395138885805.jpg","albumSize":0,"alias":[],"trans":"","musicSize":0}
             * songs : []
             * alias : []
             * status : 1
             * copyrightId : 0
             * commentThreadId : R_AL_3_38429
             * artists : [{"name":"小虎队","id":13286,"picId":0,"img1v1Id":0,"briefDesc":"","picUrl":"http://p1.music.126.net/6y-UleORITEDbvrOLV0Q8A==/5639395138885805.jpg","img1v1Url":"http://p1.music.126.net/6y-UleORITEDbvrOLV0Q8A==/5639395138885805.jpg","albumSize":0,"alias":[],"trans":"","musicSize":0}]
             * subType : 录音室版
             */

            public String name;
            public int id;
            public String type;
            public int size;
            public long picId;
            public String blurPicUrl;
            public int companyId;
            public long pic;
            public String picUrl;
            public long publishTime;
            public String description;
            public String tags;
            public String company;
            public String briefDesc;
            public ArtistBean artist;
            public int status;
            public int copyrightId;
            public String commentThreadId;
            public String subType;
            public List<?> songs;
            public List<?> alias;
            public List<ArtistsBean> artists;

            public static class ArtistBean {
                /**
                 * name :
                 * id : 0
                 * picId : 0
                 * img1v1Id : 0
                 * briefDesc :
                 * picUrl : http://p1.music.126.net/6y-UleORITEDbvrOLV0Q8A==/5639395138885805.jpg
                 * img1v1Url : http://p1.music.126.net/6y-UleORITEDbvrOLV0Q8A==/5639395138885805.jpg
                 * albumSize : 0
                 * alias : []
                 * trans :
                 * musicSize : 0
                 */

                public String name;
                public int id;
                public int picId;
                public int img1v1Id;
                public String briefDesc;
                public String picUrl;
                public String img1v1Url;
                public int albumSize;
                public String trans;
                public int musicSize;
                public List<?> alias;
            }

            public static class ArtistsBean {
                /**
                 * name : 小虎队
                 * id : 13286
                 * picId : 0
                 * img1v1Id : 0
                 * briefDesc :
                 * picUrl : http://p1.music.126.net/6y-UleORITEDbvrOLV0Q8A==/5639395138885805.jpg
                 * img1v1Url : http://p1.music.126.net/6y-UleORITEDbvrOLV0Q8A==/5639395138885805.jpg
                 * albumSize : 0
                 * alias : []
                 * trans :
                 * musicSize : 0
                 */

                public String name;
                public int id;
                public int picId;
                public int img1v1Id;
                public String briefDesc;
                public String picUrl;
                public String img1v1Url;
                public int albumSize;
                public String trans;
                public int musicSize;
                public List<?> alias;
            }
        }

        public static class BMusicBean {
            /**
             * name : 爱
             * id : 10202122
             * size : 3266267
             * extension : mp3
             * sr : 44100
             * dfsId : 1129198441733707
             * bitrate : 96000
             * playTime : 269584
             * volumeDelta : -2.65076E-4
             */

            public String name;
            public int id;
            public int size;
            public String extension;
            public int sr;
            public long dfsId;
            public int bitrate;
            public int playTime;
            public double volumeDelta;
        }

        public static class HMusicBean {
            /**
             * name : 爱
             * id : 10202120
             * size : 10808444
             * extension : mp3
             * sr : 44100
             * dfsId : 1085217976622695
             * bitrate : 320000
             * playTime : 269584
             * volumeDelta : -2.65076E-4
             */

            public String name;
            public int id;
            public int size;
            public String extension;
            public int sr;
            public long dfsId;
            public int bitrate;
            public int playTime;
            public double volumeDelta;
        }

        public static class MMusicBean {
            /**
             * name : 爱
             * id : 10202121
             * size : 5421474
             * extension : mp3
             * sr : 44100
             * dfsId : 1187472558006034
             * bitrate : 160000
             * playTime : 269584
             * volumeDelta : -2.65076E-4
             */

            public String name;
            public int id;
            public int size;
            public String extension;
            public int sr;
            public long dfsId;
            public int bitrate;
            public int playTime;
            public double volumeDelta;
        }

        public static class LMusicBean {
            /**
             * name : 爱
             * id : 10202122
             * size : 3266267
             * extension : mp3
             * sr : 44100
             * dfsId : 1129198441733707
             * bitrate : 96000
             * playTime : 269584
             * volumeDelta : -2.65076E-4
             */

            public String name;
            public int id;
            public int size;
            public String extension;
            public int sr;
            public long dfsId;
            public int bitrate;
            public int playTime;
            public double volumeDelta;
        }

        public static class ArtistsBeanX {
            /**
             * name : 小虎队
             * id : 13286
             * picId : 0
             * img1v1Id : 0
             * briefDesc :
             * picUrl : http://p1.music.126.net/6y-UleORITEDbvrOLV0Q8A==/5639395138885805.jpg
             * img1v1Url : http://p1.music.126.net/6y-UleORITEDbvrOLV0Q8A==/5639395138885805.jpg
             * albumSize : 0
             * alias : []
             * trans :
             * musicSize : 0
             */

            public String name;
            public int id;
            public int picId;
            public int img1v1Id;
            public String briefDesc;
            public String picUrl;
            public String img1v1Url;
            public int albumSize;
            public String trans;
            public int musicSize;
            public List<?> alias;
        }
    }
}
