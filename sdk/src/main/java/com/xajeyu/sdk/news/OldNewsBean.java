package com.xajeyu.sdk.news;

import java.util.List;

/**
 * Created by 13517 on 2017/5/5.
 */

public class OldNewsBean {
    /**
     * date : 20161231
     * stories : [{"images":["http://pic3.zhimg.com/9a1e02ed7f6a3370976a39fdc9d49876.jpg"],"type":0,"id":9095858,"ga_prefix":"123122","title":"小事 · 和深爱的人结婚，第七年"},{"images":["http://pic4.zhimg.com/9966d7be676634564901a0f18f83feb7.jpg"],"type":0,"id":9111167,"ga_prefix":"123121","title":"今年还有这些有趣的作品，你可能错过了"},{"images":["http://pic4.zhimg.com/0bd7c07b7a2d7e2559cbb3c015a1e863.jpg"],"type":0,"id":9097072,"ga_prefix":"123120","title":"红肉也致癌？还能不能好好吃饭了"},{"images":["http://pic4.zhimg.com/6108e163d57b44241a5b589fbf0ae4b3.jpg"],"type":0,"id":9100660,"ga_prefix":"123119","title":"原来图书馆是这么给书编号的，以后可以一下子找到了"},{"images":["http://pic1.zhimg.com/b36a9844bb21705c50c3af735a98ab4c.jpg"],"type":0,"id":9097620,"ga_prefix":"123118","title":"父母离婚了，怎么告诉还不到六岁的孩子？"},{"images":["http://pic2.zhimg.com/970e003b6fc5036ae9e06a1a5d94ea31.jpg"],"type":0,"id":9112073,"ga_prefix":"123117","title":"知乎好问题 · 逃避痛苦的力量和追求快乐的力量哪个更大？"},{"title":"梅丽尔 · 斯特里普：一路开挂，成为美国最会演的阿姨","ga_prefix":"123116","images":["http://pic1.zhimg.com/47ce417d8b2e4a690198623198c57aa8.jpg"],"multipic":true,"type":0,"id":9100769},{"images":["http://pic4.zhimg.com/3380af81bdf7e5bf4c25660c325a8ec3.jpg"],"type":0,"id":9106642,"ga_prefix":"123115","title":"能测出脂肪含量的智能秤，可没用什么黑科技"},{"images":["http://pic2.zhimg.com/0b91c197bb3d0422b7383dc1d994fd99.jpg"],"type":0,"id":9098112,"ga_prefix":"123114","title":"就是画了几个不同颜色的框框？那你是真不懂蒙德里安"},{"images":["http://pic1.zhimg.com/2cb150110ca623aa6c0730941d4e40bc.jpg"],"type":0,"id":9098002,"ga_prefix":"123113","title":"USB 芯片原来静悄悄地干了好多事啊"},{"images":["http://pic4.zhimg.com/43f6ae6e6d7a1071df1a7c5b648f2e7f.jpg"],"type":0,"id":9107421,"ga_prefix":"123112","title":"大误 · 你，怕不怕我"},{"images":["http://pic3.zhimg.com/5268fa6ab2a37a3e520097fa3c38dce2.jpg"],"type":0,"id":9100942,"ga_prefix":"123111","title":"超级简单的三步烤鸡制作法，皮脆肉嫩一点不柴"},{"images":["http://pic3.zhimg.com/fb6425845ab83abfecc56240528b1442.jpg"],"type":0,"id":9095389,"ga_prefix":"123110","title":"乾隆皇帝啊，你对瓷器的品位看起来好像不如你爸？"},{"images":["http://pic4.zhimg.com/334fb3d2037171e6b23c69b2699ac893.jpg"],"type":0,"id":9107764,"ga_prefix":"123109","title":"是该说走就走地裸辞，还是骑驴找马先找好下家？"},{"images":["http://pic3.zhimg.com/0e9c314e8e5c24562ef03cd7bc684342.jpg"],"type":0,"id":9097630,"ga_prefix":"123108","title":"看看人家美国英文课的套路，5 岁开始培养大学能力"},{"images":["http://pic3.zhimg.com/6125c4b3ccd1586e57b3bd01e5096176.jpg"],"type":0,"id":9098103,"ga_prefix":"123107","title":"张大爷和李大妈要在纽约卖打卤馕，办个什么样的公司呢？"},{"images":["http://pic4.zhimg.com/e4be92451cd10aa2e70579636df4f867.jpg"],"type":0,"id":9111269,"ga_prefix":"123107","title":"董明珠说 90 后开网店会危害实体经济，这种担忧毫无必要"},{"images":["http://pic2.zhimg.com/20fe458d80beaeab30ef8c66cb250e5d.jpg"],"type":0,"id":9111198,"ga_prefix":"123107","title":"2016 年度盘点 · 科技领域出现了哪些新趋势？"},{"images":["http://pic2.zhimg.com/e58351f1791aa5a6ba560ffa9957e495.jpg"],"type":0,"id":9108420,"ga_prefix":"123106","title":"瞎扯 · 如何正确地吐槽"}]
     */
    public String date;
    public List<StoriesBean> stories;

    public static class StoriesBean {
        /**
         * images : ["http://pic3.zhimg.com/9a1e02ed7f6a3370976a39fdc9d49876.jpg"]
         * type : 0
         * id : 9095858
         * ga_prefix : 123122
         * title : 小事 · 和深爱的人结婚，第七年
         * multipic : true
         */

        public int type;
        public int id;
        public String ga_prefix;
        public String title;
        public boolean multipic;
        public List<String> images;
    }
}
