package com.xajeyu.sdk.news;

import java.util.List;

/**
 * Created by 13517 on 2017/4/20.
 */

/**
 * 知乎新闻的Bean
 */
public class NewsBean {

    /**
     * date : 20170420
     * stories : [{"images":["https://pic3.zhimg.com/v2-536a0b29cacf86df0a84af1f7223d7b6.jpg"],"type":0,"id":9370541,"ga_prefix":"042010","title":"面试被问到「职业规划」， 怎样回答最加分？"},{"images":["https://pic1.zhimg.com/v2-f5f287caac6fce9a0ab1aa26f4841ab0.jpg"],"type":0,"id":9369711,"ga_prefix":"042009","title":"首相特里莎 · 梅忽然宣布提前举行大选，整个英国都有点懵"},{"title":"凭什么说我没有理想，我很好吃啊","ga_prefix":"042008","images":["https://pic2.zhimg.com/v2-c42e7b65efbeb8277000688c1edb4c39.jpg"],"multipic":true,"type":0,"id":9368844},{"images":["https://pic1.zhimg.com/v2-fed3c60239053c737d677b553e697fec.jpg"],"type":0,"id":9369813,"ga_prefix":"042007","title":"在心理咨询室里倾诉时，把现实暂且隔在门外"},{"images":["https://pic3.zhimg.com/v2-9e80bec347c453b0791f5d85966e2bee.jpg"],"type":0,"id":9369561,"ga_prefix":"042007","title":"别只看「亏损 4 亿」，锤子科技可能比你想的值钱多了"},{"title":"《速度与激情 8》 里出现了哪些车？","ga_prefix":"042007","images":["https://pic3.zhimg.com/v2-eb86be4db6fa4365af9cee5fd29ab5b2.jpg"],"multipic":true,"type":0,"id":9369598},{"images":["https://pic2.zhimg.com/v2-284951e3180f625de185ba2e4050b9b1.jpg"],"type":0,"id":9368782,"ga_prefix":"042006","title":"瞎扯 · 如何正确地吐槽"}]
     * top_stories : [{"image":"https://pic4.zhimg.com/v2-99298bef6651e1d74edf63f578a76787.jpg","type":0,"id":9369598,"ga_prefix":"042007","title":"《速度与激情 8》 里出现了哪些车？"},{"image":"https://pic2.zhimg.com/v2-fc8c01026f7a934629d5e7d3a8b9e3f9.jpg","type":0,"id":9369561,"ga_prefix":"042007","title":"别只看「亏损 4 亿」，锤子科技可能比你想的值钱多了"},{"image":"https://pic2.zhimg.com/v2-63740b573f8ed42be05cb935944883a9.jpg","type":0,"id":9354748,"ga_prefix":"041915","title":"柜子一推变桌子、大床折叠能上墙的「变形家具」，真正用起来可能会累哭"},{"image":"https://pic2.zhimg.com/v2-78421626cadc2959e3484ba8f04c4859.jpg","type":0,"id":9367712,"ga_prefix":"041907","title":"最早明天，中国首个货运飞船「天舟一号」就要发射"},{"image":"https://pic1.zhimg.com/v2-647f8ff01179b922d0948dc9a44dd868.jpg","type":0,"id":9367103,"ga_prefix":"041907","title":"做了「看时爽死、出门即忘」的准备，速 8 依然让人失望"}]
     */
    public String date;
    public List<StoriesBean> stories;
    public List<TopStoriesBean> top_stories;

    public static class StoriesBean {
        /**
         * images : ["https://pic3.zhimg.com/v2-536a0b29cacf86df0a84af1f7223d7b6.jpg"]
         * type : 0
         * id : 9370541
         * ga_prefix : 042010
         * title : 面试被问到「职业规划」， 怎样回答最加分？
         * multipic : true
         */

        public int type;
        public int id;
        public String ga_prefix;
        public String title;
        public boolean multipic;
        public List<String> images;
    }

    public static class TopStoriesBean {
        /**
         * image : https://pic4.zhimg.com/v2-99298bef6651e1d74edf63f578a76787.jpg
         * type : 0
         * id : 9369598
         * ga_prefix : 042007
         * title : 《速度与激情 8》 里出现了哪些车？
         */

        public String image;
        public int type;
        public int id;
        public String ga_prefix;
        public String title;
    }
    public String res;
}
