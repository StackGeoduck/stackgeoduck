package com.xajeyu.sdk.imageloader;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.ImageView;

import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.xajeyu.sdk.R;

/**
 * @PackageName: com.xajeyu.imageloader
 * @FileName:
 * @CreationTime: 2017/3/30  18:53
 * @author: XajeYu
 * @E-mail: xajeyu@gmail.com
 * @function: 图片加载器,封装UIL，简化重复代码
 */
public class ImageLoaderManager {
    private static Context mContext; // 上下文
    private static final int THREAD_COUNT = 4; // 线程数
    private static final int PRIORITY = 2;// 进程优先级
    private static final int DISK_CACHE_SIZE = 50 * 1024 * 1024; // 缓存文件大小
    private static final int CONNECTION_TIME_OUT = 5 * 1000;// 连接超时时间
    private static final int READER_TIME_OUT = 30 * 1000;// 读取超时时间
    private static ImageLoader mImageLoader = null;
    private static ImageLoaderManager mImageLoaderManager = null;

//    /**
//     * 使用内部构造类优化单例模式
//     */
//    private static class ImageLoaderManagerHolder {
//        private static final ImageLoaderManager instance = new ImageLoaderManager();
//    }

    /**
     * 私有化构造函数
     */
    private ImageLoaderManager(Context mContext) {
        getImageLoaderConfiguration(mContext);
    }

    /**
     * 获取一个ImageLoaderManager实例
     *
     * @return 返回一个ImageLoaderManager实例
     */
    public static ImageLoaderManager getInstance(Context context) {
        if (mImageLoaderManager == null){
            synchronized (ImageLoaderManager.class){
                if (mImageLoaderManager == null){
                    mImageLoaderManager = new ImageLoaderManager(context);
                }
            }
        }
        return mImageLoaderManager;
    }

    /**
     * 获取ImageLoaderConfiguration配置对象
     *
     * @param context 传入上下文
     * @return 返回一个构建好的ImageLoaderConfiguration对象
     */
    private static void getImageLoaderConfiguration(Context context) {
        ImageLoaderConfiguration configuration = new ImageLoaderConfiguration
                .Builder(context)
                .threadPoolSize(THREAD_COUNT) // 配置图片下载线程的最大数量
                .threadPriority(Thread.NORM_PRIORITY - PRIORITY) // 配置进程的优先级
                .denyCacheImageMultipleSizesInMemory() // 防止缓存多套尺寸的图片到内存中
                .memoryCache(new WeakMemoryCache())  // 使用弱引用内存缓存
                .diskCacheSize(DISK_CACHE_SIZE) // 分配硬盘缓存大小
                .diskCacheFileNameGenerator(new Md5FileNameGenerator()) // 使用MD5对其文件进行命名
                .tasksProcessingOrder(QueueProcessingType.LIFO) // 图片下载顺序
                .defaultDisplayImageOptions(getDefultOptions()) // 设置默认显示图片
                .imageDownloader(new BaseImageDownloader(context, CONNECTION_TIME_OUT, READER_TIME_OUT)) // 配置图片下载器
                .writeDebugLogs() // debug环境下输出日志
                .build();
        ImageLoader.getInstance().init(configuration);
        mImageLoader = ImageLoader.getInstance();
    }

    /**
     * 实现默认的Options
     * @return DisplayImageOptions对象
     */
    private static DisplayImageOptions getDefultOptions() {
        DisplayImageOptions options = new DisplayImageOptions
                .Builder()
                .showImageForEmptyUri(R.drawable.jg_img_error) // 当图片地址为空时的默认图片
                .showImageOnFail(R.drawable.jg_img_error) // 图片下载失败时的默认图片
                .cacheInMemory(true) // 设置图片可以缓存在内存中
                .cacheOnDisk(true) // 设置图片可以缓存在硬盘中
                .bitmapConfig(Bitmap.Config.RGB_565)// 使用图片的解码类型
                .decodingOptions(new BitmapFactory.Options()) // 图片解码配置
                .build();
        return options;

    }

    /**
     * 加载图片API
     * @param imageView
     * @param url
     * @param options
     * @param listener
     */
    public  void displayImage(ImageView imageView, String url,
                             DisplayImageOptions options,
                             ImageLoadingListener listener){
        if (mImageLoader != null){
            mImageLoader.displayImage(url,imageView,options,listener);
        }
    }

    public  void display(ImageView imageView,String url){
        displayImage(imageView,url,null,null);
    }
    public  void display(ImageView imageView,String url,ImageLoadingListener listener){
        displayImage(imageView,url,null,listener);
    }

}
